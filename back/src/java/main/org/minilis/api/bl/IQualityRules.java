package org.minilis.api.bl;

import org.minilis.entity.jors.QualityControl;
import org.minilis.impl.bl.QualityCalcIndics;
import org.minilis.impl.bl.QualityRuleRes;


/**
 * The Interface IQualityRules check for Shewhard, Westgard rules.
 */
public interface IQualityRules {

	/**
	 * method the qualityCalcIindics.
	 *
	 * @param qc is current item from QualityControl journal.
	 * @return the quality parameters.
	 */
	public QualityCalcIndics getQualityCalcIindics(QualityControl qc);
	
	/**
	 * Gets the shewhart rule.
	 *
	 * @param qc is current item from QualityControl journal.
	 * @return error if the shewhart rule was failed.
	 */
	public QualityRuleRes getShewhartRule(QualityControl qc);
	
	/**
	 * Gets the westgard rule.
	 *
	 * @param qc is current item from QualityControl journal.
	 * @return error if the westgard rule was failed.
	 */
	public QualityRuleRes getWestgardRule(QualityControl qc);
	
}
