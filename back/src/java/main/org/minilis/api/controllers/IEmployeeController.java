package org.minilis.api.controllers;

import org.minilis.entity.dics.Employee;

/**
 * The Interface IEmployeeController.
 */
public interface IEmployeeController extends IController<Employee>{
	

}
