package org.minilis.api.controllers;

import java.util.List;

import org.hibernate.criterion.Criterion;

/**
 * The Base Interface IController.
 *
 * @param <T> the generic type
 */
public interface IController<T> {

	/**
	 * Insert object using BaseService.
	 *
	 * @param obj is inserting object
	 * @return inserted object
	 */
	public T insert(T obj);
	
	/**
	 * Update object using BaseService.
	 *
	 * @param obj is replacing object.
	 * @return updated object.
	 */
	public T update(T obj);
	
	/**
	 * Delete object using BaseService.
	 *
	 * @param obj is deleting object.
	 */
	public void delete(T obj);
	
	/**
	 * Select objects using BaseService.
	 *
	 * @param from is how many elements will selected.
	 * @param first is the first element from which the data will selected.
	 * @param filter is the list of criteria.
	 * @return all selected elements.
	 */
	public List<T> select(int from, int first, List<Criterion> filter);
	/**
	 * Select objects using BaseService.
	 *
	 * @param filter is the list of criteria.
	 * @return all selected elements.
	 */
	public List<T> select(List<Criterion> filter);


}
