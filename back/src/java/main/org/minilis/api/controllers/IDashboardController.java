package org.minilis.api.controllers;

// don't know what return
/**
 * The Interface IDashboardController.
 */
public interface IDashboardController {

	/**
	 * Gets the dach board.
	 *
	 * @return the dach board
	 */
	public String getDachBoard();
}
