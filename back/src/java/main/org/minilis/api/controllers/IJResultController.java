package org.minilis.api.controllers;

import java.util.List;

import org.minilis.entity.jors.JResult;

/**
 * The Interface IJResultController.
 */
public interface IJResultController extends IController<JResult>{

 	/**
	 * Select JResult by order.
	 *
	 * @param id the JResult
	 * @return the list 
	 */
	public List<JResult> selectByOrder(String id);

	/**
	 * Send email by order.
	 *
	 * @param id the id
	 */
	public void sendEmailByOrder(String id);

	/**
	 * Close by order.
	 *
	 * @param id the id
	 */
	public void closeByOrder(String id);

	/**
	 * Select JResult by JLabProccess.
	 *
	 * @param idLabProc the id JLabProccess
	 * @return the list
	 */
	public List<JResult> selectByLabProc(String idLabProc);

	/**
	 * Select free results.
	 *
	 * @return the list
	 */
	public List<JResult> selectFreeResults();

}
