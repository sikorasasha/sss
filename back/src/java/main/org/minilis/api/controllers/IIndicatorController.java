package org.minilis.api.controllers;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.minilis.entity.dics.Indicator;

/**
 * The Interface IIndicatorController.
 */
public interface IIndicatorController extends IController<Indicator>{
	

	/**
	 * Sets the import indicators.
	 *
	 * @param nameimport the new import indicators
	 */
	public void setImportIndicators(String nameimport);

	/**
	 * Select Indicators.
	 *
	 * @param from is how many elements will selected.
	 * @param first is the first element from which the data will selected.
	 * @param filter is the list of criteria.
	 * @return all selected elements.
	 */
	public List<Indicator> selectComplex(int from, int first, List<Criterion> filter);

	/**
	 * Gets the import indicators.
	 *
	 * @return the import indicators
	 */
	public String getImportIndicators();

	

}
