/**
 * 
 */
package org.minilis.api.controllers;

/**
 * @author sasha
 *
 */
public interface IPrintController {

	/**
	 * @param id
	 */
	public void printBarCode(String id);
	
	
	/**
	 * @param id
	 */
	public void printOrder(String id);

	/**
	 * @param id
	 */
	public void printResultByOrder(String id);

	/**
	 * @param idRes
	 * @return 
	 */
	public void printResultByLabProc(String idRes);

	/**
	 * @param idLabProc
	 */
	public void print250f(String idLabProc);

	/**
	 * @param idLabProc
	 */
	public void printQC(String idLabProc);


}
