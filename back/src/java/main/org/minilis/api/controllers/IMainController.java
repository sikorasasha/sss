/**
 *
 */
package org.minilis.api.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.minilis.entity.jors.JOrder;

/**
 * The Interface IMainController.
 *
 * @author sasha
 */
public interface IMainController {

	/**
	 * Log out.
	 *
	 * @param request the request
	 */
	public String logOut(HttpServletRequest request);

	/**
	 * Search.
	 *
	 * @param from the from
	 * @param filter the filter
	 * @return the list
	 */
	public List<JOrder> search(int from, String filter);
}
