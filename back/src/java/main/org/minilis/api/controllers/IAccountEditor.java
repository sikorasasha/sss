package org.minilis.api.controllers;

import org.minilis.entity.security.Account;

/**
 * The Interface IAccountEditor.
 *
 * @author sasha
 */
public interface IAccountEditor extends IController<Account>{
	
}
