package org.minilis.api.controllers;

import org.minilis.entity.jors.QualityControl;

/**
 * The Interface IQualityControlController.
 */
public interface IQualityControlController extends IController<QualityControl>{

}
