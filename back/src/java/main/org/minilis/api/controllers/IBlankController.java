package org.minilis.api.controllers;

import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.minilis.entity.dics.Blank;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 * The Interface IBlankController.
 */
public interface IBlankController extends IController<Blank>{

	/**
	 * Import blank.
	 *
	 * @param id the id
	 * @param request the request
	 * @param response the response
	 */
	public void importBlank(String id,MultipartHttpServletRequest request, HttpServletResponse response);

	/**
	 * Export blank.
	 *
	 * @param id the id
	 * @return the blank
	 */
	public JSONObject exportBlank(String id);




}
