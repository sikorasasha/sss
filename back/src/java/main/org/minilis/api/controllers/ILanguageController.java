package org.minilis.api.controllers;

import org.minilis.entity.config.Language;

/**
 * The Interface ILanguageController.
 */
public interface ILanguageController extends IController<Language>{

}
