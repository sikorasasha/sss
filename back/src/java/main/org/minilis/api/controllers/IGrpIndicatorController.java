package org.minilis.api.controllers;

import org.minilis.entity.dics.GrpIndicator;

/**
 * The Interface IGrpIndicatorController.
 */
public interface IGrpIndicatorController extends IController<GrpIndicator>{


}
