package org.minilis.api.controllers;

import org.minilis.entity.jors.ResultFiles;

/**
 * The Interface IResultFilesController.
 */
public interface IResultFilesController extends IController<ResultFiles>{

}
