package org.minilis.api.controllers;

import org.minilis.entity.config.CfgEmail;

/**
 * The Interface ICfgEmailController.
 */
public interface ICfgEmailController extends IController<CfgEmail>{

}
