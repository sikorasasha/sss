package org.minilis.api.controllers;

import org.minilis.entity.jors.QualityControlDt;

/**
 * The Interface IQualityControlDtController.
 */
public interface IQualityControlDtController extends IController<QualityControlDt>{

}
