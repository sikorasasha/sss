package org.minilis.api.controllers;

import org.minilis.entity.dics.Indicator;
import org.minilis.entity.jors.JOrder;

/**
 * The Interface IJOrderController.
 */
public interface IJOrderController extends IController<JOrder>{

	/**
	 * Gets the last price.
	 *
	 * @param obj the Indicator
	 * @return the last price
	 */
	public Double getLastPrice(Indicator obj);

}
