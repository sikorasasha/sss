package org.minilis.api.controllers;

import org.minilis.entity.security.Account;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 * The Interface IAccountController.
 */
public interface IAccountController extends IController<Account>{

	/**
	 * @param idAccount
	 * @return
	 */
	public String showPreviewReport(String idAccount);

	/**
	 * @param request
	 * @return
	 */
	public String savefile(MultipartHttpServletRequest request);

}
