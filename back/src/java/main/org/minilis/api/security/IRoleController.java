package org.minilis.api.security;

import org.minilis.api.controllers.IController;
import org.minilis.entity.security.Role;

/**
 * The Interface IRoleController.
 */
public interface IRoleController extends IController<Role>{

}
