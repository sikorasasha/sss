/**
 * 
 */
package org.minilis.api.security;

/**
 * @author sasha
 *
 */
public interface IPassRecoverController {

	public String sendRecoverLink(String data);
	
	public String changePass(String data);
	
	public String getDateRecoverLink(String dateRecoveryPass);
}
