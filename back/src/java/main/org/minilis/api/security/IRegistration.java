package org.minilis.api.security;

/**
 * The Interface IRegistration.
 */
public interface IRegistration {

	/**
	 * Adds the account.
	 *
	 * @param acc
	 *            the Account
	 * @return the boolean
	 */
	public String registration(String acc);

	/**
	 * @param domen
	 * @return
	 */
	public String getDomain(String domain);


	/**
	 * @param emain
	 * @return
	 */
	String getEmail(String emain);

}
