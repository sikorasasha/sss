package org.minilis.api.security;

/**
 * The Interface IActivation.
 */
public interface IActivation {

	/**
	 * Activate employee.
	 *
	 * @param id Employee
	 * @return the string
	 */
	public String activateEmployee(String id);

	/**
	 * @param acc
	 * @return
	 */
	public String activation(String acc);
}
