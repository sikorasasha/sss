/**
 *
 */
package org.minilis.api.security;


/**
 * @author sasha
 *
 */
public interface ILoginController{

	/**
	 * @param login
	 * @return
	 */
	public String login(String login);

	/**
	 *
	 */
	public String checkAuth(String authKey);

	/**
	 * @param authid
	 * @return
	 */
	String relogin(String authid);

}
