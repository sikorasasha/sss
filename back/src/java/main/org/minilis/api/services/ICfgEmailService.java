package org.minilis.api.services;

import org.minilis.entity.config.CfgEmail;

/**
 * The Interface for CfgEmailService.
 */
public interface ICfgEmailService extends IService<CfgEmail>{

}
