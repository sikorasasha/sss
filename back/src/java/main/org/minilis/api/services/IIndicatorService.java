package org.minilis.api.services;

import org.minilis.entity.dics.Indicator;

/**
 * The Interface for IndicatorService.
 */
public interface IIndicatorService extends IService<Indicator>{

}
