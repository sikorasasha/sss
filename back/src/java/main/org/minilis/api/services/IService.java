package org.minilis.api.services;

import java.util.List;

import org.hibernate.criterion.Criterion;


/**
 * The Main Interface for Service.
 *
 * @param <T> the generic type
 */
public interface IService<T> {

	/**
	 * Insert object using BaseDao.
	 *
	 * @param obj is inserting object
	 * @return inserted object
	 */
	public T insert(T obj);

	/**
	 * Update object using BaseDao.
	 *
	 * @param obj is replacing object.
	 * @return updated object.
	 */
	public T update(T obj);

	/**
	 * Delete object using BaseDao.
	 *
	 * @param obj is deleting object.
	 */
	public void delete(T obj);

	/**
	 * Select objects using BaseDao.
	 *
	 * @param from is how many elements will selected.
	 * @param first is the first element from which the data will selected.
	 * @param filter is the list of criteria.
	 * @return all selected elements.
	 */
	public List<T> select(int from, int first, List<Criterion> filter);
	
	/**
	 * Select objects using BaseDao.
	 *
	 * @return all selected elements.
	 */
	public List<T> select(List<Criterion> filter);

	/**
	 * Initialization list of JOrders
	 */
	public void init(List<T> l);
	
	public T selectById(String id);

}
