package org.minilis.api.services;

import org.minilis.entity.jors.QualityControl;

/**
 * The Interface for QualityControlService.
 */
public interface IQualityControlService extends IService<QualityControl>{

}
