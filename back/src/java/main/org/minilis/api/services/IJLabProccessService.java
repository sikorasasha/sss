package org.minilis.api.services;

import org.minilis.entity.jors.JLabProccess;

/**
 * The Interface for JLabProccessService.
 *
 * @author sasha
 */
public interface IJLabProccessService extends IService<JLabProccess>{

}
