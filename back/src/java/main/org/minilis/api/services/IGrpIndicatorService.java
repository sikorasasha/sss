package org.minilis.api.services;

import org.minilis.entity.dics.GrpIndicator;

/**
 * The Interface for GrpIndicatorService.
 */
public interface IGrpIndicatorService extends IService<GrpIndicator>{

}
