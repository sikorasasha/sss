/**
 * 
 */
package org.minilis.api.services;

import org.minilis.entity.dics.PrintMarker;

/**
 * @author sasha
 *
 */
public interface IPrintMarkerService extends IService<PrintMarker>{

}
