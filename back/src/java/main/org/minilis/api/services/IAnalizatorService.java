package org.minilis.api.services;

import org.minilis.entity.dics.Analizator;

/**
 * The Interface for AnalizatorService.
 */
public interface IAnalizatorService extends IService<Analizator>{

}
