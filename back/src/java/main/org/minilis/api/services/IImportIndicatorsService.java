package org.minilis.api.services;

import org.minilis.entity.dics.ImportIndicators;

/**
 * The Interface for ImportIndicatorsService.
 *
 * @author sasha
 */
public interface IImportIndicatorsService extends IService<ImportIndicators>{

}
