package org.minilis.api.services;

import org.minilis.entity.jors.ResultFiles;

/**
 * The Interface for ResultFilesService.
 */
public interface IResultFilesService extends IService<ResultFiles>{

}
