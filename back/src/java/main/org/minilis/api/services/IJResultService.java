package org.minilis.api.services;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.minilis.entity.jors.JResult;

/**
 * The Interface for JResultService.
 */
public interface IJResultService extends IService<JResult>{

	/**
	 * @param from
	 * @param first
	 * @param filter
	 * @return
	 */
	public List<JResult> selectJResult(int from, int first, List<Criterion> filter);

}
