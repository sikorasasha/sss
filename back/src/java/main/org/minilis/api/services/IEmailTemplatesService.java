/**
 * 
 */
package org.minilis.api.services;

import org.minilis.entity.config.EmailTemplates;

/**
 * @author sasha
 *
 */
public interface IEmailTemplatesService extends IService<EmailTemplates>{

}
