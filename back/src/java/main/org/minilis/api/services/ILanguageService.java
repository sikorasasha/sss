package org.minilis.api.services;

import org.minilis.entity.config.Language;

/**
 * The Interface for LanguageService.
 */
public interface ILanguageService extends IService<Language>{

}
