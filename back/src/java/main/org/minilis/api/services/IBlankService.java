package org.minilis.api.services;

import org.minilis.entity.dics.Blank;

/**
 * The Interface for BlankService.
 */
public interface IBlankService extends IService<Blank>{

}
