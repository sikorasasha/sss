package org.minilis.api.services;

import org.minilis.api.services.IService;
import org.minilis.entity.security.Account;

/**
 * The Interface for AccountService.
 */
public interface IAccountService extends IService<Account>{

}
