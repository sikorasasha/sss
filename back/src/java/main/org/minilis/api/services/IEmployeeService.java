package org.minilis.api.services;

import org.minilis.entity.dics.Employee;

/**
 * The Interface for EmployeeService.
 */
public interface IEmployeeService extends IService<Employee>{

}
