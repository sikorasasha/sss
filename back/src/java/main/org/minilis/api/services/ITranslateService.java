package org.minilis.api.services;

import org.minilis.entity.config.Translation;

/**
 * The Interface for TranslateService.
 */
public interface ITranslateService extends IService<Translation>{

}
