package org.minilis.api.services;

import org.minilis.entity.dics.CollectionPlace;

/**
 * The Interface for CollectionPlaceService.
 */
public interface ICollectionPlaceService extends IService<CollectionPlace>{

}
