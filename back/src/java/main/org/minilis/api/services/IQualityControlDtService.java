package org.minilis.api.services;

import org.minilis.entity.jors.QualityControlDt;

/**
 * The Interface for QualityControlDtService.
 */
public interface IQualityControlDtService extends IService<QualityControlDt>{

}
