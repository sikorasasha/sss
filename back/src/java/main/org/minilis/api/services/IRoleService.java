package org.minilis.api.services;

import org.minilis.api.services.IService;
import org.minilis.entity.security.Role;

/**
 * The Interface for RoleService.
 */
public interface IRoleService extends IService<Role> {

}
