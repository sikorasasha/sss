/**
 * 
 */
package org.minilis.api.helper;

import java.util.HashMap;

/**
 * @author gum
 *
 */
public interface IMailHelper {

	public String getSubject(String email, HashMap<String,String> parameters, String emailType);
	
	public String getContext(String email, HashMap<String,String> parameters, String emailType);
}
