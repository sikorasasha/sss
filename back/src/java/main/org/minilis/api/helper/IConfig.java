package org.minilis.api.helper;

import java.util.Map;

import org.minilis.entity.dics.Employee;
import org.minilis.entity.security.Account;

/**
 * The Interface with configuration for setting language, getting Ean8, getting
 * account id, deleting account directory.
 *
 */
public interface IConfig {

	/**
	 * Gets the ean8.
	 *
	 * @param code
	 * @return Ean8 code
	 */
	public String getEan8(String code);

	/**
	 * Gets the account id.
	 *
	 * @return return account id
	 */
	public String getAccountPath(String idAccount);

	/**
	 * Deleting account directory.
	 *
	 * @param account
	 *            is current account id
	 */
	public void delAccDir(String account);

	/**
	 * @param email
	 * @return
	 */
	public Boolean sendMail(String email, String subject, String context);


	/**
	 * @param subdome
	 * @return
	 */
	public Boolean validationSubDomen(String subdome);

	/**
	 * @param
	 * @return URL of current account
	 */
	public String getHostURL();

	/**
	 * @param
	 * @return current autentificated employee
	 */
	public Employee getCurrentEmployee();

	/**
	 * @param
	 * @return current employee by email
	 */
	public Employee getCurrentEmployee(String email);


	public Map<String, String> getAuthemail();


	public Map<String, String> getAuthpass();

	public void delAuthemail(String key);


	public void delAuthpass(String key);

	public void putAuthemail(String key, String value);

	public void putAuthpass(String key, String value);


	/**
	 * @param account
	 * @return
	 */
	Boolean checkActivationTimeout(Account account);

	/**
	 * @param employee
	 * @return
	 */
	Boolean checkRecoverLinkTimeout(Employee employee);


	public String getUUID(String email, String pass);

	
	public String alert(String message);

}
