/**
 * 
 */
package org.minilis.api.helper;

import org.springframework.core.io.Resource;

/**
 * @author sasha
 *
 */
public interface IContextHelper {

	/**
	 * @param name
	 * @return
	 */
	public String getRealPath(String name);

	/**
	 * @param name
	 * @return
	 */
	public Resource getResource(String name);

}
