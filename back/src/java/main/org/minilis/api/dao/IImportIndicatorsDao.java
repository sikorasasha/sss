package org.minilis.api.dao;

import org.minilis.entity.dics.ImportIndicators;

/**
 * The Interface for ImportIndicatorsDao.
 */
public interface IImportIndicatorsDao extends IDao<ImportIndicators>{

}
