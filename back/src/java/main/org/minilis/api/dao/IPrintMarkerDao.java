/**
 * 
 */
package org.minilis.api.dao;

import org.minilis.entity.dics.PrintMarker;

/**
 * @author sasha
 *
 */
public interface IPrintMarkerDao extends IDao<PrintMarker>{

}
