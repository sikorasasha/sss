package org.minilis.api.dao;

import org.minilis.entity.dics.Blank;

/**
 * The Interface for BlankDao.
 */
public interface IBlankDao extends IDao<Blank>{

}
