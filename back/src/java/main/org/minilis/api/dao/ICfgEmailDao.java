package org.minilis.api.dao;

import org.minilis.api.dao.IDao;
import org.minilis.entity.config.CfgEmail;

/**
 * The Interface for CfgEmailDao.
 */
public interface ICfgEmailDao extends IDao<CfgEmail>{

}
