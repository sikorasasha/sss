package org.minilis.api.dao;

import org.minilis.entity.dics.Analizator;

/**
 * The Interface for AnalizatorDao.
 */
public interface IAnalizatorDao extends IDao<Analizator>{

}
