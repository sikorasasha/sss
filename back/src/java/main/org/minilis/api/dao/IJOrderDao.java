package org.minilis.api.dao;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.minilis.entity.jors.JOrder;

/**
 * The Interface for JOrderDao.
 */
public interface IJOrderDao extends IDao<JOrder>{

	/**
	 * Select JResults that related with current JOrder from DB.
	 *
	 * @param from is how many elements will selected.
	 * @param first is the first element from which the data will selected.
	 * @param filter is the list of criteria.
	 * @return all selected elements.
	 */
	public List<JOrder> selectJResult(int from, int first,
			List<Criterion> filter);

	/**
	 * Select JResults, Indicators that related with current JOrder from DB.
	 *
	 * @param from is how many elements will selected.
	 * @param first is the first element from which the data will selected.
	 * @param filter is the list of criteria.
	 * @return all selected elements.
	 */
	public List<JOrder> selectJResultIndicator(int from, int first,
			List<Criterion> filter);

}
