package org.minilis.api.dao;

import org.minilis.entity.dics.GrpIndicator;

/**
 * The Interface for GrpIndicatorDao.
 */
public interface IGrpIndicatorDao extends IDao<GrpIndicator>{

}
