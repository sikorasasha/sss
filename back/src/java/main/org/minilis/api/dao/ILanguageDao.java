package org.minilis.api.dao;

import org.minilis.api.dao.IDao;
import org.minilis.entity.config.Language;

/**
 * The Interface for LanguageDao.
 */
public interface ILanguageDao extends IDao<Language>{

}
