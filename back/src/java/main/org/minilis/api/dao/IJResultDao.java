package org.minilis.api.dao;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.minilis.entity.jors.JResult;

/**
 * The Interface for JResultDao.
 */
public interface IJResultDao extends IDao<JResult>{

	/**
	 * @param from
	 * @param first
	 * @param filter
	 * @return
	 */
	public List<JResult> selectGroupByHDIDBlankID(int from, int first,
			List<Criterion> filter);

}
