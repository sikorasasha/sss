package org.minilis.api.dao;

import org.minilis.entity.dics.Employee;

/**
 * The Interface for EmployeeDao.
 */
public interface IEmployeeDao extends IDao<Employee>{

}
