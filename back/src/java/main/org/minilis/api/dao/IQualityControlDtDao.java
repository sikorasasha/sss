package org.minilis.api.dao;

import org.minilis.entity.jors.QualityControlDt;

/**
 * The Interface for QualityControlDtDao.
 */
public interface IQualityControlDtDao extends IDao<QualityControlDt>{

}
