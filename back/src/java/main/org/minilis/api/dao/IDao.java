package org.minilis.api.dao;

import java.util.List;

import org.hibernate.criterion.Criterion;

/**
 * The Base Interface for Dao.
 *
 * @param <T>
 *            the generic type
 */
public interface IDao<T> {

	/**
	 * Insert object in DB.
	 *
	 * @param obj
	 *            is inserting object
	 * @return inserted object
	 */
	public T insert(T obj);

	/**
	 * Update object in DB
	 *
	 * @param obj
	 *            is replacing object.
	 * @return updated object.
	 */
	public T update(T obj);

	/**
	 * Delete object from DB.
	 *
	 * @param obj
	 *            is deleting object.
	 */
	public void delete(T obj);

	/**
	 * Select objects from DB.
	 *
	 * @param count
	 *            is how many elements will selected.
	 * @param from
	 *            is the first element from which the data will selected.
	 * @param filter
	 *            is the list of criteria.
	 * @return all selected elements.
	 */
	public List<T> select(int count, int from, List<Criterion> filter);

	/**
	 * Select objects from DB.
	 * 
	 * @param filter
	 *            is the list of criteria.
	 * @return all selected elements.
	 */
	public List<T> select(List<Criterion> filter);

	/**
	 * @param account
	 */
	public void deleteByAccId(String account);

	
	public T selectById(String id);
}
