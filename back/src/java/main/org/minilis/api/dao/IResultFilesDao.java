package org.minilis.api.dao;

import org.minilis.entity.jors.ResultFiles;

/**
 * The Interface for ResultFilesDao.
 */
public interface IResultFilesDao extends IDao<ResultFiles>{

}
