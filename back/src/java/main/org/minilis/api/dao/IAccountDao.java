package org.minilis.api.dao;

import org.minilis.api.dao.IDao;
import org.minilis.entity.security.Account;

/**
 * The Interface for AccountDao.
 */
public interface IAccountDao extends IDao<Account>{

}
