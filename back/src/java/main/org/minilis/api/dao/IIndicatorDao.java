package org.minilis.api.dao;

import org.minilis.entity.dics.Indicator;

/**
 * The Interface for IndicatorDao.
 */
public interface IIndicatorDao extends IDao<Indicator>{

}
