package org.minilis.api.dao;

import org.minilis.entity.jors.QualityControl;

/**
 * The Interface for QualityControlDao.
 */
public interface IQualityControlDao extends IDao<QualityControl>{

}
