package org.minilis.api.dao;

import org.minilis.api.dao.IDao;
import org.minilis.entity.config.Translation;

/**
 * The Interface for TranslateDao.
 */
public interface ITranslateDao extends IDao<Translation>{

}
