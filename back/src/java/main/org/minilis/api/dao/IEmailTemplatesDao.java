/**
 * 
 */
package org.minilis.api.dao;

import org.minilis.entity.config.EmailTemplates;

/**
 * @author sasha
 *
 */
public interface IEmailTemplatesDao extends IDao<EmailTemplates>{

}
