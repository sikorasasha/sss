package org.minilis.api.dao;

import org.minilis.entity.jors.JLabProccess;

/**
 * The Interface for IJLabProccessDao.
 *
 * @author sasha
 */
public interface IJLabProccessDao extends IDao<JLabProccess>{

}
