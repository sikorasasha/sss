package org.minilis.api.dao;

import org.minilis.entity.dics.CollectionPlace;

/**
 * The Interface for CollectionPlaceDao.
 */
public interface ICollectionPlaceDao extends IDao<CollectionPlace>{

}
