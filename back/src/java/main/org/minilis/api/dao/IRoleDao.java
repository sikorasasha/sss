package org.minilis.api.dao;

import org.minilis.api.dao.IDao;
import org.minilis.entity.security.Role;

/**
 * The Interface for RoleDao.
 */
public interface IRoleDao extends IDao<Role> {

}
