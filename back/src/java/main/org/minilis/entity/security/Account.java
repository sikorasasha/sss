package org.minilis.entity.security;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.minilis.entity.dics.Employee;

/**
 * The Entity for Account.
 */
@Entity
public class Account {

	/** The unique id. */
	@Id
	@GenericGenerator(name = "unique_id", strategy = "uuid")
	@GeneratedValue(generator = "unique_id")
	@Column(name = "id", nullable = false, length = 32, unique = true)
	private String id;

	/** The name. */
	@Column(name = "name", nullable = false, length = 50, unique = false)
	private String name;

	/** The domain. */
	@Column(name = "domain", nullable = false, length = 32, unique = true)
	private String domain;

	/** The locale. */
	@Column(name = "locale", nullable = false, length = 32, unique = false)
	private String locale;

	/** The role is the join column to Role. */
	@OneToOne
	@JoinColumn(name = "roleId", nullable = false, unique = false)
	private Role role;

	/** The address. */
	@Column(name = "address", nullable = true, length = 255, unique = false)
	private String address;

	/** The is active. */
	@Column(name = "isactive", nullable = false, unique = false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean isActive = true;

	/** The is activated. */
	@Column(name = "isActivated", nullable = false, unique = false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean isActivated = false;

	/** The date reg. */
	@Column(name = "dateReg", nullable = true, unique = false)
	private Date dateReg;

	/** The ip. */
	@Column(name = "ip", nullable = true, length = 20, unique = false)
	private String ip;

	/** The date active. */
	@Column(name = "dateActive", nullable = true, unique = false)
	private Date dateActive;

	/** The date last login. */
	@Column(name = "dateLastLogin", nullable = true, unique = false)
	private Date dateLastLogin;

	/** The logo. */
	@Column(name = "logo", nullable = true, unique = false)
	private Byte[] logo;

	/** The blank details. */
	@Column(name = "blankDetails", nullable = true, length = 255, unique = false)
	private String blankDetails;

	/** The blank caption. */
	@Column(name = "blankCaption", nullable = true, length = 255, unique = false)
	private String blankCaption;

	/** The type bar code. */
	@Column(name = "typeBarCode", nullable = false, length = 1, unique = false)
	private Integer typeBarCode=3;

	/** The barcode length. */
	@Column(name = "barcodeLength", nullable = false, length = 10, unique = false)
	private Integer barcodeLength=2;

	/** The listEmployee is mappeed by account to list of Employees. */
	@OneToMany(mappedBy = "account", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Employee> listEmployee;

	/**
	 * Gets the list employee.
	 *
	 * @return the list employee
	 */
	public List<Employee> getListEmployee() {
		return listEmployee;
	}

	/**
	 * Sets the list employee.
	 *
	 * @param listEmployee the new list employee
	 */
	public void setListEmployee(List<Employee> listEmployee) {
		this.listEmployee = listEmployee;
	}

	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 *
	 * @param address the new address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Gets the checks if is active.
	 *
	 * @return the checks if is active
	 */
	public Boolean getIsActive() {
		return isActive;
	}

	/**
	 * Sets the checks if is active.
	 *
	 * @param isActive the new checks if is active
	 */
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * Gets the checks if is activated.
	 *
	 * @return the checks if is activated
	 */
	public Boolean getIsActivated() {
		return isActivated;
	}

	/**
	 * Sets the checks if is activated.
	 *
	 * @param isActivated the new checks if is activated
	 */
	public void setIsActivated(Boolean isActivated) {
		this.isActivated = isActivated;
	}

	/**
	 * Gets the date reg.
	 *
	 * @return the date reg
	 */
	public Date getDateReg() {
		return dateReg;
	}

	/**
	 * Sets the date reg.
	 *
	 * @param dateReg the new date reg
	 */
	public void setDateReg(Date dateReg) {
		this.dateReg = dateReg;
	}

	/**
	 * Gets the ip.
	 *
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * Sets the ip.
	 *
	 * @param ip the new ip
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	/**
	 * Gets the date active.
	 *
	 * @return the date active
	 */
	public Date getDateActive() {
		return dateActive;
	}

	/**
	 * Sets the date active.
	 *
	 * @param dateActive the new date active
	 */
	public void setDateActive(Date dateActive) {
		this.dateActive = dateActive;
	}

	/**
	 * Gets the date last login.
	 *
	 * @return the date last login
	 */
	public Date getDateLastLogin() {
		return dateLastLogin;
	}

	/**
	 * Sets the date last login.
	 *
	 * @param dateLastLogin the new date last login
	 */
	public void setDateLastLogin(Date dateLastLogin) {
		this.dateLastLogin = dateLastLogin;
	}

	/**
	 * Gets the logo.
	 *
	 * @return the logo
	 */
	public Byte[] getLogo() {
		return logo;
	}

	/**
	 * Sets the logo.
	 *
	 * @param logo the new logo
	 */
	public void setLogo(Byte[] logo) {
		this.logo = logo;
	}

	/**
	 * Gets the blank details.
	 *
	 * @return the blank details
	 */
	public String getBlankDetails() {
		return blankDetails;
	}

	/**
	 * Sets the blank details.
	 *
	 * @param blankDetails the new blank details
	 */
	public void setBlankDetails(String blankDetails) {
		this.blankDetails = blankDetails;
	}

	/**
	 * Gets the blank caption.
	 *
	 * @return the blank caption
	 */
	public String getBlankCaption() {
		return blankCaption;
	}

	/**
	 * Sets the blank caption.
	 *
	 * @param blankCaption the new blank caption
	 */
	public void setBlankCaption(String blankCaption) {
		this.blankCaption = blankCaption;
	}

	/**
	 * Gets the type bar code.
	 *
	 * @return the type bar code
	 */
	public Integer getTypeBarCode() {
		return typeBarCode;
	}

	/**
	 * Gets the barcode length.
	 *
	 * @return the barcode length
	 */
	public Integer getBarcodeLength() {
		return barcodeLength;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the role.
	 *
	 * @return the role
	 */
	public Role getRole() {
		return role;
	}

	/**
	 * Sets the role.
	 *
	 * @param role the new role
	 */
	public void setRole(Role role) {
		this.role = role;
	}

	/**
	 * Gets the domain.
	 *
	 * @return the domain
	 */
	public String getDomain() {
		return domain;
	}

	/**
	 * Sets the domain.
	 *
	 * @param domain the new domain
	 */
	public void setDomain(String domain) {
		this.domain = domain;
	}

	/**
	 * Gets the locale.
	 *
	 * @return the locale
	 */
	public String getLocale() {
		return locale;
	}

	/**
	 * Sets the locale.
	 *
	 * @param locale the new locale
	 */
	public void setLocale(String locale) {
		this.locale = locale;
	}

	/**
	 * Sets the type bar code.
	 *
	 * @param typeBarCode the new type bar code
	 */
	public void setTypeBarCode(Integer typeBarCode) {
		this.typeBarCode = typeBarCode;
	}

	/**
	 * Sets the barcode length.
	 *
	 * @param barcodeLength the new barcode length
	 */
	public void setBarcodeLength(Integer barcodeLength) {
		this.barcodeLength = barcodeLength;
	}
}
