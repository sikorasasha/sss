/**
 * 
 */
package org.minilis.entity.config;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

/**
 * @author gum
 *
 */

@Entity
public class EmailTemplates {
	
	/** The unique id. */
	@Id
	@GenericGenerator(name = "unique_id", strategy = "uuid")
    @GeneratedValue(generator = "unique_id")
	@Column(name = "id", nullable = false, length = 32, unique=true)
	private String id;	
	
	/** The emailName (activation_subject_en, registration_context_ua, etc). */
	@Column(name = "emailName", nullable = false, length = 50, unique=true)
	private String emailName;
	
	/** The emailBody. */
	@Type(type="text")
	@Column(name = "body", nullable = false)
	private String body;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the emailName
	 */
	public String getEmailName() {
		return emailName;
	}

	/**
	 * @param emailName the emailName to set
	 */
	public void setEmailName(String emailName) {
		this.emailName = emailName;
	}

	/**
	 * @return the body
	 */
	public String getBody() {
		return body;
	}

	/**
	 * @param body the body to set
	 */
	public void setBody(String body) {
		this.body = body;
	}
	
	
}
