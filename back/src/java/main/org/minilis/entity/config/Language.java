package org.minilis.entity.config;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.GenericGenerator;

/**
 * The Entity for Language.
 */
@Entity
public class Language {

	/** The unique id. */
	@Id
	@GenericGenerator(name = "unique_id", strategy = "uuid")
    @GeneratedValue(generator = "unique_id")
	@Column(name = "id", nullable = false, length = 32, unique=true)
	private String id;
	
	/** The name. */
	@Column(name = "name", nullable = false, length = 50, unique=true)
	private String name;
	
	/** The translation is mapped by lngid to List of Translations. */
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "lngid")
	private List<Translation> translation;

	/**
	 * Gets the translation.
	 *
	 * @return the translation
	 */
	public List<Translation> getTranslation() {
		return translation;
	}

	/**
	 * Sets the translation.
	 *
	 * @param translation the new translation
	 */
	public void setTranslation(List<Translation> translation) {
		this.translation = translation;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	
}
