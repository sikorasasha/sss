package org.minilis.entity.config;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;

 /**
 * The Entity for Translation.
 */
@Entity
public class Translation {

	/** The unique id. */
	@Id
	@GenericGenerator(name = "unique_id", strategy = "uuid")
    @GeneratedValue(generator = "unique_id")
	@Column(name = "id", nullable = false, length = 32, unique=true)
	private String id;
	
	/** The lngid is the join column to Language. */
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "lngid", nullable = false)
	private Language lngid;
	
	/** The key. */
	@Column(name = "key", nullable = false, length = 50, unique=false)
	private String key;
	
	/** The value. */
	@Column(name = "value", nullable = false, length = 255, unique=false)
	private String value;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the lngid.
	 *
	 * @return the lngid
	 */
	public Language getLngid() {
		return lngid;
	}

	/**
	 * Sets the lngid.
	 *
	 * @param lngid the new lngid
	 */
	public void setLngid(Language lngid) {
		this.lngid = lngid;
	}

	/**
	 * Gets the key.
	 *
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Sets the key.
	 *
	 * @param key the new key
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	
}
