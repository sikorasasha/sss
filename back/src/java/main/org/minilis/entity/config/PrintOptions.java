package org.minilis.entity.config;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;
import org.minilis.entity.dics.Employee;
import org.minilis.entity.security.Account;

/**
 * The Entity for PrintOptions.
 *
 * @author sasha
 */
@Entity
public class PrintOptions {

	/** The unique id. */
	@Id
	@GenericGenerator(name = "unique_id", strategy = "uuid")
    @GeneratedValue(generator = "unique_id")
	@Column(name = "id", nullable = false, length = 32, unique=true)
	private String id;

	/** The account is the join column to Account. */
	@OneToOne
	@JoinColumn(name="accid", nullable=false, unique=false)
	private Account account;

	/** The employee is the join column to Employee. */
	@OneToOne
	@JoinColumn(name="employeeId", nullable=false, unique=false)
	private Employee employee;

	/** The manager id. */
	@Column(name = "managerId", nullable = false, length = 32, unique=false)
	private String managerId;

	/** The printer name. */
	@Column(name = "printerName", nullable = false, length = 64, unique=false)
	private String printerName;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the account.
	 *
	 * @return the account
	 */
	public Account getAccount() {
		return account;
	}

	/**
	 * Sets the account.
	 *
	 * @param account the new account
	 */
	public void setAccount(Account account) {
		this.account = account;
	}

	/**
	 * Gets the employee id.
	 *
	 * @return the employee id
	 */
	public Employee getEmployeeId() {
		return employee;
	}

	/**
	 * Sets the employee id.
	 *
	 * @param employee the new employee id
	 */
	public void setEmployeeId(Employee employee) {
		this.employee = employee;
	}

	/**
	 * Gets the manager id.
	 *
	 * @return the manager id
	 */
	public String getManagerId() {
		return managerId;
	}

	/**
	 * Sets the manager id.
	 *
	 * @param managerId the new manager id
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	/**
	 * Gets the printer name.
	 *
	 * @return the printer name
	 */
	public String getPrinterName() {
		return printerName;
	}

	/**
	 * Sets the printer name.
	 *
	 * @param printerName the new printer name
	 */
	public void setPrinterName(String printerName) {
		this.printerName = printerName;
	}

}
