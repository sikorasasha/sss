package org.minilis.entity.config;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.minilis.entity.security.Account;

/**
 * The Entity for CfgEmail.
 */
@Entity
public class CfgEmail {

	/** The unique id. */
	@Id
	@GenericGenerator(name = "unique_id", strategy = "uuid")
    @GeneratedValue(generator = "unique_id")
	@Column(name = "id", nullable = false, length = 32, unique=true)
	private String id;

	/** The account is the join column to Account. */
	@OneToOne
	@JoinColumn(name="accid", nullable=false, unique=false)
	private Account account;

	/** The smtp. */
	@Column(name = "smtp", nullable = true, length = 50, unique=false)
	private String smtp;

	/** The port. */
	@Column(name = "port", nullable = true, length = 5, unique=false)
	private Integer port;

	/** The login. */
	@Column(name = "login", nullable = true, length = 50, unique=false)
	private String login;

	/** The password. */
	@Column(name = "pass", nullable = true, length = 50, unique=false)
	private String pass;

	/** The subject. */
	@Column(name = "subject", nullable = false, length = 100, unique=false)
	private String subject;

	/** The text. */
	@Column(name = "text", nullable = true, length = 255, unique=false)
	private String text;

	/** The is ssl. */
	@Column(name = "isSsl", nullable = false, unique=false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean isSsl=false;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the account.
	 *
	 * @return the account
	 */
	public Account getAccount() {
		return account;
	}

	/**
	 * Sets the account.
	 *
	 * @param account the new account
	 */
	public void setAccount(Account account) {
		this.account = account;
	}

	/**
	 * Gets the smtp.
	 *
	 * @return the smtp
	 */
	public String getSmtp() {
		return smtp;
	}

	/**
	 * Sets the smtp.
	 *
	 * @param smtp the new smtp
	 */
	public void setSmtp(String smtp) {
		this.smtp = smtp;
	}

	/**
	 * Gets the port.
	 *
	 * @return the port
	 */
	public Integer getPort() {
		return port;
	}

	/**
	 * Sets the port.
	 *
	 * @param port the new port
	 */
	public void setPort(Integer port) {
		this.port = port;
	}

	/**
	 * Gets the login.
	 *
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * Sets the login.
	 *
	 * @param login the new login
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * Gets the pass.
	 *
	 * @return the pass
	 */
	public String getPass() {
		return pass;
	}

	/**
	 * Sets the pass.
	 *
	 * @param pass the new pass
	 */
	public void setPass(String pass) {
		this.pass = pass;
	}

	/**
	 * Gets the subject.
	 *
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * Sets the subject.
	 *
	 * @param subject the new subject
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * Gets the text.
	 *
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * Sets the text.
	 *
	 * @param text the new text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * Gets the checks if is ssl.
	 *
	 * @return the checks if is ssl
	 */
	public Boolean getIsSsl() {
		return isSsl;
	}

	/**
	 * Sets the checks if is ssl.
	 *
	 * @param isSsl the new checks if is ssl
	 */
	public void setIsSsl(Boolean isSsl) {
		this.isSsl = isSsl;
	}


}
