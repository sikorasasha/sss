package org.minilis.entity.jors;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;
import org.minilis.entity.dics.CollectionPlace;
import org.minilis.entity.dics.Employee;
import org.minilis.entity.security.Account;

/**
 * The Entity for JOrder.
 */
@Entity
public class JOrder {


	/** The unique id. */
	@Id
	@GenericGenerator(name = "unique_id", strategy = "uuid")
    @GeneratedValue(generator = "unique_id")
	@Column(name = "id", nullable = false, length = 32, unique=true)
	private String id;
	
	/** The num. */
	@Column(name = "num", nullable = false, length = 8, unique=false)
	private String num;
	
	/** The date time. */
	@Column(name = "dateTime", nullable = false, unique=false)
	private Date dateTime;
	
	/** The patient name. */
	@Column(name = "patient", nullable = true, length = 50, unique=false)
	private String patient;
	
	/** The sex. */
	@Column(name = "sex", nullable = false, length = 1, unique=false)
	private Integer sex;
	
	/** The birth date. */
	@Column(name = "birthDate", nullable = true, unique=false)
	private Date birthDate;
	
	/** The email. */
	@Column(name = "email", nullable = true, length = 50, unique=false)
	private String email;
	
	/** The card code. */
	@Column(name = "cardCode", nullable = true, length = 50, unique=false)
	private String cardCode;
	
	/** The doctor name. */
	@Column(name = "doctor", nullable = true, length = 50, unique=false)
	private String doctor;
	
	/** The material description. */
	@Column(name = "materialDescr", nullable = true, length = 2000, unique=false)
	private String materialDescr;
	
	/** The description. */
	@Column(name = "descr", nullable = true, length = 2000, unique=false)
	private String descr;
	
	/** The sum. */
	@Column(name = "sum_", nullable = true, unique=false, precision=18, scale=2)
	private Double sum_;
	
	/** The collectionPlace is the join column to ColectionPlace. */
	@OneToOne
	@JoinColumn(name="collectionPlace", nullable = false, unique=false)
	private CollectionPlace collectionPlace;
	
	/** The employee is the join column to Employee. */
	@OneToOne
	@JoinColumn(name="employee", nullable=false, unique=false)
	private Employee employee;
	
	/** The jResult is mapped by hdId to List of JResults. */
	@OneToMany(mappedBy="hdId",cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private List<JResult> jResult;
	
	/** The account is the join column to Account. */
	@OneToOne
	@JoinColumn(name="accid", nullable=false, unique=false)
	private Account account;

	/**
	 * Gets the account.
	 *
	 * @return the account
	 */
	public Account getAccount() {
		return account;
	}

	/**
	 * Sets the account.
	 *
	 * @param account the new account
	 */
	public void setAccount(Account account) {
		this.account = account;
	}

	/**
	 * Sets the sex.
	 *
	 * @param sex the new sex
	 */
	public void setSex(Integer sex) {
		this.sex = sex;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the num.
	 *
	 * @return the num
	 */
	public String getNum() {
		return num;
	}

	/**
	 * Sets the num.
	 *
	 * @param num the new num
	 */
	public void setNum(String num) {
		this.num = num;
	}

	/**
	 * Gets the date time.
	 *
	 * @return the date time
	 */
	public Date getDateTime() {
		return dateTime;
	}

	/**
	 * Sets the date time.
	 *
	 * @param dateTime the new date time
	 */
	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	/**
	 * Gets the patient.
	 *
	 * @return the patient
	 */
	public String getPatient() {
		return patient;
	}

	/**
	 * Sets the patient.
	 *
	 * @param patient the new patient
	 */
	public void setPatient(String patient) {
		this.patient = patient;
	}

	/**
	 * Gets the sex.
	 *
	 * @return the sex
	 */
	public Integer getSex() {
		return sex;
	}


	/**
	 * Gets the birth date.
	 *
	 * @return the birth date
	 */
	public Date getBirthDate() {
		return birthDate;
	}

	/**
	 * Sets the birth date.
	 *
	 * @param birthDate the new birth date
	 */
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the card code.
	 *
	 * @return the card code
	 */
	public String getCardCode() {
		return cardCode;
	}

	/**
	 * Sets the card code.
	 *
	 * @param cardCode the new card code
	 */
	public void setCardCode(String cardCode) {
		this.cardCode = cardCode;
	}

	/**
	 * Gets the doctor.
	 *
	 * @return the doctor
	 */
	public String getDoctor() {
		return doctor;
	}

	/**
	 * Sets the doctor.
	 *
	 * @param doctor the new doctor
	 */
	public void setDoctor(String doctor) {
		this.doctor = doctor;
	}

	/**
	 * Gets the material descr.
	 *
	 * @return the material descr
	 */
	public String getMaterialDescr() {
		return materialDescr;
	}

	/**
	 * Sets the material descr.
	 *
	 * @param materialDescr the new material descr
	 */
	public void setMaterialDescr(String materialDescr) {
		this.materialDescr = materialDescr;
	}

	/**
	 * Gets the descr.
	 *
	 * @return the descr
	 */
	public String getDescr() {
		return descr;
	}

	/**
	 * Sets the descr.
	 *
	 * @param descr the new descr
	 */
	public void setDescr(String descr) {
		this.descr = descr;
	}

	/**
	 * Gets the sum_.
	 *
	 * @return the sum_
	 */
	public Double getSum_() {
		return sum_;
	}

	/**
	 * Sets the sum_.
	 *
	 * @param sum_ the new sum_
	 */
	public void setSum_(Double sum_) {
		this.sum_ = sum_;
	}

	/**
	 * Gets the collection place.
	 *
	 * @return the collection place
	 */
	public CollectionPlace getCollectionPlace() {
		return collectionPlace;
	}

	/**
	 * Sets the collection place.
	 *
	 * @param collectionPlace the new collection place
	 */
	public void setCollectionPlace(CollectionPlace collectionPlace) {
		this.collectionPlace = collectionPlace;
	}

	/**
	 * Gets the employee.
	 *
	 * @return the employee
	 */
	public Employee getEmployee() {
		return employee;
	}

	/**
	 * Sets the employee.
	 *
	 * @param employee the new employee
	 */
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	/**
	 * Gets the j result.
	 *
	 * @return the j result
	 */
	public List<JResult> getjResult() {
		return jResult;
	}

	/**
	 * Sets the j result.
	 *
	 * @param jResult the new j result
	 */
	public void setjResult(List<JResult> jResult) {
		this.jResult = jResult;
	}
	
	
}
