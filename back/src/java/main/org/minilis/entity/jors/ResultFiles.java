package org.minilis.entity.jors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;
import org.minilis.entity.security.Account;

/**
 * The Entity for ResultFiles.
 */
@Entity
public class ResultFiles {

	/** The unique id. */
	@Id
	@GenericGenerator(name = "unique_id", strategy = "uuid")
    @GeneratedValue(generator = "unique_id")
	@Column(name = "id", nullable = false, length = 32, unique=true)
	private String id;
	
	/** The JResult. */
	@OneToOne
	@JoinColumn(name = "hdId", nullable=false)
	private JResult JResult;
	
	/** The name. */
	@Column(name = "name", nullable = false, length = 20, unique=false)
	private String name;
	
	/** The body. */
	@Column(name = "body", nullable = true, unique=false)
	private Byte[] body;
	
	/** The account is the join column to Account. */
	@OneToOne
	@JoinColumn(name="accid", nullable=false, unique=false)
	private Account account;

	/**
	 * Gets the account.
	 *
	 * @return the account
	 */
	public Account getAccount() {
		return account;
	}

	/**
	 * Sets the account.
	 *
	 * @param account the new account
	 */
	public void setAccount(Account account) {
		this.account = account;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the j result.
	 *
	 * @return the j result
	 */
	public JResult getJResult() {
		return JResult;
	}

	/**
	 * Sets the j result.
	 *
	 * @param jResult the new j result
	 */
	public void setJResult(JResult jResult) {
		JResult = jResult;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the body.
	 *
	 * @return the body
	 */
	public Byte[] getBody() {
		return body;
	}

	/**
	 * Sets the body.
	 *
	 * @param body the new body
	 */
	public void setBody(Byte[] body) {
		this.body = body;
	}
	
	
}
