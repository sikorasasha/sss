package org.minilis.entity.jors;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.minilis.entity.dics.Employee;
import org.minilis.entity.security.Account;

/**
 * The Entity for QualityControlDt.
 */
@Entity
public class QualityControlDt {

	/** The unique id. */
	@Id
	@GenericGenerator(name = "unique_id", strategy = "uuid")
    @GeneratedValue(generator = "unique_id")
	@Column(name = "id", nullable = false, length = 32, unique=true)
	private String id;
	
	/** The hdId is the join column to QualityContorl. */
	@ManyToOne
	@JoinColumn(name = "hdId", nullable=false)
	private QualityControl hdId;
	
	/** The date time. */
	@Column(name = "dateTime", nullable = false, unique=false)
	private Date dateTime;
	
	/** The employee is the join column to Employee. */
	@OneToOne
	@JoinColumn(name = "employee", nullable = false, unique=false)
	private Employee employee;
	
	/** The result. */
	@Column(name = "result", nullable = false, unique=false, precision=15, scale=4)
	private Double result;
	
	/** The result double. */
	@Column(name = "resultDouble", nullable = true, unique=false, precision=15, scale=4)
	private Double resultDouble;
	
	/** The is accepted. */
	@Column(name = "isAccepted", nullable = false, unique=false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean isAccepted;
	
	/** The error code. */
	@Column(name = "errorCode", nullable = true, length = 20, unique=false)
	private String errorCode;
	
	/** The description. */
	@Column(name = "descr", nullable = true, length = 250, unique=false)
	private String descr;
	
	/** The account is the join column to Account. */
	@OneToOne
	@JoinColumn(name="accid", nullable=false, unique=false)
	private Account account;

	/**
	 * Gets the account.
	 *
	 * @return the account
	 */
	public Account getAccount() {
		return account;
	}

	/**
	 * Sets the account.
	 *
	 * @param account the new account
	 */
	public void setAccount(Account account) {
		this.account = account;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the hd id.
	 *
	 * @return the hd id
	 */
	public QualityControl getHdId() {
		return hdId;
	}

	/**
	 * Sets the hd id.
	 *
	 * @param hdId the new hd id
	 */
	public void setHdId(QualityControl hdId) {
		this.hdId = hdId;
	}

	/**
	 * Gets the date time.
	 *
	 * @return the date time
	 */
	public Date getDateTime() {
		return dateTime;
	}

	/**
	 * Sets the date time.
	 *
	 * @param dateTime the new date time
	 */
	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	/**
	 * Gets the employee.
	 *
	 * @return the employee
	 */
	public Employee getEmployee() {
		return employee;
	}

	/**
	 * Sets the employee.
	 *
	 * @param employee the new employee
	 */
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	/**
	 * Gets the result.
	 *
	 * @return the result
	 */
	public Double getResult() {
		return result;
	}

	/**
	 * Sets the result.
	 *
	 * @param result the new result
	 */
	public void setResult(Double result) {
		this.result = result;
	}

	/**
	 * Gets the result double.
	 *
	 * @return the result double
	 */
	public Double getResultDouble() {
		return resultDouble;
	}

	/**
	 * Sets the result double.
	 *
	 * @param resultDouble the new result double
	 */
	public void setResultDouble(Double resultDouble) {
		this.resultDouble = resultDouble;
	}

	/**
	 * Gets the checks if is accepted.
	 *
	 * @return the checks if is accepted
	 */
	public Boolean getIsAccepted() {
		return isAccepted;
	}

	/**
	 * Sets the checks if is accepted.
	 *
	 * @param isAccepted the new checks if is accepted
	 */
	public void setIsAccepted(Boolean isAccepted) {
		this.isAccepted = isAccepted;
	}

	/**
	 * Gets the error code.
	 *
	 * @return the error code
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * Sets the error code.
	 *
	 * @param errorCode the new error code
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * Gets the descr.
	 *
	 * @return the descr
	 */
	public String getDescr() {
		return descr;
	}

	/**
	 * Sets the descr.
	 *
	 * @param descr the new descr
	 */
	public void setDescr(String descr) {
		this.descr = descr;
	}
	
	
}
