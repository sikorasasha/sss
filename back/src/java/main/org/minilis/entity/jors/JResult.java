package org.minilis.entity.jors;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.minilis.entity.dics.Attachment;
import org.minilis.entity.dics.Employee;
import org.minilis.entity.dics.Indicator;
import org.minilis.entity.security.Account;

/**
 * The entity for JResult.
 */
@Entity
public class JResult {

	/** The unique id. */
	@Id
	@GenericGenerator(name = "unique_id", strategy = "uuid")
	@GeneratedValue(generator = "unique_id")
	@Column(name = "id", nullable = false, length = 32, unique = true)
	private String id;

	/** The account is the join column to Account. */
	@OneToOne
	@JoinColumn(name="accid", nullable=false, unique=false)
	private Account account;
	
	/** The hdId is the join column to JOrder. */
	@ManyToOne
	@JoinColumn(name = "hdId", nullable = false)
	private JOrder hdId;
	
	/** The indicator is the join column to Indicator. */
	@OneToOne
	@JoinColumn(name = "indicator", nullable = false, unique = false)
	private Indicator indicator;

	/** The attachment is the join column to Attachment. */
	@OneToMany
	@JoinColumn(name = "attachment", nullable = true)
	private List<Attachment> attachment;

	/** The complexId is the join column to JResult. */
	@ManyToOne
	@JoinColumn(name = "complexId", nullable = false)
	private JResult complexId;

	/** Does is the JResult is Complex. */
	@Column(name = "isComplex", nullable = false, unique = false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean isComplex = false;
	
	/** The jResults is mapped by comlexId to list of JResults. */
	@OneToMany(mappedBy = "complexId", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<JResult> jResults;

	/** The norm text. */
	@Column(name = "normText", nullable = true, unique = false, length = 2000)
	private String normText;

	/** The order. */
	@Column(name = "order_", nullable = true, unique = false, length = 10)
	private Integer order_;

	/** The result. */
	@Column(name = "result", nullable = true, unique = false, length = 2000)
	private String result;

	/** The is out norm. */
	@Column(name = "isOutNorm", nullable = false, unique = false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean isOutNorm;

	/** The date time verified. */
	@Column(name = "dateTimeVerified", nullable = true, unique = false)
	private Date dateTimeVerified;

	/** The employeeVerified is join column to Employee. */
	@OneToOne
	@JoinColumn(name = "employeeVerified", nullable = false, unique = false)
	private Employee employeeVerified;

	/** The sum. */
	@Column(name = "sum_", nullable = true, unique = false, precision = 18, scale = 2)
	private Double sum_;

	/** The labprocId is the join column to JLabProccess. */
	@ManyToOne
	@JoinColumn(name = "labprocId", nullable = true)
	private JLabProccess labprocId;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the indicator.
	 *
	 * @return the indicator
	 */
	public Indicator getIndicator() {
		return indicator;
	}

	/**
	 * Sets the indicator.
	 *
	 * @param indicator the new indicator
	 */
	public void setIndicator(Indicator indicator) {
		this.indicator = indicator;
	}

	/**
	 * Gets the attachment.
	 *
	 * @return the attachment
	 */
	public List<Attachment> getAttachment() {
		return attachment;
	}

	/**
	 * Sets the attachment.
	 *
	 * @param attachment the new attachment
	 */
	public void setAttachment(List<Attachment> attachment) {
		this.attachment = attachment;
	}

	/**
	 * Gets the complex id.
	 *
	 * @return the complex id
	 */
	public JResult getComplexId() {
		return complexId;
	}

	/**
	 * Sets the complex id.
	 *
	 * @param complexId the new complex id
	 */
	public void setComplexId(JResult complexId) {
		this.complexId = complexId;
	}

	/**
	 * Gets the j results.
	 *
	 * @return the j results
	 */
	public List<JResult> getjResults() {
		return jResults;
	}

	/**
	 * Sets the j results.
	 *
	 * @param jResults the new j results
	 */
	public void setjResults(List<JResult> jResults) {
		this.jResults = jResults;
	}

	/**
	 * Gets the checks if is complex.
	 *
	 * @return the checks if is complex
	 */
	public Boolean getIsComplex() {
		return isComplex;
	}

	/**
	 * Sets the checks if is complex.
	 *
	 * @param isComplex the new checks if is complex
	 */
	public void setIsComplex(Boolean isComplex) {
		this.isComplex = isComplex;
	}

	/**
	 * Gets the norm text.
	 *
	 * @return the norm text
	 */
	public String getNormText() {
		return normText;
	}

	/**
	 * Sets the norm text.
	 *
	 * @param normText the new norm text
	 */
	public void setNormText(String normText) {
		this.normText = normText;
	}

	/**
	 * Gets the order_.
	 *
	 * @return the order_
	 */
	public Integer getOrder_() {
		return order_;
	}

	/**
	 * Sets the order_.
	 *
	 * @param order_ the new order_
	 */
	public void setOrder_(Integer order_) {
		this.order_ = order_;
	}

	/**
	 * Gets the result.
	 *
	 * @return the result
	 */
	public String getResult() {
		return result;
	}

	/**
	 * Sets the result.
	 *
	 * @param result the new result
	 */
	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * Gets the checks if is out norm.
	 *
	 * @return the checks if is out norm
	 */
	public Boolean getIsOutNorm() {
		return isOutNorm;
	}

	/**
	 * Sets the checks if is out norm.
	 *
	 * @param isOutNorm the new checks if is out norm
	 */
	public void setIsOutNorm(Boolean isOutNorm) {
		this.isOutNorm = isOutNorm;
	}

	/**
	 * Gets the date time verified.
	 *
	 * @return the date time verified
	 */
	public Date getDateTimeVerified() {
		return dateTimeVerified;
	}

	/**
	 * Sets the date time verified.
	 *
	 * @param dateTimeVerified the new date time verified
	 */
	public void setDateTimeVerified(Date dateTimeVerified) {
		this.dateTimeVerified = dateTimeVerified;
	}

	/**
	 * Gets the employee verified.
	 *
	 * @return the employee verified
	 */
	public Employee getEmployeeVerified() {
		return employeeVerified;
	}

	/**
	 * Sets the employee verified.
	 *
	 * @param employeeVerified the new employee verified
	 */
	public void setEmployeeVerified(Employee employeeVerified) {
		this.employeeVerified = employeeVerified;
	}

	/**
	 * Gets the sum_.
	 *
	 * @return the sum_
	 */
	public Double getSum_() {
		return sum_;
	}

	/**
	 * Sets the sum_.
	 *
	 * @param sum_ the new sum_
	 */
	public void setSum_(Double sum_) {
		this.sum_ = sum_;
	}

	/**
	 * Gets the hd id.
	 *
	 * @return the hd id
	 */
	public JOrder getHdId() {
		return hdId;
	}

	/**
	 * Sets the hd id.
	 *
	 * @param hdId the new hd id
	 */
	public void setHdId(JOrder hdId) {
		this.hdId = hdId;
	}

	/**
	 * Gets the labproc id.
	 *
	 * @return the labproc id
	 */
	public JLabProccess getLabprocId() {
		return labprocId;
	}

	/**
	 * Sets the labproc id.
	 *
	 * @param labprocId the new labproc id
	 */
	public void setLabprocId(JLabProccess labprocId) {
		this.labprocId = labprocId;
	}

	/**
	 * Gets the account.
	 *
	 * @return the account
	 */
	public Account getAccount() {
		return account;
	}

	/**
	 * Sets the account.
	 *
	 * @param account the new account
	 */
	public void setAccount(Account account) {
		this.account = account;
	}



}
