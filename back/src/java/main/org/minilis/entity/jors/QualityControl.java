package org.minilis.entity.jors;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.minilis.entity.dics.Indicator;
import org.minilis.entity.security.Account;

/**
 * The Entity for QualityControl.
 */
@Entity
public class QualityControl {

	/** The unique id. */
	@Id
	@GenericGenerator(name = "unique_id", strategy = "uuid")
	@GeneratedValue(generator = "unique_id")
	@Column(name = "id", nullable = false, length = 32, unique = true)
	private String id;

	/** The qualityControlDt is mapped by hdId to list of QualityControlDts. */
	@OneToMany(mappedBy = "hdId", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<QualityControlDt> qualityControlDt;

	/** The date time. */
	@Column(name = "dateTime", nullable = false, unique = false)
	private Date dateTime;
	
	/** The date time. */
	@Column(name = "dateFirst", nullable = true, unique = false)
	private Date dateFirst;
	
	/** The date time. */
	@Column(name = "dateLast", nullable = true, unique = false)
	private Date dateLast;
	
	/** The color. */
	@Column(name = "color", nullable = true, unique = false, length = 10)
	private Integer color;

	/** The indicator is the join column to Indicator. */
	@OneToOne
	@JoinColumn(name = "indicator", nullable = false, unique = false)
	private Indicator indicator;

	/** The lot. */
	@Column(name = "lot", nullable = true, length = 20, unique = false)
	private String lot;

	/** The low range. */
	@Column(name = "lowRange", nullable = true, unique = false, precision = 15, scale = 4)
	private Double lowRange;

	/** The height range. */
	@Column(name = "heightRange", nullable = true, unique = false, precision = 15, scale = 4)
	private Double heightRange;

	/** The attest value. */
	@Column(name = "attestValue", nullable = true, unique = false, precision = 15, scale = 4)
	private Double attestValue;

	/** The is active. */
	@Column(name = "isActive", nullable = false, unique = false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean isActive = false;

	/** The description. */
	@Column(name = "descr", nullable = true, unique = false, length = 250)
	private String descr;

	/** The type_. */
	@Column(name = "type_", nullable = false, unique = false, length = 1)
	private Integer type_;

	/** The picture graph. */
	@Column(name = "picGraph", nullable = true, unique = false)
	private Byte[] picGraph;

	/** The account is the join column to Account. */
	@OneToOne
	@JoinColumn(name = "accid", nullable = false, unique = false)
	private Account account;

	/**
	 * Gets the date first.
	 *
	 * @return the date first
	 */
	public Date getDateFirst() {
		return dateFirst;
	}

	/**
	 * Sets the date first.
	 *
	 * @param dateFirst the new date first
	 */
	public void setDateFirst(Date dateFirst) {
		this.dateFirst = dateFirst;
	}

	/**
	 * Gets the date last.
	 *
	 * @return the date last
	 */
	public Date getDateLast() {
		return dateLast;
	}

	/**
	 * Sets the date last.
	 *
	 * @param dateLast the new date last
	 */
	public void setDateLast(Date dateLast) {
		this.dateLast = dateLast;
	}

	/**
	 * Gets the color.
	 *
	 * @return the color
	 */
	public Integer getColor() {
		return color;
	}

	/**
	 * Sets the color.
	 *
	 * @param color the new color
	 */
	public void setColor(Integer color) {
		this.color = color;
	}

	/**
	 * Gets the account.
	 *
	 * @return the account
	 */
	public Account getAccount() {
		return account;
	}

	/**
	 * Sets the account.
	 *
	 * @param account the new account
	 */
	public void setAccount(Account account) {
		this.account = account;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the quality control dt.
	 *
	 * @return the quality control dt
	 */
	public List<QualityControlDt> getQualityControlDt() {
		return qualityControlDt;
	}

	/**
	 * Sets the quality control dt.
	 *
	 * @param qualityControlDt the new quality control dt
	 */
	public void setQualityControlDt(List<QualityControlDt> qualityControlDt) {
		this.qualityControlDt = qualityControlDt;
	}

	/**
	 * Gets the date time.
	 *
	 * @return the date time
	 */
	public Date getDateTime() {
		return dateTime;
	}

	/**
	 * Sets the date time.
	 *
	 * @param dateTime the new date time
	 */
	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	/**
	 * Gets the indicator.
	 *
	 * @return the indicator
	 */
	public Indicator getIndicator() {
		return indicator;
	}

	/**
	 * Sets the indicator.
	 *
	 * @param indicator the new indicator
	 */
	public void setIndicator(Indicator indicator) {
		this.indicator = indicator;
	}

	/**
	 * Gets the lot.
	 *
	 * @return the lot
	 */
	public String getLot() {
		return lot;
	}

	/**
	 * Sets the lot.
	 *
	 * @param lot the new lot
	 */
	public void setLot(String lot) {
		this.lot = lot;
	}

	/**
	 * Gets the low range.
	 *
	 * @return the low range
	 */
	public Double getLowRange() {
		return lowRange;
	}

	/**
	 * Sets the low range.
	 *
	 * @param lowRange the new low range
	 */
	public void setLowRange(Double lowRange) {
		this.lowRange = lowRange;
	}

	/**
	 * Gets the height range.
	 *
	 * @return the height range
	 */
	public Double getHeightRange() {
		return heightRange;
	}

	/**
	 * Sets the height range.
	 *
	 * @param heightRange the new height range
	 */
	public void setHeightRange(Double heightRange) {
		this.heightRange = heightRange;
	}

	/**
	 * Gets the attest value.
	 *
	 * @return the attest value
	 */
	public Double getAttestValue() {
		return attestValue;
	}

	/**
	 * Sets the attest value.
	 *
	 * @param attestValue the new attest value
	 */
	public void setAttestValue(Double attestValue) {
		this.attestValue = attestValue;
	}

	/**
	 * Gets the checks if is active.
	 *
	 * @return the checks if is active
	 */
	public Boolean getIsActive() {
		return isActive;
	}

	/**
	 * Sets the checks if is active.
	 *
	 * @param isActive the new checks if is active
	 */
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * Sets the type_.
	 *
	 * @param type_ the new type_
	 */
	public void setType_(Integer type_) {
		this.type_ = type_;
	}

	/**
	 * Gets the descr.
	 *
	 * @return the descr
	 */
	public String getDescr() {
		return descr;
	}

	/**
	 * Sets the descr.
	 *
	 * @param descr the new descr
	 */
	public void setDescr(String descr) {
		this.descr = descr;
	}

	/**
	 * Gets the type_.
	 *
	 * @return the type_
	 */
	public Integer getType_() {
		return type_;
	}

	/**
	 * Gets the pic graph.
	 *
	 * @return the pic graph
	 */
	public Byte[] getPicGraph() {
		return picGraph;
	}

	/**
	 * Sets the pic graph.
	 *
	 * @param picGraph the new pic graph
	 */
	public void setPicGraph(Byte[] picGraph) {
		this.picGraph = picGraph;
	}

}
