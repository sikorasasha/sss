package org.minilis.entity.jors;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;
import org.minilis.entity.dics.Employee;
import org.minilis.entity.security.Account;

/**
 * The Entity for JLabProccess.
 *
 * @author sasha
 */
@Entity
public class JLabProccess {

	/** The unique id. */
	@Id
	@GenericGenerator(name = "unique_id", strategy = "uuid")
	@GeneratedValue(generator = "unique_id")
	@Column(name = "id", nullable = false, length = 32, unique = true)
	private String id;

	/** The account is the join column to Account. */
	@OneToOne
	@JoinColumn(name = "accid", nullable = false, unique = false)
	private Account account;

	/** The datetime. */
	@Column(name = "datetime", nullable = false, unique = false)
	private Date datetime;
	
	/** The employee is the join column to Employee. */
	@OneToOne
	@JoinColumn(name="employee", nullable=false, unique=false)
	private Employee employee;

	/** The num. */
	@Column(name = "num", nullable = false, length = 8, unique = false)
	private String num;

	/** The name. */
	@Column(name = "name", nullable = true, length = 64, unique = false)
	private String name;

	/**  The jResultList is mapped by labprocId to list of JResults. */
	@OneToMany(mappedBy = "labprocId", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<JResult> jResultList;

	/** The dateclose. */
	@Column(name = "dateclose", nullable = true, unique = false)
	private Date dateclose;
	
	/** The cnt indics. */
	@Column(name = "cntIndics", nullable = true, length = 10, unique = false)
	private String cntIndics;

	/**
	 * Gets the cnt indics.
	 *
	 * @return the cnt indics
	 */
	public String getCntIndics() {
		return cntIndics;
	}

	/**
	 * Sets the cnt indics.
	 *
	 * @param cntIndics the new cnt indics
	 */
	public void setCntIndics(String cntIndics) {
		this.cntIndics = cntIndics;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the account.
	 *
	 * @return the account
	 */
	public Account getAccount() {
		return account;
	}

	/**
	 * Sets the account.
	 *
	 * @param account the new account
	 */
	public void setAccount(Account account) {
		this.account = account;
	}

	/**
	 * Gets the employee.
	 *
	 * @return the employee
	 */
	public Employee getEmployee() {
		return employee;
	}

	/**
	 * Sets the employee.
	 *
	 * @param employee the new employee
	 */
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	/**
	 * Gets the num.
	 *
	 * @return the num
	 */
	public String getNum() {
		return num;
	}

	/**
	 * Sets the num.
	 *
	 * @param num the new num
	 */
	public void setNum(String num) {
		this.num = num;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the j result list.
	 *
	 * @return the j result list
	 */
	public List<JResult> getjResultList() {
		return jResultList;
	}

	/**
	 * Sets the j result list.
	 *
	 * @param jResultList the new j result list
	 */
	public void setjResultList(List<JResult> jResultList) {
		this.jResultList = jResultList;
	}

	/**
	 * Gets the datetime.
	 *
	 * @return the datetime
	 */
	public Date getDatetime() {
		return datetime;
	}

	/**
	 * Sets the datetime.
	 *
	 * @param datetime the new datetime
	 */
	public void setDatetime(Date datetime) {
		this.datetime = datetime;
	}

	/**
	 * Gets the dateclose.
	 *
	 * @return the dateclose
	 */
	public Date getDateclose() {
		return dateclose;
	}

	/**
	 * Sets the dateclose.
	 *
	 * @param dateclose the new dateclose
	 */
	public void setDateclose(Date dateclose) {
		this.dateclose = dateclose;
	}
	
}
