/**
 * 
 */
package org.minilis.entity.dics;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;
import org.minilis.entity.jors.JLabProccess;
import org.minilis.entity.jors.JOrder;
import org.minilis.entity.security.Account;

/**
 * The Class PrintMarker.
 *
 * @author sasha
 */
@Entity
public class PrintMarker {

	/** The id. */
	@Id
	@GenericGenerator(name = "unique_id", strategy = "uuid")
	@GeneratedValue(generator = "unique_id")
	@Column(name = "id", nullable = false, length = 32, unique = true)
	private String id;
	
	/** The account the is join column to Account. */
	@OneToOne
	@JoinColumn(name="accid", nullable=false, unique=false)
	private Account account;

	
	/** The employee is the join column to Employee. */
	@OneToOne
	@JoinColumn(name="jorder", nullable=false, unique=false)
	private JOrder jorder;
	
	/** The lab proc. */
	@OneToOne
	@JoinColumn(name="labProc", nullable=false, unique=false)
	private JLabProccess labProc;
	
	/** The blank. */
	@OneToOne
	@JoinColumn(name="blank", nullable=false, unique=false)
	private Blank blank;
	
	/** The type_. */
	@Column(name = "type_", nullable = false, length = 1, unique = false)
	private Integer type_;
	
	/** The date print. */
	@Column(name = "datePrint", nullable = false, unique = false)
	private Date datePrint;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the account.
	 *
	 * @return the account
	 */
	public Account getAccount() {
		return account;
	}

	/**
	 * Sets the account.
	 *
	 * @param account the new account
	 */
	public void setAccount(Account account) {
		this.account = account;
	}

	/**
	 * Gets the order.
	 *
	 * @return the order
	 */
	public JOrder getOrder() {
		return jorder;
	}

	/**
	 * Sets the order.
	 *
	 * @param order the new order
	 */
	public void setOrder(JOrder jorder) {
		this.jorder = jorder;
	}

	/**
	 * Gets the lab proc.
	 *
	 * @return the lab proc
	 */
	public JLabProccess getLabProc() {
		return labProc;
	}

	/**
	 * Sets the lab proc.
	 *
	 * @param labProc the new lab proc
	 */
	public void setLabProc(JLabProccess labProc) {
		this.labProc = labProc;
	}

	/**
	 * Gets the blank.
	 *
	 * @return the blank
	 */
	public Blank getBlank() {
		return blank;
	}

	/**
	 * Sets the blank.
	 *
	 * @param blank the new blank
	 */
	public void setBlank(Blank blank) {
		this.blank = blank;
	}

	/**
	 * Gets the type_.
	 *
	 * @return the type_
	 */
	public Integer getType_() {
		return type_;
	}

	/**
	 * Sets the type_.
	 *
	 * @param type_ the new type_
	 */
	public void setType_(Integer type_) {
		this.type_ = type_;
	}

	/**
	 * Gets the date print.
	 *
	 * @return the date print
	 */
	public Date getDatePrint() {
		return datePrint;
	}

	/**
	 * Sets the date print.
	 *
	 * @param datePrint the new date print
	 */
	public void setDatePrint(Date datePrint) {
		this.datePrint = datePrint;
	}
	
}
