package org.minilis.entity.dics;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.minilis.entity.security.Account;

/**
 * The Entity for Employee.
 */
@Entity
public class Employee {

	/** The unique id. */
	@Id
	@GenericGenerator(name = "unique_id", strategy = "uuid")
	@GeneratedValue(generator = "unique_id")
	@Column(name = "id", nullable = false, length = 32, unique = true)
	private String id;

	/** The name. */
	@Column(name = "name", nullable = false, length = 20, unique = false)
	private String name;

	/** The jobtitle. */
	@Column(name = "jobtitle", nullable = true, length = 20, unique = false)
	private String jobtitle;

	/** The pass. */
	@Column(name = "pass", nullable = false, length = 32, unique = false)
	private String pass;

	/** The email. */
	@Column(name = "email", nullable = false, length = 128, unique = true)
	private String email;

	/** The signed print. */
	@Column(name = "signedPrint", nullable = true, unique = false)
	private Byte[] signedPrint;

	/** The analizator is the join column to Account . */
	@OneToOne
	@JoinColumn(name = "analizator", nullable = true)
	private Analizator analizator;

	/** The account is the join column to Account. */
	@ManyToOne
	@JoinColumn(name = "accid", nullable = false)
	private Account account;

	/** The super user. */
	@Column(name="superUser", nullable = false, unique = false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean superUser = true;

	/** The is active. */
	@Column(name = "isActive", nullable = false, unique = false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean isActive = true;

	/** The printer name. */
	@Column(name = "printerName", nullable = true, length = 64, unique = false)
	private String printerName;

	@Column(name ="dateRecoveryPass", nullable = true, unique = false)
	private Date dateRecoveryPass;

	@Column(name = "loginCount", nullable = false, unique = false)
	private Integer loginCount=0;
	
	@Column(name = "loginLast", nullable = true, unique = false)
	private Date loginLast;
	
	/**
	 * @return the loginCount
	 */
	public Integer getLoginCount() {
		return loginCount;
	}

	/**
	 * @param loginCount the loginCount to set
	 */
	public void setLoginCount(Integer loginCount) {
		this.loginCount = loginCount;
	}

	/**
	 * @return the loginLast
	 */
	public Date getLoginLast() {
		return loginLast;
	}

	/**
	 * @param loginLast the loginLast to set
	 */
	public void setLoginLast(Date loginLast) {
		this.loginLast = loginLast;
	}

	public Date getDateRecoveryPass() {
		return dateRecoveryPass;
	}

	public void setDateRecoveryPass(Date dateRecoveryPass) {
		this.dateRecoveryPass = dateRecoveryPass;
	}

	/**
	 * Gets the printer name.
	 *
	 * @return the printer name
	 */
	public String getPrinterName() {
		return printerName;
	}

	/**
	 * Sets the printer name.
	 *
	 * @param printerName the new printer name
	 */
	public void setPrinterName(String printerName) {
		this.printerName = printerName;
	}

	/**
	 * Gets the checks if is active.
	 *
	 * @return the checks if is active
	 */
	public Boolean getIsActive() {
		return isActive;
	}

	/**
	 * Sets the checks if is active.
	 *
	 * @param isActive the new checks if is active
	 */
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * Gets the super user.
	 *
	 * @return the super user
	 */
	public Boolean getSuperUser() {
		return superUser;
	}

	/**
	 * Sets the super user.
	 *
	 * @param superUser the new super user
	 */
	public void setSuperUser(Boolean superUser) {
		this.superUser = superUser;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the acc id.
	 *
	 * @return the acc id
	 */
	public Account getAccount() {
		return account;
	}

	/**
	 * Sets the acc id.
	 *
	 * @param account the new acc id
	 */
	public void setAccount(Account account) {
		this.account = account;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the jobtitle.
	 *
	 * @return the jobtitle
	 */
	public String getJobtitle() {
		return jobtitle;
	}

	/**
	 * Sets the jobtitle.
	 *
	 * @param jobtitle the new jobtitle
	 */
	public void setJobtitle(String jobtitle) {
		this.jobtitle = jobtitle;
	}

	/**
	 * Gets the pass.
	 *
	 * @return the pass
	 */
	public String getPass() {
		return pass;
	}

	/**
	 * Sets the pass.
	 *
	 * @param pass the new pass
	 */
	public void setPass(String pass) {
		this.pass = pass;
	}

	/**
	 * Gets the signed print.
	 *
	 * @return the signed print
	 */
	public Byte[] getSignedPrint() {
		return signedPrint;
	}

	/**
	 * Sets the signed print.
	 *
	 * @param signedPrint the new signed print
	 */
	public void setSignedPrint(Byte[] signedPrint) {
		this.signedPrint = signedPrint;
	}

	/**
	 * Gets the analizator.
	 *
	 * @return the analizator
	 */
	public Analizator getAnalizator() {
		return analizator;
	}

	/**
	 * Sets the analizator.
	 *
	 * @param analizator the new analizator
	 */
	public void setAnalizator(Analizator analizator) {
		this.analizator = analizator;
	}

}
