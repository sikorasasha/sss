package org.minilis.entity.dics;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.minilis.entity.security.Account;

/**
 * The Class Indicator.
 */
@Entity
public class Indicator {

	/** The unique id. */
	@Id
	@GenericGenerator(name = "unique_id", strategy = "uuid")
	@GeneratedValue(generator = "unique_id")
	@Column(name = "id", nullable = false, length = 32, unique = true)
	private String id;

	/** The grp is the join column to GrpIndicator. */
	@ManyToOne
	@JoinColumn(name = "grp", nullable = false)
	private GrpIndicator grp;

	/** The name. */
	@Column(name = "name", nullable = false, length = 250, unique = true)
	private String name;

	/** The code. */
	@Column(name = "code", nullable = true, length = 20, unique = false)
	private String code;

	/** The unit. */
	@Column(name = "unit", nullable = true, length = 20, unique = false)
	private String unit;

	/** The norm text. */
	@Column(name = "normText", nullable = true, length = 2000, unique = false)
	private String normText;

	/** The host code. */
	@Column(name = "hostCode", nullable = true, length = 20, unique = false)
	private String hostCode;

	/** The order_. */
	@Column(name = "order_", nullable = true, unique = false)
	private Integer order_;

	/** The blank is the join column to Blank. */
	@OneToOne
	@JoinColumn(name = "blank", nullable = true)
	private Blank blank;

	/** The analizator is the join column to Analizator. */
	@OneToOne
	@JoinColumn(name = "analizator", nullable = true)
	private Analizator analizator;

	/** The complex is the join table for List of Indicators. */
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "complex")
	private List<Indicator> complex;

	/** The is complex. */
	@Column(name = "isComplex", nullable = false, unique = false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean isComplex = false;

	/** The account is the join column to Account. */
	@OneToOne
	@JoinColumn(name = "accid", nullable = false, unique = false)
	private Account account;

	/**
	 * Gets the account.
	 *
	 * @return the account
	 */
	public Account getAccount() {
		return account;
	}

	/**
	 * Sets the account.
	 *
	 * @param account the new account
	 */
	public void setAccount(Account account) {
		this.account = account;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the grp.
	 *
	 * @return the grp
	 */
	public GrpIndicator getGrp() {
		return grp;
	}

	/**
	 * Sets the grp.
	 *
	 * @param grp the new grp
	 */
	public void setGrp(GrpIndicator grp) {
		this.grp = grp;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Gets the unit.
	 *
	 * @return the unit
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * Sets the unit.
	 *
	 * @param unit the new unit
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * Gets the norm text.
	 *
	 * @return the norm text
	 */
	public String getNormText() {
		return normText;
	}

	/**
	 * Sets the norm text.
	 *
	 * @param normText the new norm text
	 */
	public void setNormText(String normText) {
		this.normText = normText;
	}

	/**
	 * Gets the host code.
	 *
	 * @return the host code
	 */
	public String getHostCode() {
		return hostCode;
	}

	/**
	 * Sets the host code.
	 *
	 * @param hostCode the new host code
	 */
	public void setHostCode(String hostCode) {
		this.hostCode = hostCode;
	}

	/**
	 * Gets the order_.
	 *
	 * @return the order_
	 */
	public Integer getOrder_() {
		return order_;
	}

	/**
	 * Gets the blank.
	 *
	 * @return the blank
	 */
	public Blank getBlank() {
		return blank;
	}

	/**
	 * Sets the blank.
	 *
	 * @param blank the new blank
	 */
	public void setBlank(Blank blank) {
		this.blank = blank;
	}

	/**
	 * Gets the analizator.
	 *
	 * @return the analizator
	 */
	public Analizator getAnalizator() {
		return analizator;
	}

	/**
	 * Sets the analizator.
	 *
	 * @param analizator the new analizator
	 */
	public void setAnalizator(Analizator analizator) {
		this.analizator = analizator;
	}

	/**
	 * Gets the complex.
	 *
	 * @return the complex
	 */
	public List<Indicator> getComplex() {
		return complex;
	}

	/**
	 * Sets the complex.
	 *
	 * @param complex the new complex
	 */
	public void setComplex(List<Indicator> complex) {
		this.complex = complex;
	}

	/**
	 * Gets the checks if is complex.
	 *
	 * @return the checks if is complex
	 */
	public Boolean getIsComplex() {
		return isComplex;
	}

	/**
	 * Sets the checks if is complex.
	 *
	 * @param isComplex the new checks if is complex
	 */
	public void setIsComplex(Boolean isComplex) {
		this.isComplex = isComplex;
	}

	/**
	 * Sets the order_.
	 *
	 * @param order_ the new order_
	 */
	public void setOrder_(Integer order_) {
		this.order_ = order_;
	}

}
