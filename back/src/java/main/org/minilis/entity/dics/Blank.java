package org.minilis.entity.dics;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.minilis.entity.security.Account;

/**
 * The Entity for Blank.
 */
@Entity
public class Blank {

	/** The unique id. */
	@Id
	@GenericGenerator(name = "unique_id", strategy = "uuid")
    @GeneratedValue(generator = "unique_id")
	@Column(name = "id", nullable = false, length = 32, unique=true)
	private String id;

	/** The name. */
	@Column(name = "name", nullable = false, length = 50, unique=false)
	private String name;

	/** The is system. */
	@Column(name = "isSystem", nullable = false, unique=false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean isSystem;

	/** The body. */
	@Column(name = "body", nullable = true, unique=false)
	private Byte[] body;

	/** The is visible. */
	@Column(name = "isVisible", nullable = false, unique=false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean isVisible;

	/** The account is the join column to Account. */
	@OneToOne
	@JoinColumn(name="accid", nullable=false, unique=false)
	private Account account;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the checks if is system.
	 *
	 * @return the checks if is system
	 */
	public Boolean getIsSystem() {
		return isSystem;
	}

	/**
	 * Sets the checks if is system.
	 *
	 * @param isSystem the new checks if is system
	 */
	public void setIsSystem(Boolean isSystem) {
		this.isSystem = isSystem;
	}

	/**
	 * Gets the body.
	 *
	 * @return the body
	 */
	public Byte[] getBody() {
		return body;
	}

	/**
	 * Sets the body.
	 *
	 * @param body the new body
	 */
	public void setBody(Byte[] body) {
		this.body = body;
	}

	/**
	 * Gets the checks if is visible.
	 *
	 * @return the checks if is visible
	 */
	public Boolean getIsVisible() {
		return isVisible;
	}

	/**
	 * Sets the checks if is visible.
	 *
	 * @param isVisible the new checks if is visible
	 */
	public void setIsVisible(Boolean isVisible) {
		this.isVisible = isVisible;
	}

	/**
	 * Gets the account.
	 *
	 * @return the account
	 */
	public Account getAccount() {
		return account;
	}

	/**
	 * Sets the account.
	 *
	 * @param account the new account
	 */
	public void setAccount(Account account) {
		this.account = account;
	}





}
