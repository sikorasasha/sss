package org.minilis.impl.dao;

import org.minilis.api.dao.IAccountDao;
import org.minilis.entity.security.Account;
import org.springframework.stereotype.Repository;

/**
 * The Class AccountDao.
 */
@Repository
public class AccountDao extends BaseDao<Account> implements IAccountDao {


}
