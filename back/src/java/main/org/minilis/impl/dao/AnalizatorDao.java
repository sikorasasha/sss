package org.minilis.impl.dao;

import org.minilis.api.dao.IAnalizatorDao;
import org.minilis.entity.dics.Analizator;
import org.springframework.stereotype.Repository;

/**
 * The Class AnalizatorDao.
 */
@Repository
public class AnalizatorDao extends BaseDao<Analizator> implements IAnalizatorDao{

}
