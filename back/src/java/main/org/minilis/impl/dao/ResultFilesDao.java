package org.minilis.impl.dao;

import org.minilis.api.dao.IResultFilesDao;
import org.minilis.entity.jors.ResultFiles;
import org.springframework.stereotype.Repository;

/**
 * The Class ResultFilesDao.
 */
@Repository
public class ResultFilesDao extends BaseDao<ResultFiles> implements IResultFilesDao{

}
