/**
 * 
 */
package org.minilis.impl.dao;

import org.minilis.api.dao.IEmailTemplatesDao;
import org.minilis.entity.config.EmailTemplates;
import org.springframework.stereotype.Repository;

/**
 * @author sasha
 *
 */
@Repository
public class EmailTemplatesDao extends BaseDao<EmailTemplates> implements IEmailTemplatesDao{

}
