package org.minilis.impl.dao;

import org.minilis.api.dao.IBlankDao;
import org.minilis.entity.dics.Blank;
import org.springframework.stereotype.Repository;

/**
 * The Class BlankDao.
 */
@Repository
public class BlankDao extends BaseDao<Blank> implements IBlankDao{

}
