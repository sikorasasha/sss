package org.minilis.impl.dao;

import org.minilis.api.dao.ICfgEmailDao;
import org.minilis.entity.config.CfgEmail;
import org.minilis.impl.dao.BaseDao;
import org.springframework.stereotype.Repository;

/**
 * The Class CfgEmailDao.
 */
@Repository
public class CfgEmailDao extends BaseDao<CfgEmail> implements ICfgEmailDao{

}
