package org.minilis.impl.dao;

import org.minilis.api.dao.IImportIndicatorsDao;
import org.minilis.entity.dics.ImportIndicators;
import org.springframework.stereotype.Repository;

/**
 * The Class ImportIndicatorsDao.
 */
@Repository
public class ImportIndicatorsDao extends BaseDao<ImportIndicators> implements IImportIndicatorsDao{

}
