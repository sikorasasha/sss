package org.minilis.impl.dao;

import org.minilis.api.dao.IGrpIndicatorDao;
import org.minilis.entity.dics.GrpIndicator;
import org.springframework.stereotype.Repository;

/**
 * The Class GrpIndicatorDao.
 */
@Repository
public class GrpIndicatorDao extends BaseDao<GrpIndicator> implements IGrpIndicatorDao {

}
