package org.minilis.impl.dao;

import org.minilis.api.dao.IIndicatorDao;
import org.minilis.entity.dics.Indicator;
import org.springframework.stereotype.Repository;

/**
 * The Class IndicatorDao.
 */
@Repository
public class IndicatorDao extends BaseDao<Indicator> implements IIndicatorDao{

}
