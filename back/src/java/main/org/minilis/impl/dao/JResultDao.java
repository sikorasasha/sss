package org.minilis.impl.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.minilis.api.dao.IJResultDao;
import org.minilis.entity.jors.JResult;
import org.springframework.stereotype.Repository;

/**
 * The Class JResultDao.
 */
@Repository
public class JResultDao extends BaseDao<JResult> implements IJResultDao{

	@SuppressWarnings("unchecked")
	@Override
	public List<JResult> selectGroupByHDIDBlankID(int from, int first, List<Criterion> filter) {
		final Criteria crit;
		if (from != 0) {
			crit = sessionFactory.getCurrentSession().createCriteria(JResult.class)
					.setMaxResults(from);
		} else {
			crit = sessionFactory.getCurrentSession().createCriteria(JResult.class);
		}
		crit.createAlias("indicator", "indicator");
		crit.setProjection(Projections.groupProperty("hdid"));
		crit.setProjection(Projections.groupProperty("indicator.blank"));
		if (first > 0)
			crit.setFirstResult(first);
		if (filter != null) {
			for (Criterion criterion : filter) {
				crit.add(criterion);
			}
		}
		return crit.list();
	}
}
