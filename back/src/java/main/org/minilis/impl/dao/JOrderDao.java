package org.minilis.impl.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.minilis.api.dao.IJOrderDao;
import org.minilis.entity.jors.JOrder;
import org.springframework.stereotype.Repository;

/**
 * The Class JOrderDao.
 */
@Repository
public class JOrderDao extends BaseDao<JOrder> implements IJOrderDao{

	/**
	 * Select JResults that related with current JOrder from DB.
	 *
	 * @param from is how many elements will selected.
	 * @param first is the first element from which the data will selected.
	 * @param filter is the list of criteria.
	 * @return all selected elements.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<JOrder> selectJResult(int from, int first, List<Criterion> filter) {
		final Criteria crit;
		if (from != 0) {
			crit = sessionFactory.getCurrentSession().createCriteria(JOrder.class)
					.setMaxResults(from);
		} else {
			crit = sessionFactory.getCurrentSession().createCriteria(JOrder.class);
		}
		if (first > 0)
			crit.setFirstResult(first);
		crit.createAlias("jResult", "jResult");
		if (filter != null) {
			for (Criterion criterion : filter) {
				crit.add(criterion);
			}
		}
		return crit.list();
	}
	
	/**
	 * Select JResults, Indicators that related with current JOrder from DB.
	 *
	 * @param from is how many elements will selected.
	 * @param first is the first element from which the data will selected.
	 * @param filter is the list of criteria.
	 * @return all selected elements.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<JOrder> selectJResultIndicator(int from, int first, List<Criterion> filter) {
		final Criteria crit;
		if (from != 0) {
			crit = sessionFactory.getCurrentSession().createCriteria(JOrder.class)
					.setMaxResults(from);
		} else {
			crit = sessionFactory.getCurrentSession().createCriteria(JOrder.class);
		}
		if (first > 0)
			crit.setFirstResult(first);
		crit.createAlias("jResult", "jResult");
		crit.createAlias("jResult.indicator", "indicator");
		crit.addOrder(Order.desc("dateTime"));
		if (filter != null) {
			for (Criterion criterion : filter) {
				crit.add(criterion);
			}
		}
		return crit.list();
	}
}
