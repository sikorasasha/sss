package org.minilis.impl.dao;

import org.minilis.api.dao.IEmployeeDao;
import org.minilis.entity.dics.Employee;
import org.springframework.stereotype.Repository;

/**
 * The Class EmployeeDao.
 */
@Repository
public class EmployeeDao extends BaseDao<Employee> implements IEmployeeDao{

}
