package org.minilis.impl.dao;

import org.minilis.api.dao.ICollectionPlaceDao;
import org.minilis.entity.dics.CollectionPlace;
import org.springframework.stereotype.Repository;

/**
 * The Class CollectionPlaceDao.
 */
@Repository
public class CollectionPlaceDao extends BaseDao<CollectionPlace> implements ICollectionPlaceDao{

}
