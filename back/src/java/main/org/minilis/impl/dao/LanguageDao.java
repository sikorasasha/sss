package org.minilis.impl.dao;

import org.minilis.api.dao.ILanguageDao;
import org.minilis.entity.config.Language;
import org.springframework.stereotype.Repository;

/**
 * The Class LanguageDao.
 */
@Repository
public class LanguageDao extends BaseDao<Language> implements ILanguageDao{

}
