package org.minilis.impl.dao;

import org.minilis.api.dao.IJLabProccessDao;
import org.minilis.entity.jors.JLabProccess;
import org.springframework.stereotype.Repository;

/**
 * The Class JLabProccessDao.
 */
@Repository
public class JLabProccessDao extends BaseDao<JLabProccess> implements IJLabProccessDao{

}
