package org.minilis.impl.dao;

import org.minilis.api.dao.IQualityControlDtDao;
import org.minilis.entity.jors.QualityControlDt;
import org.springframework.stereotype.Repository;

/**
 * The Class QualityControlDtDao.
 */
@Repository
public class QualityControlDtDao extends BaseDao<QualityControlDt> implements IQualityControlDtDao{

}
