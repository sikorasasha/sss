/**
 * 
 */
package org.minilis.impl.dao;

import org.minilis.api.dao.IPrintMarkerDao;
import org.minilis.entity.dics.PrintMarker;
import org.springframework.stereotype.Repository;

/**
 * @author sasha
 *
 */
@Repository
public class PrintMarkerDao extends BaseDao<PrintMarker> implements IPrintMarkerDao{

}
