package org.minilis.impl.dao;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.minilis.api.dao.IDao;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * The Base Dao class.
 *
 * @param <T>
 *            the generic type
 */
public abstract class BaseDao<T> implements IDao<T> {

	/** The session factory. */
	@Autowired
	protected SessionFactory sessionFactory;

	/** The type. */
	private Class<T> type;

	/**
	 * Instantiates a new base dao.
	 */
	@SuppressWarnings("unchecked")
	public BaseDao() {
		Type t = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType) t;
		type = (Class<T>) pt.getActualTypeArguments()[0];
	}

	/**
	 * Insert object in DB.
	 *
	 * @param obj
	 *            is inserting object
	 * @return inserted object
	 */
	@Override
	public T insert(T obj) {
		sessionFactory.getCurrentSession().save(obj);
		return obj;
	}

	/**
	 * Update object in DB
	 *
	 * @param obj
	 *            is replacing object.
	 * @return updated object.
	 */
	@Override
	public T update(T obj) {
		sessionFactory.getCurrentSession().merge(obj);
		return obj;

	}

	/**
	 * Delete object from DB.
	 *
	 * @param obj
	 *            is deleting object.
	 */
	@Override
	public void delete(T obj) {
		sessionFactory.getCurrentSession().delete(obj);

	}

	/**
	 * Select objects from DB.
	 *
	 * @param count
	 *            is how many elements will selected.
	 * @param from
	 *            is the first element from which the data will selected.
	 * @param filter
	 *            is the list of criteria.
	 * @return all selected elements.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<T> select(int count, int from, List<Criterion> filter) {
		final Criteria crit;
		if (count != 0) {
			crit = sessionFactory.getCurrentSession().createCriteria(type)
					.setMaxResults(count);
		} else {
			crit = sessionFactory.getCurrentSession().createCriteria(type);
		}
		if (from > 0)
			crit.setFirstResult(from);
		if (filter != null) {
			for (Criterion criterion : filter) {
				crit.add(criterion);
			}
		}
		return crit.list();
	}

	/**
	 * Select objects from DB.
	 *
	 * @param filter
	 *            is the list of criteria.
	 * @return all selected elements.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<T> select(List<Criterion> filter) {
		final Criteria crit = sessionFactory.getCurrentSession()
				.createCriteria(type);
		if (filter != null) {
			for (Criterion criterion : filter) {
				crit.add(criterion);
			}
		}
		return crit.list();
	}

	@Override
	public void deleteByAccId(String account) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"delete from " + type.getName() + " g where g.account = '"
						+ account + "'");
		query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	public T selectById(String id) {
		return (T) sessionFactory.getCurrentSession().get(type, id);
	}
}
