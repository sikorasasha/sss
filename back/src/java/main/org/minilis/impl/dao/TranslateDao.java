package org.minilis.impl.dao;

import org.minilis.api.dao.ITranslateDao;
import org.minilis.entity.config.Translation;
import org.springframework.stereotype.Repository;

/**
 * The Class TranslateDao.
 */
@Repository
public class TranslateDao extends BaseDao<Translation> implements ITranslateDao{

}
