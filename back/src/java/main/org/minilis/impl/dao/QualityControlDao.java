package org.minilis.impl.dao;

import org.minilis.api.dao.IQualityControlDao;
import org.minilis.entity.jors.QualityControl;
import org.springframework.stereotype.Repository;

/**
 * The Class QualityControlDao.
 */
@Repository
public class QualityControlDao extends BaseDao<QualityControl> implements IQualityControlDao{

}
