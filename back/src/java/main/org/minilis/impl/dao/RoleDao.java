package org.minilis.impl.dao;

import org.minilis.api.dao.IRoleDao;
import org.minilis.entity.security.Role;
import org.springframework.stereotype.Repository;

/**
 * The Class RoleDao.
 */
@Repository
public class RoleDao extends BaseDao<Role> implements IRoleDao {

}
