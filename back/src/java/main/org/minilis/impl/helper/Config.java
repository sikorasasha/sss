package org.minilis.impl.helper;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.minilis.api.helper.IConfig;
import org.minilis.api.helper.IContextHelper;
import org.minilis.api.services.IAccountService;
import org.minilis.api.services.IEmployeeService;
import org.minilis.api.services.ILanguageService;
import org.minilis.api.services.ITranslateService;
import org.minilis.entity.dics.Employee;
import org.minilis.entity.security.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * The Class Config for configuring settings of language, getting Ean8, getting
 * account id, deleting account directory..
 */
@Component
public class Config implements IConfig {

	final public static Map<String, String> authEmail = Collections
			.synchronizedMap(new HashMap<String, String>());
	final public static Map<String, String> authPass = Collections
			.synchronizedMap(new HashMap<String, String>());

	@Override
	public Map<String, String> getAuthemail() {
		return authEmail;
	}

	@Override
	public Map<String, String> getAuthpass() {
		return authPass;
	}

	@Override
	public void delAuthemail(String key) {
		authEmail.remove(key);
	}

	@Override
	public void delAuthpass(String key) {
		authPass.remove(key);
	}

	@Override
	public void putAuthemail(String key, String value) {
		authEmail.put(key, value);
	}

	@Override
	public void putAuthpass(String key, String value) {
		authPass.put(key, value);
	}

	/** The context. */
	@Autowired
	private IContextHelper context;

	/** The language service. */
	@Autowired
	private ILanguageService languageService;

	/** The translate service. */
	@Autowired
	private ITranslateService translateService;

	@Autowired
	private IEmployeeService employeeService;

	@Autowired
	private IAccountService accountService;

	@Autowired
	private GlobalOptions globalOptions;

	/**
	 * Initialization language.
	 */
	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Override
	 * 
	 * @PostConstruct public void init() { List<Language> l =
	 * languageService.select(0, 0, null); if (l != null) { for (int i = 0; i <
	 * l.size(); i++) { File file = new
	 * File(context.getRealPath("/scripts/services/translation") +
	 * "/translation_" + l.get(i).getName() + ".json"); try { file.mkdir();
	 * file.createNewFile(); } catch (IOException e) { e.printStackTrace(); }
	 * JSONObject js = new JSONObject(); for (int j = 0; j <
	 * l.get(i).getTranslation().size(); j++) {
	 * js.put(l.get(i).getTranslation().get(j).getKey(), l.get(i)
	 * .getTranslation().get(j).getValue()); } PrintWriter out = null; try { out
	 * = new PrintWriter(file.getAbsoluteFile()); } catch (FileNotFoundException
	 * e) { e.printStackTrace(); } out.print(js.toString()); out.close(); } } }
	 */
	/**
	 * Gets the ean8.
	 *
	 * @param code
	 * @return Ean8 code
	 */
	@Override
	public String getEan8(String code) {
		int i;
		int rl1;
		int rl2;
		rl1 = 0;
		rl2 = 0;
		i = 0;
		String result;
		String tmp;

		tmp = "00000000" + code.trim();
		result = tmp.substring(tmp.length() - 7, tmp.length()) + "0";

		if (result.substring(1, 2).equals("0")) {
			result = "1" + result.substring(1, 8);
		}

		i = 8;
		while (i > 0) {
			if (i % 2 == 0) {
				rl2 = rl2 + Integer.valueOf(result.substring(i - 1, i)) * 1;
			} else
				rl2 = rl2 + Integer.valueOf(result.substring(i - 1, i)) * 3;
			i = i - 1;
		}

		rl1 = rl2 % 10;
		if (rl1 != 0)
			rl2 = 10 - rl1;
		else
			rl2 = 0;

		result = result.substring(0, 7) + rl2;
		return result;
	}

	@Override
	public Boolean checkActivationTimeout(Account account)
	{
		if (account.getIsActivated())
			return false;

		Calendar gc = new GregorianCalendar();
		gc.setTime(new Date());
		Calendar gc1 = new GregorianCalendar();
		gc1.setTime(account.getDateReg());
		gc1.add(Calendar.DATE, globalOptions.getDaysBeforeAct());
		if (gc1.getTime().before(gc.getTime()))
			return true;
		return false;
	}

	@Override
	public Boolean checkRecoverLinkTimeout(Employee employee)
	{
		if (employee.getDateRecoveryPass() == null)
			return true;
		Calendar currentDay = new GregorianCalendar();
		Calendar actualDate = new GregorianCalendar();
		currentDay.setTime(new Date());
		actualDate.setTime(employee.getDateRecoveryPass());
		actualDate.add(Calendar.DATE, 1);
		if (actualDate.getTime().before(currentDay.getTime()))
			return true;
		return false;
	}
	/**
	 * Gets the account path.
	 *
	 * @return return account path
	 */
	@Override
	public String getAccountPath(String idAccount) {
		String check = context.getRealPath("/"+idAccount);
		//проверка на иснування папки
		if ((new File(check)).exists()) {
			return check;
		} else {
			//создание папки
			File accId = new File(context.getRealPath("/"+idAccount));
			accId.mkdir();
			File accTemp = new File(context.getRealPath("/"+idAccount+"/temp"));
			accTemp.mkdir();
			return check;
		}
	}

	/**
	 * Deleting account directory.
	 *
	 * @param account
	 *            is current account id
	 */
	@Override
	public void delAccDir(String account) {
		File directory = new File(context.getRealPath("/accounts/" + account));
		if (directory.isDirectory()) {
			for (File file : directory.listFiles()) {
				file.delete();
				System.out.println("deleting files" + file.getName());
			}
			directory.delete();
			System.out.println("delete succes");
		} else {
			directory.delete();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.minilis.front.api.controllers.IRegistration#sendMailActivate(java
	 * .lang.String)
	 */
	@Override
	public Boolean sendMail(String email, String subject, String context) {
		Properties props = new Properties();
		GlobalOptions options = new GlobalOptions();
		final String username = options.getEmailLogin();
		final String password = options.getEmailPass();
		props.put("mail.smtp.from", options.getEmailFrom());
		props.put("mail.smtp.auth", options.getEmailAuth());
		props.put("mail.smtp.starttls.enable", options.getEmailStartTls());
		props.put("mail.smtp.host", options.getEmailHost());
		props.put("mail.smtp.port", options.getEmailPort());
		Session session = Session.getInstance(props, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});
		try {
			Message message = new MimeMessage(session);
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(email));
			message.setSubject(subject);
			message.setContent(context, "text/html; charset=utf-8");
			Transport.send(message);
		} catch (MessagingException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.minilis.front.api.controllers.IRegistration#validationSubDomen(java
	 * .lang.String)
	 */
	@Override
	public Boolean validationSubDomen(String subdome) {
		List<Criterion> list = new ArrayList<Criterion>();
		list.add(Restrictions.eq("domain", subdome.toLowerCase()));
		List<Account> accList = accountService.select(0, 0, list);
		if (accList.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.minilis.back.api.config.IConfig#getHostURL()
	 */
	@Override
	public String getHostURL() {
		return getCurrentEmployee().getAccount().getDomain() + '.'
				+ globalOptions.getSiteUrl();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.minilis.back.api.config.IConfig#getCurrentEmployee()
	 */
	@Override
	public Employee getCurrentEmployee() {
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		List<Criterion> list = new ArrayList<Criterion>();
		list.add(Restrictions.eq("email", authentication.getName().toLowerCase()));
		List<Employee> employees = employeeService.select(0, 0, list);
		if (employees.size() == 0)
			return null;
		else
			return employees.get(0);

	}

	@Override
	public Employee getCurrentEmployee(String email) {
		List<Criterion> list = new ArrayList<Criterion>();
		list.add(Restrictions.eq("email", email.toLowerCase()));
		List<Employee> employees = employeeService.select(0, 0, list);
		if (employees.size() == 0)
			return null;
		else
			return employees.get(0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.minilis.api.helper.IConfig#getUUID(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public String getUUID(String email, String pass) {
		String keyUUID = UUID.randomUUID().toString();
		putAuthemail(keyUUID, email.toLowerCase());
		putAuthpass(keyUUID, pass);
		return keyUUID;
	}
	
	@Override
	public String alert(String message){
		JSONObject alert = new JSONObject();
		alert.put("responce", false).put(
				"alertsData",
				new JSONObject().put("alerts", new JSONArray()
						.put(new JSONObject().put("type", 2).put("msg",
								message))));
		return alert.toString();
	}
}
