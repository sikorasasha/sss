/**
 * 
 */
package org.minilis.impl.helper;

import javax.servlet.ServletContext;

import org.minilis.api.helper.IContextHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

/**
 * @author sasha
 *
 */
@Component
public class ContextHelper implements IContextHelper{
	
	@Autowired
	private ServletContext context;
	
	@Autowired
	private ApplicationContext appContext;
	
	@Override
	public String getRealPath(String name)
	{
		return context.getRealPath(name);
	}

	@Override
	public Resource getResource(String name)
	{
		return appContext.getResource(name);
	}
}
