/**
 *
 */
package org.minilis.impl.helper;

import org.springframework.stereotype.Component;

/**
 * @author gum
 *
 */

@Component
public class GlobalOptions {

	private String siteUrl = "minilis.org";

	private String emailLogin = "minilis@minilis.org";
	private String emailPass = "rn29vchw";
	private String emailFrom = "minilis@minilis.org";
	private Boolean emailAuth = true;
	private Boolean emailStartTls = false;
	private String emailHost = "mail.minilis.org";
	private Integer emailPort = 587;
	private Integer daysBeforeAct = 14;
	public Integer getDaysBeforeAct() {
		return daysBeforeAct;
	}
	public void setDaysBeforeAct(Integer daysBeforeAct) {
		this.daysBeforeAct = daysBeforeAct;
	}
	/**
	 * @return the siteUrl
	 */
	public String getSiteUrl() {
		return siteUrl;
	}
	/**
	 * @param siteUrl the siteUrl to set
	 */
	public void setSiteUrl(String siteUrl) {
		this.siteUrl = siteUrl;
	}
	/**
	 * @return the emailLogin
	 */
	public String getEmailLogin() {
		return emailLogin;
	}
	/**
	 * @param emailLogin the emailLogin to set
	 */
	public void setEmailLogin(String emailLogin) {
		this.emailLogin = emailLogin;
	}
	/**
	 * @return the emailPass
	 */
	public String getEmailPass() {
		return emailPass;
	}
	/**
	 * @param emailPass the emailPass to set
	 */
	public void setEmailPass(String emailPass) {
		this.emailPass = emailPass;
	}
	/**
	 * @return the emailFrom
	 */
	public String getEmailFrom() {
		return emailFrom;
	}
	/**
	 * @param emailFrom the emailFrom to set
	 */
	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}
	/**
	 * @return the emailAuth
	 */
	public Boolean getEmailAuth() {
		return emailAuth;
	}
	/**
	 * @param emailAuth the emailAuth to set
	 */
	public void setEmailAuth(Boolean emailAuth) {
		this.emailAuth = emailAuth;
	}
	/**
	 * @return the emailStartTls
	 */
	public Boolean getEmailStartTls() {
		return emailStartTls;
	}
	/**
	 * @param emailStartTls the emailStartTls to set
	 */
	public void setEmailStartTls(Boolean emailStartTls) {
		this.emailStartTls = emailStartTls;
	}
	/**
	 * @return the emailHost
	 */
	public String getEmailHost() {
		return emailHost;
	}
	/**
	 * @param emailHost the emailHost to set
	 */
	public void setEmailHost(String emailHost) {
		this.emailHost = emailHost;
	}
	/**
	 * @return the emailPort
	 */
	public Integer getEmailPort() {
		return emailPort;
	}
	/**
	 * @param emailPort the emailPort to set
	 */
	public void setEmailPort(Integer emailPort) {
		this.emailPort = emailPort;
	}

}
