/**
 *
 */
package org.minilis.impl.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.minilis.api.helper.IConfig;
import org.minilis.api.helper.IMailHelper;
import org.minilis.api.services.IEmailTemplatesService;
import org.minilis.entity.config.EmailTemplates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author tester
 *
 */
@Component
public class MailHelper implements IMailHelper {

	@Autowired
	private IEmailTemplatesService emailService;
	@Autowired
	private IConfig config;
	/*
	 * (non-Javadoc)
	 *
	 * @see org.minilis.back.api.config.IMailHelper#getSubject(java.lang.String,
	 * java.util.HashMap)
	 */
	@Override
	public String getSubject(String email, HashMap<String, String> parameters, String emailType) {
		String locale = emailType + config.getCurrentEmployee(email).getAccount().getLocale();
		String body = "";
		List<Criterion> list = new ArrayList<Criterion>();
		list.add(Restrictions.eq("emailName", locale));
		List<EmailTemplates> emailTemplates = emailService.select(0, 0, list);
		if (emailTemplates.size() != 0) {
			body = emailTemplates.get(0).getBody();
		} else{
			locale = emailType + "en";
			list.clear();
			list.add(Restrictions.eq("emailName", locale));
			emailTemplates = emailService.select(0, 0, list);
			body = emailTemplates.get(0).getBody();
		}
		for (Map.Entry<String, String> entry: parameters.entrySet()){
		    body = body.replace(entry.getKey() , entry.getValue());
		}
		return body;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.minilis.back.api.config.IMailHelper#getContext(java.lang.String,
	 * java.util.HashMap)
	 */
	@Override
	public String getContext(String email, HashMap<String, String> parameters, String emailType) {
		String locale = emailType + config.getCurrentEmployee(email).getAccount().getLocale();
		String body = "";
		List<Criterion> list = new ArrayList<Criterion>();
		list.clear();
		list.add(Restrictions.eq("emailName", locale));
		List<EmailTemplates> emailTemplates = emailService.select(0, 0, list);
		if (emailTemplates.size() != 0) {
			body = emailTemplates.get(0).getBody();
		} else {
			locale = emailType + "en";
			list.clear();
			list.add(Restrictions.eq("emailName", locale));
			emailTemplates = emailService.select(0, 0, list);
			body = emailTemplates.get(0).getBody();
		}
		for (Map.Entry<String, String> entry : parameters.entrySet()) {
			body = body.replace(entry.getKey(), entry.getValue());
		}
		return body;
	}
}
