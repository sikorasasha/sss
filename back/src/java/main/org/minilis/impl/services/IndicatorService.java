package org.minilis.impl.services;

import javax.annotation.PostConstruct;

import org.minilis.api.dao.IIndicatorDao;
import org.minilis.api.services.IIndicatorService;
import org.minilis.entity.dics.Indicator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class IndicatorService.
 */
@Service
@Transactional
public class IndicatorService extends BaseService<Indicator> implements
		IIndicatorService {

	/** The IndicatorDao. */
	@Autowired
	protected IIndicatorDao pdao;

	/**
	 * Initialization dao.
	 */
	@PostConstruct
	private void init() {
		this.dao = pdao;
	}
}
