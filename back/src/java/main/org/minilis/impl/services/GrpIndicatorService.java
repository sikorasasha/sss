package org.minilis.impl.services;

import javax.annotation.PostConstruct;

import org.minilis.api.dao.IGrpIndicatorDao;
import org.minilis.api.services.IGrpIndicatorService;
import org.minilis.entity.dics.GrpIndicator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class GrpIndicatorService.
 */
@Service
@Transactional
public class GrpIndicatorService extends BaseService<GrpIndicator> implements IGrpIndicatorService{

	/** The GrpIndicatorDao. */
	@Autowired
    protected IGrpIndicatorDao pdao;

	/**
	 * Initialization dao.
	 */
	@PostConstruct
    private void init() {
            this.dao = pdao;
    }
}
