package org.minilis.impl.services;

import javax.annotation.PostConstruct;

import org.minilis.api.dao.IAnalizatorDao;
import org.minilis.api.services.IAnalizatorService;
import org.minilis.entity.dics.Analizator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class AnalizatorService.
 */
@Service
@Transactional
public class AnalizatorService extends BaseService<Analizator> implements
		IAnalizatorService {

	/** The AnalizatorDao. */
	@Autowired
	protected IAnalizatorDao pdao;

	/**
	 * Initialization dao.
	 */
	@PostConstruct
	private void init() {
		this.dao = pdao;
	}
}
