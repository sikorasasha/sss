package org.minilis.impl.services;

import javax.annotation.PostConstruct;

import org.minilis.api.dao.IEmployeeDao;
import org.minilis.api.services.IEmployeeService;
import org.minilis.entity.dics.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class EmployeeService.
 */
@Service
@Transactional
public class EmployeeService extends BaseService<Employee> implements
		IEmployeeService {

	/** The EmployeeDao. */
	@Autowired
	protected IEmployeeDao pdao;

	/**
	 * Initialization dao.
	 */
	@PostConstruct
	private void init() {
		this.dao = pdao;
	}
}
