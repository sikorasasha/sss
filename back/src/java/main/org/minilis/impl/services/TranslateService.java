package org.minilis.impl.services;

import javax.annotation.PostConstruct;

import org.minilis.api.dao.ITranslateDao;
import org.minilis.api.services.ITranslateService;
import org.minilis.entity.config.Translation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class TranslateService.
 */
@Service
@Transactional
public class TranslateService extends BaseService<Translation> implements ITranslateService{

	 /** The TranslateDao. */
 	@Autowired
     protected ITranslateDao pdao;

	/**
	 * Initialization dao.
	 */
     @PostConstruct
     private void init() {
             this.dao = pdao;
     }
}
