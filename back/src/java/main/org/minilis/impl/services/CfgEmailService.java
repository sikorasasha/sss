package org.minilis.impl.services;

import javax.annotation.PostConstruct;

import org.minilis.api.dao.ICfgEmailDao;
import org.minilis.api.services.ICfgEmailService;
import org.minilis.entity.config.CfgEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class CfgEmailService.
 */
@Service
@Transactional
public class CfgEmailService extends BaseService<CfgEmail> implements
		ICfgEmailService {

	/** The CfgEmailDao. */
	@Autowired
	protected ICfgEmailDao pdao;

	/**
	 * Initialization dao.
	 */
	@PostConstruct
	private void init() {
		this.dao = pdao;
	}
}
