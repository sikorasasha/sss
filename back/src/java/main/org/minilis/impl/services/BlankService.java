package org.minilis.impl.services;

import javax.annotation.PostConstruct;

import org.minilis.api.dao.IBlankDao;
import org.minilis.api.services.IBlankService;
import org.minilis.entity.dics.Blank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class BlankService.
 */
@Service
@Transactional
public class BlankService extends BaseService<Blank> implements IBlankService{

	/** The BlankDao. */
	@Autowired
    protected IBlankDao pdao;

	/**
	 * Initialization dao.
	 */
	@PostConstruct
    private void init() {
            this.dao = pdao;
    }
}
