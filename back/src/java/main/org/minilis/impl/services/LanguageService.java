package org.minilis.impl.services;

import javax.annotation.PostConstruct;

import org.minilis.api.dao.ILanguageDao;
import org.minilis.api.services.ILanguageService;
import org.minilis.entity.config.Language;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class LanguageService.
 */
@Service
@Transactional
public class LanguageService extends BaseService<Language> implements ILanguageService{

	 /** The languageDao. */
 	@Autowired
     protected ILanguageDao pdao;

	/**
	 * Initialization dao.
	 */
     @PostConstruct
     private void init() {
             this.dao = pdao;
     }
}
