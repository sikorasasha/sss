package org.minilis.impl.services;

import javax.annotation.PostConstruct;

import org.minilis.api.dao.IResultFilesDao;
import org.minilis.api.services.IResultFilesService;
import org.minilis.entity.jors.ResultFiles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class ResultFilesService.
 */
@Service
@Transactional
public class ResultFilesService extends BaseService<ResultFiles> implements IResultFilesService{

	/** The resultFilesDao. */
	@Autowired
    protected IResultFilesDao pdao;

	/**
	 * Initialization dao.
	 */
    @PostConstruct
    private void init() {
            this.dao = pdao;
    }
}
