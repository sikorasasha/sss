/**
 * 
 */
package org.minilis.impl.services;

import javax.annotation.PostConstruct;

import org.minilis.api.dao.IPrintMarkerDao;
import org.minilis.api.services.IPrintMarkerService;
import org.minilis.entity.dics.PrintMarker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author sasha
 *
 */
@Service
public class PrintMarkerService extends BaseService<PrintMarker> implements IPrintMarkerService{

	 /** The languageDao. */
 	@Autowired
     protected IPrintMarkerDao pdao;

	/**
	 * Initialization dao.
	 */
     @PostConstruct
     private void init() {
             this.dao = pdao;
     }
}
