package org.minilis.impl.services;

import java.util.List;

import javax.annotation.PostConstruct;

import org.hibernate.Hibernate;
import org.hibernate.criterion.Criterion;
import org.minilis.api.dao.IJResultDao;
import org.minilis.api.services.IJResultService;
import org.minilis.entity.jors.JResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class JResultService.
 */
@Service
@Transactional
public class JResultService extends BaseService<JResult> implements	IJResultService {

	/** The JResultDao. */
	@Autowired
	protected IJResultDao pdao;

	/**
	 * Initialization dao.
	 */
	@PostConstruct
	private void init() {
		this.dao = pdao;
	}
	
	@Override
	public List<JResult> selectJResult(int from, int first,
			List<Criterion> filter) {

		List<JResult> l =pdao.selectGroupByHDIDBlankID(from, first, filter);
		init(l);
		return l;
	}
	
	@Override
	public void init(List<JResult> l) {
		for (int i = 0; i < l.size(); i++) {
			Hibernate.initialize(l.get(i).getIndicator());
		}
	}
}
