package org.minilis.impl.services;

import javax.annotation.PostConstruct;

import org.minilis.api.dao.IQualityControlDtDao;
import org.minilis.api.services.IQualityControlDtService;
import org.minilis.entity.jors.QualityControlDt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class QualityControlDtService.
 */
@Service
@Transactional
public class QualityControlDtService extends BaseService<QualityControlDt> implements IQualityControlDtService{

	/** The QualityControlDtDao. */
	@Autowired
    protected IQualityControlDtDao pdao;

	/**
	 * Initialization dao.
	 */
    @PostConstruct
    private void init() {
            this.dao = pdao;
    }
}
