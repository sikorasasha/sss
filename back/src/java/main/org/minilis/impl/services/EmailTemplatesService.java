/**
 * 
 */
package org.minilis.impl.services;

import javax.annotation.PostConstruct;

import org.minilis.api.dao.IEmailTemplatesDao;
import org.minilis.api.services.IEmailTemplatesService;
import org.minilis.entity.config.EmailTemplates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author sasha
 *
 */
@Service
@Transactional
public class EmailTemplatesService extends BaseService<EmailTemplates> implements IEmailTemplatesService{

	/** The CollectionOlaceDao. */
	@Autowired
	protected IEmailTemplatesDao pdao;

	/**
	 * Initialization dao.
	 */
	@PostConstruct
	private void init() {
		this.dao = pdao;
	}
}
