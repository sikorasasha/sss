package org.minilis.impl.services;

import javax.annotation.PostConstruct;

import org.minilis.api.dao.IRoleDao;
import org.minilis.api.services.IRoleService;
import org.minilis.entity.security.Role;
import org.minilis.impl.services.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class RoleService.
 */
@Service
@Transactional
public class RoleService extends BaseService<Role> implements IRoleService{
	
	/** The RoleDao. */
	@Autowired
    protected IRoleDao pdao;

	/**
	 * Initialization dao.
	 */
    @PostConstruct
    private void init() {
            this.dao = pdao;
    }

}
