package org.minilis.impl.services;

import javax.annotation.PostConstruct;

import org.minilis.api.dao.IQualityControlDao;
import org.minilis.api.services.IQualityControlService;
import org.minilis.entity.jors.QualityControl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class QualityControlService.
 */
@Service
@Transactional
public class QualityControlService extends BaseService<QualityControl> implements IQualityControlService{

	/** The QulaityControlDao. */
	@Autowired
    protected IQualityControlDao pdao;

	/**
	 * Initialization dao.
	 */
    @PostConstruct
    private void init() {
            this.dao = pdao;
    }
}
