package org.minilis.impl.services;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.minilis.api.dao.IDao;
import org.minilis.api.services.IService;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class BaseService.
 *
 * @param <T> the generic type
 */
@Transactional
public abstract class BaseService<T> implements IService<T>{

	/** The Base dao. */
	protected IDao<T> dao;
	
	/**
	 * Insert object using BaseDao.
	 *
	 * @param obj is inserting object
	 * @return inserted object
	 */
	@Override
	public T insert(T obj) {
		return dao.insert(obj);
	}

	/**
	 * Update object using BaseDao.
	 *
	 * @param obj is replacing object.
	 * @return updated object.
	 */
	@Override
	public T update(T obj) {
		return dao.update(obj);
	}

	/**
	 * Delete object using BaseDao.
	 *
	 * @param obj is deleting object.
	 */
	@Override
	public void delete(T obj) {
		dao.delete(obj);
		
	}

	/**
	 * Select objects using BaseDao.
	 *
	 * @param from is how many elements will selected.
	 * @param first is the first element from which the data will selected.
	 * @param filter is the list of criteria.
	 * @return all selected elements.
	 */
	@Override
	public List<T> select(int from, int first, List<Criterion> filter) {
		List<T> l=dao.select(from, first, filter);
		init(l);
		return l;
	}
	
	/**
	 * Select objects using BaseDao.
	 *
	 * @param filter is the list of criteria.
	 * @return all selected elements.
	 */
	@Override
	public List<T> select(List<Criterion> filter) {
		List<T> l=dao.select(filter);
		init(l);
		return l;
	}
	
	/**
	 * Initialization list of JOrders
	 */
	@Override
	public void init(List<T> l) {
		
	}
	
	@Override
	public T selectById(String id){
		return dao.selectById(id);
	}
}
