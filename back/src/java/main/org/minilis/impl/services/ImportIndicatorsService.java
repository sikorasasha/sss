package org.minilis.impl.services;

import javax.annotation.PostConstruct;

import org.minilis.api.dao.IImportIndicatorsDao;
import org.minilis.api.services.IImportIndicatorsService;
import org.minilis.entity.dics.ImportIndicators;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class ImportIndicatorsService.
 */
@Service
@Transactional
public class ImportIndicatorsService extends BaseService<ImportIndicators>
		implements IImportIndicatorsService {

	/** The ImportIndicatorDao. */
	@Autowired
	protected IImportIndicatorsDao pdao;

	/**
	 * Initialization dao.
	 */
	@PostConstruct
	private void init() {
		this.dao = pdao;
	}
}
