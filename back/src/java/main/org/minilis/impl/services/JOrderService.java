package org.minilis.impl.services;

import java.util.List;

import javax.annotation.PostConstruct;

import org.hibernate.Hibernate;
import org.hibernate.criterion.Criterion;
import org.minilis.api.dao.IJOrderDao;
import org.minilis.api.services.IJOrderService;
import org.minilis.entity.jors.JOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class JOrderService.
 */
@Service
@Transactional
public class JOrderService extends BaseService<JOrder> implements
		IJOrderService {

	/** The JOrderDao. */
	@Autowired
	protected IJOrderDao pdao;

	/**
	 * Initialization dao.
	 */
	@PostConstruct
	private void init() {
		this.dao = pdao;
	}

	/**
	 * Select JResults that related with current JOrder from DB.
	 *
	 * @param from is how many elements will selected.
	 * @param first is the first element from which the data will selected.
	 * @param filter is the list of criteria.
	 * @return all selected elements.
	 */
	@Override
	public List<JOrder> selectJResult(int from, int first,
			List<Criterion> filter) {

		return pdao.selectJResult(from, first, filter);
	}

	/**
	 * Select JResults, Indicators that related with current JOrder from DB.
	 *
	 * @param from is how many elements will selected.
	 * @param first is the first element from which the data will selected.
	 * @param filter is the list of criteria.
	 * @return all selected elements.
	 */
	@Override
	public List<JOrder> selectJResultIndicator(int from, int first,
			List<Criterion> filter) {
		List<JOrder> l = pdao.selectJResultIndicator(from, first, filter);
		init(l);
		return l;
	}

	/**
	 * Initialization list of JOrders
	 */
	@Override
	public void init(List<JOrder> l) {
		for (int i = 0; i < l.size(); i++) {
			Hibernate.initialize(l.get(i).getjResult());
		}
	}
}
