package org.minilis.impl.services;

import javax.annotation.PostConstruct;

import org.minilis.api.dao.IJLabProccessDao;
import org.minilis.api.services.IJLabProccessService;
import org.minilis.entity.jors.JLabProccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class JLabProccessService.
 */
@Service
@Transactional
public class JLabProccessService extends BaseService<JLabProccess> implements
		IJLabProccessService {

	/** The JLabPorccessDao. */
	@Autowired
	protected IJLabProccessDao pdao;

	/**
	 * Initialization dao.
	 */
	@PostConstruct
	private void init() {
		this.dao = pdao;
	}
}
