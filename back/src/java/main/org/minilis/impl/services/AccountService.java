package org.minilis.impl.services;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.minilis.api.dao.IAccountDao;
import org.minilis.api.dao.IAnalizatorDao;
import org.minilis.api.dao.IBlankDao;
import org.minilis.api.dao.ICfgEmailDao;
import org.minilis.api.dao.ICollectionPlaceDao;
import org.minilis.api.dao.IEmployeeDao;
import org.minilis.api.dao.IGrpIndicatorDao;
import org.minilis.api.dao.IIndicatorDao;
import org.minilis.api.dao.IJLabProccessDao;
import org.minilis.api.dao.IJOrderDao;
import org.minilis.api.dao.IJResultDao;
import org.minilis.api.dao.IQualityControlDao;
import org.minilis.api.dao.IQualityControlDtDao;
import org.minilis.api.dao.IResultFilesDao;
import org.minilis.api.helper.IConfig;
import org.minilis.api.services.IAccountService;
import org.minilis.entity.config.CfgEmail;
import org.minilis.entity.dics.Analizator;
import org.minilis.entity.dics.Blank;
import org.minilis.entity.dics.CollectionPlace;
import org.minilis.entity.dics.Employee;
import org.minilis.entity.dics.GrpIndicator;
import org.minilis.entity.dics.Indicator;
import org.minilis.entity.jors.JLabProccess;
import org.minilis.entity.jors.JOrder;
import org.minilis.entity.jors.JResult;
import org.minilis.entity.jors.QualityControl;
import org.minilis.entity.jors.QualityControlDt;
import org.minilis.entity.jors.ResultFiles;
import org.minilis.entity.security.Account;
import org.minilis.impl.services.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class AccountService.
 */
@Service
@Transactional
public class AccountService extends BaseService<Account> implements
		IAccountService {

	/** The AccountDao. */
	@Autowired
	protected IAccountDao pdao;
	
	/** The config. */
	@Autowired
	private IConfig conf;
	
	/** The ResultFilesDao. */
	@Autowired
	private IResultFilesDao iResultFilesDao;
	
	/** The CfgEmailDao. */
	@Autowired
	private ICfgEmailDao iCfgEmailDao;
	
	/** The JResultDao. */
	@Autowired
	private IJResultDao iJResultDao;
	
	/** The JLabProccessDao. */
	@Autowired
	private IJLabProccessDao iJLabProccessDao;
	
	/** The JOrderDao. */
	@Autowired
	private IJOrderDao iJOrderDao;
	
	/** The QualityControlDtDao. */
	@Autowired
	private IQualityControlDtDao iQualityControlDtDao;
	
	/** The QualityControlDao. */
	@Autowired
	private IQualityControlDao iQualityControlDao;
	
	/** The AnalizatorDao. */
	@Autowired
	private IAnalizatorDao iAnalizatorDao;
	
	/** The CollectionPlaceDao. */
	@Autowired
	private ICollectionPlaceDao iCollectionPlaceDao;
	
	/** The BlankDao. */
	@Autowired
	private IBlankDao iBlankDao;
	
	/** The EmployeeDao. */
	@Autowired
	private IEmployeeDao iEmployeeDao;
	
	/** The IndicatorDao. */
	@Autowired
	private IIndicatorDao iIndicatorDao;
	
	/** The GrpIndicatorDao. */
	@Autowired
	private IGrpIndicatorDao iGrpIndicatorDao;

	/**
	 * Initialization dao.
	 */
	@PostConstruct
	private void init() {
		this.dao = pdao;
	}

	/**
	 * Full deleting Account 
	 * delete all journals and account directory.
	 *
	 * @param obj is account.
	 */
	@Override
	public void delete(Account obj) {
		List<Criterion> crit = new ArrayList<Criterion>();
		crit.add(Restrictions.eq("id", obj.getId()));

		List<ResultFiles> resultFilesList = iResultFilesDao.select(0, 0, crit);
		for (ResultFiles element : resultFilesList) {
			iResultFilesDao.delete(element);
		}
		List<CfgEmail> cfgEmailList = iCfgEmailDao.select(0, 0, crit);
		for (CfgEmail element : cfgEmailList) {
			iCfgEmailDao.delete(element);
		}
		List<JResult> jResultList = iJResultDao.select(0, 0, crit);
		for (JResult element : jResultList) {
			iJResultDao.delete(element);
		}
		List<JLabProccess> jLabProccessList = iJLabProccessDao.select(0, 0,
				crit);
		for (JLabProccess element : jLabProccessList) {
			iJLabProccessDao.delete(element);
		}
		List<JOrder> jOrderList = iJOrderDao.select(0, 0, crit);
		for (JOrder element : jOrderList) {
			iJOrderDao.delete(element);
		}
		List<QualityControlDt> qualityControlDtList = iQualityControlDtDao
				.select(0, 0, crit);
		for (QualityControlDt element : qualityControlDtList) {
			iQualityControlDtDao.delete(element);
		}
		List<QualityControl> qualityControlList = iQualityControlDao.select(0,
				0, crit);
		for (QualityControl element : qualityControlList) {
			iQualityControlDao.delete(element);
		}
		List<Analizator> analizatorList = iAnalizatorDao.select(0, 0, crit);
		for (Analizator element : analizatorList) {
			iAnalizatorDao.delete(element);
		}
		List<CollectionPlace> collectionPlaceList = iCollectionPlaceDao.select(
				0, 0, crit);
		for (CollectionPlace element : collectionPlaceList) {
			iCollectionPlaceDao.delete(element);
		}
		List<Blank> blankList = iBlankDao.select(0, 0, crit);
		for (Blank element : blankList) {
			iBlankDao.delete(element);
		}
		List<Employee> employeeList = iEmployeeDao.select(0, 0, crit);
		for (Employee element : employeeList) {
			iEmployeeDao.delete(element);
		}
		List<Indicator> indicatorList = iIndicatorDao.select(0, 0, crit);
		for (Indicator element : indicatorList) {
			iIndicatorDao.delete(element);
		}
		List<GrpIndicator> grpIndicatorList = iGrpIndicatorDao.select(0, 0,
				crit);
		for (GrpIndicator element : grpIndicatorList) {
			iGrpIndicatorDao.delete(element);
		}
		
		conf.delAccDir(obj.getId());
		// List<Account> accountList = pdao.select(0, 0, crit);
		pdao.delete(obj);
	}
}
