package org.minilis.impl.services;

import javax.annotation.PostConstruct;

import org.minilis.api.dao.ICollectionPlaceDao;
import org.minilis.api.services.ICollectionPlaceService;
import org.minilis.entity.dics.CollectionPlace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class CollectionPlaceService.
 */
@Service
@Transactional
public class CollectionPlaceService extends BaseService<CollectionPlace>
		implements ICollectionPlaceService {

	/** The CollectionOlaceDao. */
	@Autowired
	protected ICollectionPlaceDao pdao;

	/**
	 * Initialization dao.
	 */
	@PostConstruct
	private void init() {
		this.dao = pdao;
	}
}
