package org.minilis.impl.security;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;

import org.apache.commons.codec.digest.DigestUtils;
import org.json.JSONObject;
import org.minilis.api.helper.IConfig;
import org.minilis.api.helper.IMailHelper;
import org.minilis.api.security.IRegistration;
import org.minilis.api.services.IAccountService;
import org.minilis.api.services.IEmployeeService;
import org.minilis.api.services.IRoleService;
import org.minilis.entity.dics.Employee;
import org.minilis.entity.security.Account;
import org.minilis.impl.helper.GlobalOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * The Class Registaration.
 */
@Controller
@Scope("session")
public class Registration implements IRegistration, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -2170561365410293674L;

	/** The employee service. */
	@Autowired
	public IEmployeeService employeeService;

	/** The account service. */
	@Autowired
	public IAccountService accountService;

	/** The role service. */
	@Autowired
	public IRoleService roleService;

	@Autowired
	public IConfig config;

	@Autowired
	private GlobalOptions options;

	@Autowired
	private IMailHelper mailHelper;

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.api.controllers.IRegistration#registaration(java.lang
	 * .String)
	 */
	@Override
	@RequestMapping(value = "/serv/public/regacc", method = RequestMethod.POST)
	public @ResponseBody String registration(@RequestBody String acc) {
		JSONObject jsonAccount = new JSONObject(acc);
		JSONObject jo = new JSONObject();
		if (config.getCurrentEmployee(jsonAccount.getString("email")) != null) {
			return config.alert("NOTVALIDEMAIL");
		}
		if (!config.validationSubDomen(jsonAccount.getString("domain"))) {
			return config.alert("NOTVALIDDOMAIN");
		}
		Account account = new Account();
		account.setName(jsonAccount.getString("org"));
		account.setDomain(jsonAccount.getString("domain").toLowerCase());
		account.setRole(roleService.select(0, 0, null).get(0));
		account.setLocale(jsonAccount.getString("locale"));
		account.setIsActive(true);
		account.setIsActivated(false);
		account.setDateReg(new Date());
		Employee employee = new Employee();
		employee.setIsActive(true);
		employee.setName(jsonAccount.getString("name"));
		employee.setEmail(jsonAccount.getString("email").toLowerCase());
		employee.setPass(DigestUtils.md5Hex(jsonAccount.getString("password")
				.getBytes()));
		employee.setSuperUser(true);
		employee.setAccount(accountService.insert(account));
		employee = employeeService.insert(employee);
		config.getAccountPath(account.getId());
		/*
		 * AuthenticationImpl auth = new AuthenticationImpl();
		 * auth.initUser(employee.getEmail(), employee.getPass(), employee
		 * .getAccount().getRole().getName(), true);
		 *
		 * SecurityContextHolder.getContext().setAuthentication(auth);
		 */
		// Send mail registration
		HashMap<String, String> parameters = new HashMap<String, String>();
		String activationUrl = "http://" + account.getDomain() + "."
				+ options.getSiteUrl() + "/serv/public/activation/employee/"
				+ employee.getId();
		parameters.put("[[%ORGNAME%]]", account.getName());
		parameters.put("[[%URL%]]", "www." + options.getSiteUrl());
		parameters.put("[[%ACTIVATION_URL%]]", activationUrl);

		config.sendMail(employee.getEmail(), mailHelper.getSubject(
				employee.getEmail(), parameters, "registration_subject_"),
				mailHelper.getContext(employee.getEmail(), parameters,
						"registration_context_"));

		// Create keyUUID with email and password
		String keyUUID = config.getUUID(employee.getEmail(),
				jsonAccount.getString("password"));

		jo.put("responce", true)
				.put("authkey", keyUUID)
				.put("domain", account.getDomain() + "." + options.getSiteUrl())
				.toString();
		return jo.toString();
	}

	@Override
	@RequestMapping(value = "/serv/public/getdomain", method = RequestMethod.POST)
	public @ResponseBody String getDomain(@RequestBody String data) {
		JSONObject jo1 = new JSONObject(data);
		JSONObject jo = new JSONObject();
		if (config.validationSubDomen(jo1.getString("request"))) {
			jo.put("responce", false);
			return jo.toString();
		} else {
			jo.put("responce", true);
			return jo.toString();
		}
	}

	@Override
	@RequestMapping(value = "/serv/public/getemail", method = RequestMethod.POST)
	public @ResponseBody String getEmail(@RequestBody String data) {
		JSONObject jo1 = new JSONObject(data);
		JSONObject jo = new JSONObject();
		if (config.getCurrentEmployee(jo1.getString("request")) == null) {
			jo.put("responce", false);
			return jo.toString();
		} else {
			jo.put("responce", true);
			return jo.toString();
		}
	}


}
