/**
 *
 */
package org.minilis.impl.security;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;

import org.apache.commons.codec.digest.DigestUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.minilis.api.helper.IConfig;
import org.minilis.api.helper.IMailHelper;
import org.minilis.api.security.IPassRecoverController;
import org.minilis.api.services.IEmployeeService;
import org.minilis.entity.dics.Employee;
import org.minilis.impl.helper.GlobalOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author sasha
 *
 */
@Controller
@Scope("session")
public class PassRecoverController implements IPassRecoverController,
		Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -5573199786564378959L;

	@Autowired
	private IEmployeeService employeeService;

	@Autowired
	public IConfig config;

	@Autowired
	private GlobalOptions options;

	@Autowired
	private IMailHelper mailHelper;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.minilis.front.api.controllers.IPassRecoverController#sendRecoverLink
	 * (java.lang.String)
	 */
	@Override
	@RequestMapping(value = "/serv/public/recoverpass", method = RequestMethod.POST)
	public @ResponseBody String sendRecoverLink(@RequestBody String data) {
		JSONObject jo = new JSONObject(data);
		Employee empl = config.getCurrentEmployee(jo.getString("email"));
		// check for employee, if not exist return alert("email not exist")
		if (empl != null) {
			// check time activation for account, if false return alert
			if (config.checkActivationTimeout(empl.getAccount())) {
				return config.alert("ERRORRECOVERACTIVATE");
			}
			// creating parameters for mail
			HashMap<String, String> parameters = new HashMap<String, String>();
			String recoveryUrl = "http://" + empl.getAccount().getDomain()
					+ "." + options.getSiteUrl() + "/#/passnew/" + empl.getId();

			parameters.put("[[%URL%]]", "www." + options.getSiteUrl());
			parameters.put("[[%RECOVERY_URL%]]", recoveryUrl);
			// sending mail
			config.sendMail(empl.getEmail(), mailHelper.getSubject(
					empl.getEmail(), parameters, "passrecovery_subject_"),
					mailHelper.getContext(empl.getEmail(), parameters,
							"passrecovery_context_"));
			// creating date for link recovery
			Timestamp dateRecoveryPass = new Timestamp(new Date().getTime());
			empl.setDateRecoveryPass(dateRecoveryPass);
			employeeService.update(empl);

		} else {
			return config.alert("EMAILISNOTEXISTS");
		}
		return new JSONObject()
				.put("responce", true)
				.put("alertsData",
						new JSONObject().put(
								"alerts",
								new JSONArray().put(new JSONObject().put(
										"type", 0).put("msg",
										"PASSRECOVERLINKWASSENTONEMAIL"))))
				.toString();
	}

	@RequestMapping(value = "/serv/public/getdaterecoverlink", method = RequestMethod.POST)
	public @ResponseBody String getDateRecoverLink(
			@RequestBody String dateRecoveryPass) {
		JSONObject jsonAlert = new JSONObject();
		Employee empl = employeeService.selectById(dateRecoveryPass);
		if (empl != null) {
			if (config.checkRecoverLinkTimeout(empl)) {
				return config.alert("RECOVERLINKISNOTVALID");
			} else {
				jsonAlert.put("responce", true);
				return jsonAlert.toString();
			}
		} else {
			return config.alert("EMPLOYEEIDISNOTVALID");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.minilis.front.api.controllers.IPassRecoverController#changePass(java
	 * .lang.String)
	 */
	@Override
	@RequestMapping(value = "/serv/public/changepass", method = RequestMethod.POST)
	public @ResponseBody String changePass(@RequestBody String data) {
		JSONObject js = new JSONObject(data);
		JSONObject jo = new JSONObject();
		String keyUUID;
		Employee employee = employeeService.selectById(js.getString("id"));
		if (employee != null) {
			employee.setPass(DigestUtils.md5Hex(js.getString("password")
					.getBytes()));
			employee.setDateRecoveryPass(null);
			employeeService.update(employee);

			// Create keyUUID with email and password
			keyUUID = config.getUUID(employee.getEmail(),
					js.getString("password"));

		} else {
			return config.alert("EMPLOYEEIDISNOTVALID");
		}
		jo.put("responce", true)
				.put("authkey", keyUUID)
				.put("domain",
						employee.getAccount().getDomain() + "."
								+ options.getSiteUrl()).toString();
		return jo.toString();
	}

}
