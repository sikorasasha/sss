package org.minilis.impl.security;

import java.io.Serializable;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;
import org.minilis.api.helper.IConfig;
import org.minilis.api.helper.IMailHelper;
import org.minilis.api.security.IActivation;
import org.minilis.api.security.ILoginController;
import org.minilis.api.services.IAccountService;
import org.minilis.api.services.IEmployeeService;
import org.minilis.entity.dics.Employee;
import org.minilis.impl.helper.GlobalOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * The Class Activation.
 */
@Controller
@Scope("session")
public class Activation implements IActivation, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -6403854082142202382L;

	/** The employee service. */
	@Autowired
	private IEmployeeService employeeService;

	@Autowired
	private IAccountService accountService;

	@Autowired
	private ILoginController loginController;

	@Autowired
	private IConfig config;

	@Autowired
	private GlobalOptions options;

	@Autowired
	private IMailHelper mailHelper;

	/**
	 * Activating Employee
	 *
	 * @param id
	 *            is a Employee id
	 */
	@Override
	@RequestMapping(value = "/serv/public/activation/employee/{id}", method = RequestMethod.GET)
	public String activateEmployee(@PathVariable("id") String id) {
		if ((id != null) && (id != "")) {
			Employee empl = employeeService.selectById(id);
			if (empl != null) {
				empl.getAccount().setIsActivated(true);

				accountService.update(empl.getAccount());

				AuthenticationImpl auth = new AuthenticationImpl();
				auth.initUser(empl.getEmail(), empl.getPass(), empl
						.getAccount().getRole().getName(), true);
				SecurityContextHolder.getContext().setAuthentication(auth);

				HashMap<String, String> parameters = new HashMap<String, String>();
				String domainUrl = "http://" + empl.getAccount().getDomain()
						+ '.' + options.getSiteUrl();
				parameters.put("[[%URL%]]", "www." + options.getSiteUrl());
				parameters.put("[[%DOMAIN_URL%]]", domainUrl);

				config.sendMail(empl.getEmail(), mailHelper.getSubject(
						empl.getEmail(), parameters, "activation_subject_"),
						mailHelper.getContext(empl.getEmail(), parameters,
								"activation_context_"));

			}
		}
		return "redirect:/";
	}

	@Override
	@RequestMapping(value = "/serv/public/activation", method = RequestMethod.POST)
	public @ResponseBody String activation(@RequestBody String acc) {
		JSONObject jsonAccount = new JSONObject(acc);
		config.delAuthemail(jsonAccount.getString("authkey"));
		config.delAuthpass(jsonAccount.getString("authkey"));
		Employee empl = config.getCurrentEmployee(jsonAccount
				.getString("email"));
		if (empl != null) {
			HashMap<String, String> parameters = new HashMap<String, String>();
			String domainUrl = "http://" + empl.getAccount().getDomain() + '.'
					+ options.getSiteUrl()
					+ "/serv/public/activation/employee/" + empl.getId();
			parameters.put("[[%URL%]]", "www." + options.getSiteUrl());
			parameters.put("[[%DOMAIN_URL%]]", domainUrl);

			config.sendMail(empl.getEmail(), mailHelper.getSubject(
					empl.getEmail(), parameters, "activation_subject_"),
					mailHelper.getContext(empl.getEmail(), parameters,
							"activation_context_"));

		} else {
			return config.alert("EMAILISNOTEXISTS");
		}
		return new JSONObject().put("responce", true).put(
				"alertsData",
				new JSONObject().put("alerts", new JSONArray()
						.put(new JSONObject().put("type", 0).put("msg",
								"ACTIVATIONLINKWASSENTONEMAIL")))).toString();
	}

}
