package org.minilis.impl.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

 /**
 * The Class AuthenticationImpl.
 */
public class AuthenticationImpl implements Authentication {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4783464302336930902L;

	/** The user. */
	private User us;
	
	/** The author list. */
	private List<GrantedAuthority> authList;
	
	/** The is authenticated. */
	private boolean isAuthenticated = true;
	
	/**
	 * Inits the user.
	 *
	 * @param username the username
	 * @param pass the pass
	 * @param role the role
	 * @param enabled the enabled
	 */
	public void initUser(String username, String pass, String role, boolean enabled) {
		authList = new ArrayList<GrantedAuthority>(2);
		authList.add(new SimpleGrantedAuthority(role));
		us = new User(username, pass, enabled, false, false, false, authList);
	}
	
	/* (non-Javadoc)
	 * @see java.security.Principal#getName()
	 */
	@Override
	public String getName() {
		return us.getUsername();
	}

	/* (non-Javadoc)
	 * @see org.springframework.security.core.Authentication#getAuthorities()
	 */
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return us.getAuthorities();
	}

	/* (non-Javadoc)
	 * @see org.springframework.security.core.Authentication#getCredentials()
	 */
	@Override
	public Object getCredentials() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.springframework.security.core.Authentication#getDetails()
	 */
	@Override
	public Object getDetails() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.springframework.security.core.Authentication#getPrincipal()
	 */
	@Override
	public Object getPrincipal() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.springframework.security.core.Authentication#isAuthenticated()
	 */
	@Override
	public boolean isAuthenticated() {
		return this.isAuthenticated;
	}

	/* (non-Javadoc)
	 * @see org.springframework.security.core.Authentication#setAuthenticated(boolean)
	 */
	@Override
	public void setAuthenticated(boolean isAuthenticated)
			throws IllegalArgumentException {
		this.isAuthenticated = isAuthenticated;

	}

}
