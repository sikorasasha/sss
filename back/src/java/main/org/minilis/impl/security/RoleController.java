package org.minilis.impl.security;

import java.io.Serializable;

import org.minilis.api.security.IRoleController;
import org.minilis.entity.security.Role;
import org.minilis.impl.controllers.BaseController;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * The Class RoleController.
 */
@Controller
@Scope("session")
public class RoleController extends BaseController<Role> implements
		IRoleController, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 5658665315663910581L;

}
