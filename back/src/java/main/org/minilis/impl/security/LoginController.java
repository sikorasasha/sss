/**
 *
 */
package org.minilis.impl.security;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.minilis.api.helper.IConfig;
import org.minilis.api.security.ILoginController;
import org.minilis.api.services.IEmployeeService;
import org.minilis.entity.dics.Employee;
import org.minilis.impl.helper.GlobalOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author sasha
 *
 */
@Controller
@Scope("session")
public class LoginController implements ILoginController, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -9100829586994749868L;

	@Autowired
	public IConfig config;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private IEmployeeService employeeService;

	@Autowired
	private GlobalOptions options;

	@RequestMapping(value = "/serv/public/relogin/{authid}", method = RequestMethod.GET)
	@Override
	public String relogin(@PathVariable("authid") String authid) {
		if ((config.getAuthemail().containsKey(authid))
				&& (config.getAuthpass().containsKey(authid))) {
			Authentication request = new UsernamePasswordAuthenticationToken(
					config.getAuthemail().get(authid), config.getAuthpass()
							.get(authid));
			Authentication result = authenticationManager.authenticate(request);
			SecurityContextHolder.getContext().setAuthentication(result);
			Employee em = config.getCurrentEmployee(config.getAuthemail().get(
					authid));
			em.setLoginCount(em.getLoginCount() + 1);
			em.setLoginLast(new Date());
			employeeService.update(em);
			config.delAuthemail(authid);
			config.delAuthpass(authid);
		}
		return "redirect:/";
	}

	@RequestMapping(value = "/serv/public/login", method = RequestMethod.POST)
	@Override
	public @ResponseBody String login(@RequestBody String login) {
		if (login != null) {
			JSONObject jo1 = new JSONObject(login);

			List<Criterion> list = new ArrayList<Criterion>();
			list.add(Restrictions.eq("email", jo1.get("email").toString()
					.toLowerCase()));
			list.add(Restrictions.eq(
					"pass",
					DigestUtils.md5Hex(jo1.get("password").toString()
							.getBytes())));
			List<Employee> empls = employeeService.select(0, 0, list);
			if (empls.size() != 0) {
				Employee employee = empls.get(0);

				if (employee.getAccount().getIsActive() == false) {
					return config.alert("ACCOUNTISBLOCKED");
				}

				if (config.checkActivationTimeout(employee.getAccount())) {
					String keyUUID = config.getUUID(employee.getEmail(),
							jo1.getString("password"));
					return new JSONObject()
							.put("responce", false)
							.put("activationlink",
									"http://"
											+ employee.getAccount().getDomain()
											+ "." + options.getSiteUrl()
											+ "/#/activation/" + keyUUID + "/"
											+ employee.getEmail())
							.put("alertsData",
									new JSONObject().put(
											"alerts",
											new JSONArray()
													.put(new JSONObject()
															.put("type", 2)
															.put("msg",
																	"ACTIVATIONTIMEOUT"))))
							.toString();

				}
				String keyUUID = config.getUUID(employee.getEmail(),
						jo1.getString("password"));

				return new JSONObject()
						.put("responce", true)
						.put("authkey", keyUUID)
						.put("domain",
								employee.getAccount().getDomain() + "."
										+ options.getSiteUrl()).toString();
			} else {
				return config.alert("LOGINORPASSISNOTDEF");
			}
		}
		return null;
	}

	@RequestMapping(value = "/serv/public/checkauth/{authid}", method = RequestMethod.GET)
	@Override
	public @ResponseBody String checkAuth(@PathVariable("authid") String authKey) {
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		if ((authentication != null)
				&& (authentication.getName() != "anonymousUser")|| ((config.getAuthemail().containsKey(authKey))
						&& (config.getAuthpass().containsKey(authKey))))
			return new JSONObject().put("responce", true).toString();
		else
			return new JSONObject().put("responce", false).toString();

	}

}
