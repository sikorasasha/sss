package org.minilis.impl.controllers;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.imageio.ImageIO;
import javax.sql.DataSource;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import org.hibernate.criterion.Criterion;
import org.json.JSONObject;
import org.minilis.api.controllers.IAccountController;
import org.minilis.api.helper.IConfig;
import org.minilis.api.helper.IContextHelper;
import org.minilis.api.services.IAccountService;
import org.minilis.entity.security.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 * The Class AccountController.
 */
@Controller
@Scope("session")
public class AccountController extends BaseController<Account> implements
		IAccountController, Serializable {

	@Autowired
	private IContextHelper context;

	@Autowired
	private DataSource dataSource;

	@Autowired
	private IConfig config;

	@Autowired
	private IAccountService accountService;

	/**
	 *
	 */
	private static final long serialVersionUID = -6958167711538977065L;

	/**
	 * Select Accounts.
	 *
	 * @param from
	 *            is how many elements will selected.
	 * @param first
	 *            is the first element from which the data will selected.
	 * @param filter
	 *            is the list of criteria.
	 * @return all selected elements.
	 */
	@Override
	@RequestMapping(value = "/serv/private/account", method = RequestMethod.GET)
	public @ResponseBody List<Account> select(@RequestBody int from,
			@RequestBody int first, @RequestBody List<Criterion> filter) {
		return super.select(from, first, filter);
	}

	/**
	 * Update Account.
	 *
	 * @param obj
	 *            is replacing account.
	 * @return updated account.
	 */
	@Override
	@RequestMapping(value = "/serv/private/account/upd", method = RequestMethod.GET)
	public @ResponseBody Account update(@RequestBody Account obj) {
		return super.update(obj);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@RequestMapping(value = "/serv/private/private/previeworder", method = RequestMethod.POST)
	public @ResponseBody String showPreviewReport(@RequestBody String idAccount) {
		JasperReport report = null;
		BufferedImage image = null;
		HashMap parameterMap = new HashMap();
		try {
			// компиляция отчета
			report = JasperCompileManager.compileReport(context.getResource(
					"classpath:reports/napravlenia/napravlenia.jrxml")
					.getInputStream());
			// проверка на иснування картинки в папки
			if ((new File(context.getRealPath("/" + idAccount + "/logo.jpg")))
					.exists()) {
				//загрузка картинки вместо параметра
				InputStream in = new BufferedInputStream(new FileInputStream(
						context.getRealPath("/" + idAccount + "/logo.jpg")));
				image = ImageIO.read(in);
				parameterMap.put("img", image);
			}
		} catch (JRException | IOException e) {
			e.printStackTrace();
		}
		// добавление параметров в отчет
		parameterMap.put(JRParameter.REPORT_LOCALE, new Locale(accountService
				.selectById(idAccount).getLocale()));
		parameterMap.put("IdAcc", idAccount);
		JasperPrint print = null;
		try {
			print = JasperFillManager.fillReport(report, parameterMap,
					dataSource.getConnection());
		} catch (JRException | SQLException e) {
			e.printStackTrace();
		}

		try {
			// експорт отчета в pdf
			JasperExportManager.exportReportToPdfFile(
					print,
					context.getRealPath("/" + idAccount + "/temp/" + idAccount
							+ ".pdf"));
		} catch (JRException e) {
			e.printStackTrace();
		}
		return new JSONObject()
				.put("responce", true)
				.put("url",
						context.getRealPath("/" + idAccount + "/temp/"
								+ idAccount + ".pdf")).toString();
	}

	@Override
	@RequestMapping(value = "/serv/private/savelogo", method = RequestMethod.POST)
	public @ResponseBody String savefile(MultipartHttpServletRequest request) {
		Iterator<String> itr = request.getFileNames();
		MultipartFile file = request.getFile(itr.next());
		String fileName = "logo";
		try {
			//сохранение файла
			saveToFile(file.getBytes(), fileName);
			InputStream is = file.getInputStream();
			Image img = ImageIO.read(is);
			File flv = new File(fileName);
			// изменение размера картинки
			ImageIO.write(createResizedCopy(img, 100, 100, true), "jpg", flv);
			// сохранение измененой картинки
			saveToFile(Files.readAllBytes(flv.toPath()), "100_" + fileName);
		} catch (IOException e) {
			return config.alert("NOTUPLOADFILE");
		}
		return new JSONObject().put("responce", true).toString();
	}

	// метод сохранения файла в папку с айди аккаунта
	private boolean saveToFile(byte[] file, String fileName) {
		config.getCurrentEmployee();
		String absoluteDiskPath = context.getRealPath("/"
				+ config.getCurrentEmployee().getAccount().getId() + "/"
				+ fileName);
		File fl = new File(absoluteDiskPath);
		if (!fl.exists())
			try {
				fl.createNewFile();
				FileOutputStream out;
				out = new FileOutputStream(fl);
				out.write(file);
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
		return true;
	}

	// метод изменяет размер картинки
	private BufferedImage createResizedCopy(Image originalImage,
			int scaledWidth, int scaledHeight, boolean preserveAlpha) {
		int imageType = preserveAlpha ? BufferedImage.TYPE_INT_RGB
				: BufferedImage.TYPE_INT_ARGB;
		BufferedImage scaledBI = new BufferedImage(scaledWidth, scaledHeight,
				imageType);
		Graphics2D g = scaledBI.createGraphics();
		if (preserveAlpha) {
			g.setComposite(AlphaComposite.Src);
		}
		g.drawImage(originalImage, 0, 0, scaledWidth, scaledHeight, null);
		g.dispose();
		return scaledBI;
	}
}