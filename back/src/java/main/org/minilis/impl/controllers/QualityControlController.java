package org.minilis.impl.controllers;

import java.io.Serializable;

import org.minilis.api.controllers.IQualityControlController;
import org.minilis.entity.jors.QualityControl;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * The Class QualityControlController.
 */
@Controller
@Scope("session")
public class QualityControlController extends BaseController<QualityControl>
		implements IQualityControlController, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -677776107695093611L;

}
