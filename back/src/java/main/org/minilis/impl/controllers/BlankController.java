package org.minilis.impl.controllers;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.json.JSONObject;
import org.minilis.api.controllers.IBlankController;
import org.minilis.api.services.IBlankService;
import org.minilis.entity.dics.Blank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 * The Class BlankController.
 */
@Controller
@Scope("session")
public class BlankController extends BaseController<Blank> implements
		IBlankController, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -4358521117702248861L;
	/** The blank service. */
	@Autowired
	private IBlankService blankService;

	/*
	 * (non-Javadoc)
	 *
	 * @see org.minilis.front.impl.controllers.BaseController#select(int, int,
	 * java.util.List)
	 */
	@Override
	@RequestMapping(value = "/serv/private/blanks", method = RequestMethod.GET)
	public @ResponseBody List<Blank> select(@RequestBody int from,
			@RequestBody int first, @RequestBody List<Criterion> filter) {
		return super.select(from, first, filter);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.impl.controllers.BaseController#insert(java.lang.Object
	 * )
	 */
	@Override
	@RequestMapping(value = "/serv/private/blanks/add", method = RequestMethod.POST)
	public @ResponseBody Blank insert(@RequestBody Blank obj) {
		return super.insert(obj);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.impl.controllers.BaseController#update(java.lang.Object
	 * )
	 */
	@Override
	@RequestMapping(value = "/serv/private/blanks/upd", method = RequestMethod.POST)
	public @ResponseBody Blank update(@RequestBody Blank obj) {
		return super.update(obj);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.impl.controllers.BaseController#delete(java.lang.Object
	 * )
	 */
	@Override
	@RequestMapping(value = "/serv/private/blanks/del", method = RequestMethod.POST)
	public void delete(@RequestBody Blank obj) {
		super.delete(obj);

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.api.controllers.IBlankController#importBlank(java.lang
	 * .String, org.springframework.web.multipart.MultipartHttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@Override
	@RequestMapping(value = "/serv/private/blanks/import", method = RequestMethod.POST)
	public void importBlank(@RequestBody String id,
			MultipartHttpServletRequest request, HttpServletResponse response) {
		List<Criterion> list = new ArrayList<Criterion>();
		list.add(Restrictions.eq("id", id));
		Blank blank = blankService.select(0, 0, list).get(0);
		Iterator<String> itr = request.getFileNames();
		MultipartFile file = request.getFile(itr.next());
		byte[] byteArray = null;
		try {
			byteArray = file.getBytes();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Byte[] byteObjects = new Byte[byteArray.length];
		int k = 0;
		for (byte b : byteArray)
			byteObjects[k++] = b;
		blank.setBody(byteObjects);
		blankService.update(blank);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.api.controllers.IBlankController#exportBlank(java.lang
	 * .String)
	 */
	@Override
	@RequestMapping(value = "/serv/private/blanks/export", method = RequestMethod.GET)
	public JSONObject exportBlank(String id) {
		List<Criterion> list = new ArrayList<Criterion>();
		list.add(Restrictions.eq("id", id));
		Blank blank = blankService.select(1, 0, list).get(0);
		int j = 0;
		byte[] bytes = new byte[blank.getBody().length];
		for (Byte b : blank.getBody())
			bytes[j++] = b.byteValue();
		JSONObject obj = new JSONObject();
		obj.put("name", blank.getName());
		obj.put("body", new String(bytes));
		return obj;
	}

}
