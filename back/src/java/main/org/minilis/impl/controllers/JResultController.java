package org.minilis.impl.controllers;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletContext;
import javax.sql.DataSource;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.minilis.api.controllers.IJResultController;
import org.minilis.api.services.IJResultService;
import org.minilis.entity.jors.JResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * The Class JResultController.
 */
@Controller
@Scope("session")
public class JResultController extends BaseController<JResult> implements
		IJResultController, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 8736008110623017199L;

	/** The JResultService. */
	@Autowired
	private IJResultService jResultService;

	/** The context. */
	@Autowired
	private ServletContext context;

	/** The data source. */
	@Autowired
	private DataSource dataSource;

	/*
	 * (non-Javadoc)
	 *
	 * @see org.minilis.front.impl.controllers.BaseController#select(int, int,
	 * java.util.List)
	 */
	@Override
	@RequestMapping(value = "/serv/private/results", method = RequestMethod.GET)
	public @ResponseBody List<JResult> select(@RequestBody int from,
			@RequestBody int first, @RequestBody List<Criterion> filter) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.impl.controllers.BaseController#update(java.lang.Object
	 * )
	 */
	@Override
	@RequestMapping(value = "/serv/private/results/upd", method = RequestMethod.POST)
	public @ResponseBody JResult update(@RequestBody JResult obj) {
		return obj;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.api.controllers.IJResultController#selectByOrder(java
	 * .lang.String)
	 */
	@Override
	@RequestMapping(value = "/serv/private/results/order", method = RequestMethod.GET)
	public @ResponseBody List<JResult> selectByOrder(@RequestBody String id) {
		List<Criterion> list = new ArrayList<Criterion>();
		list.add(Restrictions.eq("hdid", id));
		return jResultService.select(0, 0, list);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.api.controllers.IJResultController#sendEmailByOrder
	 * (java.lang.String)
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	@RequestMapping(value = "/serv/private/results/emailbyorder", method = RequestMethod.POST)
	public void sendEmailByOrder(@RequestBody String id) {
		JasperPrint print = null;
		List<JasperPrint> printList = new ArrayList<JasperPrint>();
		try {
			List<Criterion> list = new ArrayList<Criterion>();
			list.add(Restrictions.eq("hdid", id));
			List<JResult> jResultList = jResultService.select(0, 0, list);
			for (int i = 0; i < jResultList.size() - 1; i++) {
				for (int j = i + 1; j < jResultList.size(); j++) {
					if (jResultList
							.get(i)
							.getIndicator()
							.getBlank()
							.getId()
							.equals(jResultList.get(j).getIndicator()
									.getBlank().getId())) {
						jResultList.remove(j);
					}
				}
			}
			for (int i = 0; i < jResultList.size(); i++) {
				JasperReport report = JasperCompileManager
						.compileReport(context
								.getRealPath("/reports/result/Blank_A4.jrxml"));
				JasperCompileManager.compileReportToFile(
						context.getRealPath("/reports/result/Title.jrxml"),
						context.getRealPath("/WEB-INF/classes/Title.jasper"));
				HashMap parameterMap = new HashMap();
				parameterMap.put(JRParameter.REPORT_LOCALE, Locale.ENGLISH);
				parameterMap.put("ID", id);
				parameterMap.put("BLANK", jResultList.get(i).getIndicator()
						.getBlank().getId());
				print = JasperFillManager.fillReport(report, parameterMap,
						dataSource.getConnection());
				printList.add(print);
				JasperExportManager.exportReportToPdfFile(print,
						"/home/sasha/1.pdf");
			}
		} catch (JRException | SQLException e1) {
			e1.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.api.controllers.IJResultController#closeByOrder(java
	 * .lang.String)
	 */
	@Override
	@RequestMapping(value = "/serv/private/results/closebyorder", method = RequestMethod.POST)
	public void closeByOrder(@RequestBody String id) {
		List<Criterion> list = new ArrayList<Criterion>();
		list.add(Restrictions.eq("hdid", id));
		list.add(Restrictions.isNull("dateTimeVerified"));
		List<JResult> jResultList = jResultService.select(0, 0, list);
		for (int i = 0; i < jResultList.size(); i++) {
			jResultList.get(i).setDateTimeVerified(new Date());
			jResultService.update(jResultList.get(i));
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.api.controllers.IJResultController#selectByLabProc(
	 * java.lang.String)
	 */
	@Override
	@RequestMapping(value = "/serv/private/results/labproc", method = RequestMethod.GET)
	public List<JResult> selectByLabProc(String idLabProc) {
		List<Criterion> list = new ArrayList<Criterion>();
		list.add(Restrictions.eq("labprocId", idLabProc));
		return jResultService.select(0, 0, list);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.api.controllers.IJResultController#selectFreeResults()
	 */
	@Override
	@RequestMapping(value = "/serv/private/results/free", method = RequestMethod.GET)
	public List<JResult> selectFreeResults() {
		List<Criterion> list = new ArrayList<Criterion>();
		list.add(Restrictions.isNull("labprocId"));
		list.add(Restrictions.isNull("dateTimeVerified"));
		return jResultService.select(0, 0, list);
	}

}
