package org.minilis.impl.controllers;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.minilis.api.controllers.IAnalizatorController;
import org.minilis.entity.dics.Analizator;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * The Class AnalizatorController.
 */
@Controller
@Scope("session")
public class AnalizatorController extends BaseController<Analizator> implements
		IAnalizatorController, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 4147538614974327382L;

	/**
	 * Select Analizators.
	 *
	 * @param from
	 *            is how many elements will selected.
	 * @param first
	 *            is the first element from which the data will selected.
	 * @param filter
	 *            is the list of criteria.
	 * @return all selected elements.
	 */
	@Override
	@RequestMapping(value = "/serv/private/analizators", method = RequestMethod.GET)
	public @ResponseBody List<Analizator> select(@RequestBody int from,
			@RequestBody int first, @RequestBody List<Criterion> filter) {
		return super.select(from, first, filter);
	}

	/**
	 * Insert Analizator.
	 *
	 * @param obj
	 *            is analizator.
	 * @return inserted analizator.
	 */
	@Override
	@RequestMapping(value = "/serv/private/analizators/add", method = RequestMethod.POST)
	public @ResponseBody Analizator insert(@RequestBody Analizator obj) {
		return super.insert(obj);
	}

	/**
	 * Update Analizator.
	 *
	 * @param obj
	 *            is analizator.
	 * @return updated analizator.
	 */
	@Override
	@RequestMapping(value = "/serv/private/analizators/upd", method = RequestMethod.POST)
	public @ResponseBody Analizator update(@RequestBody Analizator obj) {
		return super.update(obj);
	}

	/**
	 * Delete Analizator.
	 *
	 * @param obj
	 *            is deleting analizator.
	 */
	@Override
	@RequestMapping(value = "/serv/private/analizators/del", method = RequestMethod.POST)
	public void delete(@RequestBody Analizator obj) {
		super.delete(obj);
	}
}
