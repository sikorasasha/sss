/**
 *
 */
package org.minilis.impl.controllers;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletContext;
import javax.sql.DataSource;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.minilis.api.controllers.IPrintController;
import org.minilis.api.helper.IConfig;
import org.minilis.api.services.IJResultService;
import org.minilis.entity.jors.JResult;
import org.minilis.entity.security.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * The Class PrintController.
 */
@Controller
@Scope("session")
public class PrintController implements IPrintController, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 5303816802437584389L;

	/** The config. */
	@Autowired
	private IConfig conf;

	/** The JResultService. */
	@Autowired
	private IJResultService jResultService;

	/** The context. */
	@Autowired
	private ServletContext context;

	/** The data source. */
	@Autowired
	private DataSource dataSource;

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.api.controllers.IPrintController#printBarCode(java.
	 * lang.String)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@RequestMapping(value = "/serv/private/printbarcode", method = RequestMethod.POST)
	public void printBarCode(@RequestBody String id) {
		JasperReport report = null;

		Account currentAccount = conf.getCurrentEmployee(
				SecurityContextHolder.getContext().getAuthentication()
						.getName()).getAccount();
		if (currentAccount.getTypeBarCode() == 0) {
			if (currentAccount.getBarcodeLength() == 0) {
				try {
					report = JasperCompileManager
							.compileReport(context
									.getRealPath("report/etiket/Codabar(NW-7)2_3.jrxml"));
				} catch (JRException e2) {
					e2.printStackTrace();
				}
			}
			if (currentAccount.getBarcodeLength() == 1) {
				try {
					report = JasperCompileManager
							.compileReport(context
									.getRealPath("report/etiket/Codabar(NW-7)4_2.5.jrxml"));
				} catch (JRException e2) {
					e2.printStackTrace();
				}
			}
			if (currentAccount.getBarcodeLength() == 2) {
				try {
					report = JasperCompileManager
							.compileReport(context
									.getRealPath("report/etiket/Codabar(NW-7)5.7_4.jrxml"));
				} catch (JRException e2) {
					e2.printStackTrace();
				}
			}
		}
		if (currentAccount.getTypeBarCode() == 1) {
			if (currentAccount.getBarcodeLength() == 0) {
				try {
					report = JasperCompileManager.compileReport(context
							.getRealPath("report/etiket/Code128_2_3.jrxml"));
				} catch (JRException e2) {
					e2.printStackTrace();
				}
			}
			if (currentAccount.getBarcodeLength() == 1) {
				try {
					report = JasperCompileManager.compileReport(context
							.getRealPath("report/etiket/Code128_4_2.5.jrxml"));
				} catch (JRException e2) {
					e2.printStackTrace();
				}
			}
			if (currentAccount.getBarcodeLength() == 2) {
				try {
					report = JasperCompileManager.compileReport(context
							.getRealPath("report/etiket/Code128_5.7_4.jrxml"));
				} catch (JRException e2) {
					e2.printStackTrace();
				}
			}
		}
		if (currentAccount.getTypeBarCode() == 2) {
			if (currentAccount.getBarcodeLength() == 0) {
				try {
					report = JasperCompileManager.compileReport(context
							.getRealPath("report/etiket/Code39_2_3.jrxml"));
				} catch (JRException e2) {
					e2.printStackTrace();
				}
			}
			if (currentAccount.getBarcodeLength() == 1) {
				try {
					report = JasperCompileManager.compileReport(context
							.getRealPath("report/etiket/Code39_4_2.5.jrxml"));
				} catch (JRException e2) {
					e2.printStackTrace();
				}
			}
			if (currentAccount.getBarcodeLength() == 2) {
				try {
					report = JasperCompileManager.compileReport(context
							.getRealPath("report/etiket/Code39_5.7_4.jrxml"));
				} catch (JRException e2) {
					e2.printStackTrace();
				}
			}
		}
		if (currentAccount.getTypeBarCode() == 3) {
			if (currentAccount.getBarcodeLength() == 0) {
				try {
					report = JasperCompileManager
							.compileReport(context
									.getRealPath("report/etiket/Interleaved2of5_2_3.jrxml"));
				} catch (JRException e2) {
					e2.printStackTrace();
				}
			}
			if (currentAccount.getBarcodeLength() == 1) {
				try {
					report = JasperCompileManager
							.compileReport(context
									.getRealPath("report/etiket/Interleaved2of5_4_2.5.jrxml"));
				} catch (JRException e2) {
					e2.printStackTrace();
				}
			}
			if (currentAccount.getBarcodeLength() == 2) {
				try {
					report = JasperCompileManager
							.compileReport(context
									.getRealPath("report/etiket/Interleaved2of5_5.7_4.jrxml"));
				} catch (JRException e2) {
					e2.printStackTrace();
				}
			}
		}
		HashMap parameterMap = new HashMap();
		parameterMap.put("ID", id);
		JasperPrint print = null;
		try {
			print = JasperFillManager.fillReport(report, parameterMap,
					dataSource.getConnection());
		} catch (JRException | SQLException e1) {
			e1.printStackTrace();
		}

		try {
			JasperExportManager.exportReportToPdfFile(print,
					context.getRealPath("codabar.pdf"));
		} catch (JRException e) {
			e.printStackTrace();
		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.api.controllers.IPrintController#printOrder(java.lang
	 * .String)
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	@RequestMapping(value = "/serv/private/printorder", method = RequestMethod.POST)
	public void printOrder(@RequestBody String id) {
		JasperReport report = null;
		try {
			report = JasperCompileManager.compileReport(context
					.getRealPath("/reports/napravlenia/Blank_A4.jrxml"));
		} catch (JRException e3) {
			e3.printStackTrace();
		}
		try {
			JasperCompileManager.compileReportToFile(
					context.getRealPath("/reports/napravlenia/Title.jrxml"),
					context.getRealPath("/WEB-INF/classes/Title.jasper"));
		} catch (JRException e2) {
			e2.printStackTrace();
		}
		HashMap parameterMap = new HashMap();
		parameterMap.put(JRParameter.REPORT_LOCALE, Locale.ENGLISH);
		parameterMap.put("ID", id);
		JasperPrint print = null;
		try {
			print = JasperFillManager.fillReport(report, parameterMap,
					dataSource.getConnection());
		} catch (JRException | SQLException e1) {
			e1.printStackTrace();
		}

		try {
			JasperExportManager.exportReportToPdfFile(print,
					context.getRealPath("napravlenia.pdf"));
		} catch (JRException e) {
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.api.controllers.IPrintController#printResultByOrder
	 * (java.lang.String)
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	@RequestMapping(value = "/serv/private/printresultbyorder", method = RequestMethod.POST)
	public void printResultByOrder(@RequestBody String id) {
		JasperPrint print = null;
		List<JasperPrint> printList = new ArrayList<JasperPrint>();
		try {
			List<Criterion> list = new ArrayList<Criterion>();
			list.add(Restrictions.eq("hdid", id));
			list.add(Restrictions.isNull("labprocId"));
			List<JResult> jResultList = jResultService.select(0, 0, list);
			for (int i = 0; i < jResultList.size() - 1; i++) {
				for (int j = i + 1; j < jResultList.size(); j++) {
					if (jResultList
							.get(i)
							.getIndicator()
							.getBlank()
							.getId()
							.equals(jResultList.get(j).getIndicator()
									.getBlank().getId())) {
						jResultList.remove(j);
					}
				}
			}
			for (int i = 0; i < jResultList.size(); i++) {
				JasperReport report = JasperCompileManager
						.compileReport(context
								.getRealPath("/reports/result/Blank_A4.jrxml"));
				JasperCompileManager.compileReportToFile(
						context.getRealPath("/reports/result/Title.jrxml"),
						context.getRealPath("/WEB-INF/classes/Title.jasper"));
				HashMap parameterMap = new HashMap();
				parameterMap.put(JRParameter.REPORT_LOCALE, Locale.ENGLISH);
				parameterMap.put("ID", id);
				parameterMap.put("BLANK", jResultList.get(i).getIndicator()
						.getBlank().getId());
				parameterMap.put("labprocid", "");
				print = JasperFillManager.fillReport(report, parameterMap,
						dataSource.getConnection());
				printList.add(print);
				JasperExportManager.exportReportToPdfFile(print,
						context.getRealPath("result_" + i + ".pdf"));
			}
		} catch (JRException | SQLException e1) {
			e1.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.api.controllers.IPrintController#printResultByLabProc()
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@RequestMapping(value = "/serv/private/printresultbylabproc", method = RequestMethod.POST)
	public @ResponseBody void printResultByLabProc(@RequestBody String idRes) {
		JasperPrint print = null;
		List<JasperPrint> printList = new ArrayList<JasperPrint>();
		try {
			List<Criterion> list = new ArrayList<Criterion>();
			list.add(Restrictions.eq("labprocId", idRes));
			List<JResult> jResultList = jResultService
					.selectJResult(0, 0, list);
			for (int i = 0; i < jResultList.size(); i++) {
				JasperReport report = JasperCompileManager
						.compileReport(context
								.getRealPath("/reports/result/Blank_A4.jrxml"));
				JasperCompileManager.compileReportToFile(
						context.getRealPath("/reports/result/Title.jrxml"),
						context.getRealPath("/WEB-INF/classes/Title.jasper"));
				HashMap parameterMap = new HashMap();
				parameterMap.put(JRParameter.REPORT_LOCALE, Locale.ENGLISH);
				parameterMap.put("ID", jResultList.get(i).getHdId());
				parameterMap.put("BLANK", jResultList.get(i).getIndicator()
						.getBlank().getId());
				parameterMap.put("labprocid", idRes);
				print = JasperFillManager.fillReport(report, parameterMap,
						dataSource.getConnection());
				printList.add(print);
				JasperExportManager.exportReportToPdfFile(print,
						context.getRealPath("result_" + i + ".pdf"));
			}
		} catch (JRException | SQLException e1) {
			e1.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.api.controllers.IPrintController#print250f(java.lang
	 * .String)
	 */
	@Override
	@RequestMapping(value = "/serv/private/print250f", method = RequestMethod.POST)
	public void print250f(@RequestBody String idLabProc) {

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.api.controllers.IPrintController#printQC(java.lang.
	 * String)
	 */
	@Override
	@RequestMapping(value = "/serv/private/printQC", method = RequestMethod.POST)
	public void printQC(@RequestBody String idLabProc) {
	}

}
