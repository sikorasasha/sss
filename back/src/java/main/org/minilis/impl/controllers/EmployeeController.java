package org.minilis.impl.controllers;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.minilis.api.controllers.IEmployeeController;
import org.minilis.entity.dics.Employee;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * The Class EmployeeController.
 */
@Controller
@Scope("session")
public class EmployeeController extends BaseController<Employee> implements
		IEmployeeController, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 490167696820263337L;

	/*
	 * (non-Javadoc)
	 *
	 * @see org.minilis.front.impl.controllers.BaseController#select(int, int,
	 * java.util.List)
	 */
	@Override
	@RequestMapping(value = "/serv/private/employees", method = RequestMethod.GET)
	public @ResponseBody List<Employee> select(@RequestBody int from,
			@RequestBody int first, @RequestBody List<Criterion> filter) {
		return super.select(from, first, filter);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.impl.controllers.BaseController#insert(java.lang.Object
	 * )
	 */
	@Override
	@RequestMapping(value = "/serv/private/employees/add", method = RequestMethod.POST)
	public @ResponseBody Employee insert(@RequestBody Employee obj) {
		return super.insert(obj);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.impl.controllers.BaseController#update(java.lang.Object
	 * )
	 */
	@Override
	@RequestMapping(value = "/serv/private/employees/upd", method = RequestMethod.POST)
	public @ResponseBody Employee update(@RequestBody Employee obj) {
		return super.update(obj);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.impl.controllers.BaseController#delete(java.lang.Object
	 * )
	 */
	@Override
	@RequestMapping(value = "/serv/private/employees/del", method = RequestMethod.POST)
	public void delete(@RequestBody Employee obj) {
	}

}
