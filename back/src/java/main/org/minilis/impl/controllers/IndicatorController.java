package org.minilis.impl.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.minilis.api.controllers.IIndicatorController;
import org.minilis.api.services.IGrpIndicatorService;
import org.minilis.api.services.IImportIndicatorsService;
import org.minilis.api.services.IIndicatorService;
import org.minilis.entity.dics.GrpIndicator;
import org.minilis.entity.dics.ImportIndicators;
import org.minilis.entity.dics.Indicator;
import org.minilis.entity.security.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * The Class IndicatorController.
 */
@Controller
@Scope("session")
public class IndicatorController extends BaseController<Indicator> implements
		IIndicatorController, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 857085687591446705L;

	/** The ImportIndicatorsService. */
	@Autowired
	private IImportIndicatorsService importIndicatorsService;

	/** The GrpIndicatorService. */
	@Autowired
	private IGrpIndicatorService grpIndicatorService;

	/** The IndicatorService. */
	@Autowired
	IIndicatorService indicatorService;

	/*
	 * (non-Javadoc)
	 *
	 * @see org.minilis.front.impl.controllers.BaseController#select(int, int,
	 * java.util.List)
	 */
	@Override
	@RequestMapping(value = "/serv/private/indicators", method = RequestMethod.GET)
	public @ResponseBody List<Indicator> select(@RequestBody int from,
			@RequestBody int first, @RequestBody List<Criterion> filter) {
		return super.select(from, first, filter);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.impl.controllers.BaseController#insert(java.lang.Object
	 * )
	 */
	@Override
	@RequestMapping(value = "/serv/private/indicators/add", method = RequestMethod.POST)
	public @ResponseBody Indicator insert(@RequestBody Indicator obj) {
		return super.insert(obj);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.impl.controllers.BaseController#update(java.lang.Object
	 * )
	 */
	@Override
	@RequestMapping(value = "/serv/private/indicators/upd", method = RequestMethod.POST)
	public @ResponseBody Indicator update(@RequestBody Indicator obj) {
		return super.update(obj);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.impl.controllers.BaseController#delete(java.lang.Object
	 * )
	 */
	@Override
	@RequestMapping(value = "/serv/private/indicators/del", method = RequestMethod.POST)
	public void delete(@RequestBody Indicator obj) {
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.api.controllers.IIndicatorController#getImportIndicators
	 * ()
	 */
	@Override
	@RequestMapping(value = "/serv/private/importindicators", method = RequestMethod.GET)
	public @ResponseBody String getImportIndicators() {
		List<ImportIndicators> l = importIndicatorsService.select(0, 0, null);
		JSONObject jo = new JSONObject();
		for (int i = 0; i < l.size(); i++) {
			jo.put("name", l.get(i).getName());
		}
		return jo.toString();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.api.controllers.IIndicatorController#setImportIndicators
	 * (java.lang.String)
	 */
	@Override
	@RequestMapping(value = "/serv/private/importindicators/set", method = RequestMethod.POST)
	public void setImportIndicators(@RequestBody String nameimport) {
		/*
		 * JSONArray joArray = new JSONArray(); JSONObject jo = new
		 * JSONObject(); jo.put("id", "2"); jo.put("iscomplex", "false");
		 * jo.put("name", "name1"); JSONObject joGrp = new JSONObject();
		 * joGrp.put("id", "2"); joGrp.put("name", "name1");
		 * jo.put("grp",joGrp); joArray.put(jo); ImportIndicators i=new
		 * ImportIndicators(); i.setId("1"); i.setName("name"); byte[] byteArray
		 * = joArray.toString().getBytes(); Byte[] byteObjects = new
		 * Byte[byteArray.length]; int k=0;
		 *
		 * for(byte b: byteArray) byteObjects[k++] = b; i.setBody(byteObjects);
		 * importIndicatorsService.insert(i);
		 */
		List<Criterion> list = new ArrayList<Criterion>();
		list.add(Restrictions.eq("name", nameimport));
		ImportIndicators importIndicators = importIndicatorsService.select(0,
				0, list).get(0);
		int j = 0;
		byte[] bytes = new byte[importIndicators.getBody().length];
		for (Byte b : importIndicators.getBody())
			bytes[j++] = b.byteValue();
		JSONArray arr = new JSONArray(new String(bytes));
		for (int i = 0; i < arr.length(); i++) {
			JSONObject joGrp = arr.getJSONObject(i).getJSONObject("grp");
			JSONObject jo = arr.getJSONObject(i);
			GrpIndicator grp = new GrpIndicator();
			// Change indicator.setAccount(new Account()) to
			// indicator.setAccount(acc from context);
			grp.setAccount(new Account());
			grp.setName(joGrp.getString("name"));
			grp = grpIndicatorService.insert(grp);
			Indicator indicator = new Indicator();
			// Change indicator.setAccount(new Account()) to
			// indicator.setAccount(acc from context);
			indicator.setAccount(new Account());
			indicator.setIsComplex(jo.getBoolean("iscomplex"));
			indicator.setGrp(grp);
			indicator.setName(jo.getString("name"));
			indicatorService.insert(indicator);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.api.controllers.IIndicatorController#selectComplex(int,
	 * int, java.util.List)
	 */
	@Override
	@RequestMapping(value = "/serv/private/complex", method = RequestMethod.GET)
	public @ResponseBody List<Indicator> selectComplex(@RequestBody int from,
			@RequestBody int first, @RequestBody List<Criterion> filter) {
		filter.add(Restrictions.eq("isComplex", 1));
		return super.select(from, first, filter);
	}

}
