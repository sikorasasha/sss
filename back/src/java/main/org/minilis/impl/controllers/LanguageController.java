package org.minilis.impl.controllers;

import java.io.Serializable;

import org.minilis.api.controllers.ILanguageController;
import org.minilis.entity.config.Language;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * The Class LanguageController.
 */
@Controller
@Scope("session")
public class LanguageController extends BaseController<Language> implements
		ILanguageController, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 2408072517458105460L;

}
