package org.minilis.impl.controllers;

import java.io.Serializable;

import org.minilis.api.controllers.IResultFilesController;
import org.minilis.entity.jors.ResultFiles;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * The Class ResultFilesController.
 */
@Controller
@Scope("session")
public class ResultFilesController extends BaseController<ResultFiles>
		implements IResultFilesController, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -3277194840123627843L;

}
