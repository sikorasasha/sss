package org.minilis.impl.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.minilis.api.controllers.IJOrderController;
import org.minilis.api.services.IIndicatorService;
import org.minilis.api.services.IJOrderService;
import org.minilis.entity.dics.Indicator;
import org.minilis.entity.jors.JOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * The Class JOrderController.
 */
@Controller
@Scope("session")
public class JOrderController extends BaseController<JOrder> implements
		IJOrderController, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 6714036689728060447L;

	/** The IndicatorService. */
	@Autowired
	private IIndicatorService indicatorService;

	/** The JOrderService. */
	@Autowired
	private IJOrderService jOrderService;

	/*
	 * (non-Javadoc)
	 *
	 * @see org.minilis.front.impl.controllers.BaseController#select(int, int,
	 * java.util.List)
	 */
	@Override
	@RequestMapping(value = "/serv/private/orders", method = RequestMethod.GET)
	public @ResponseBody List<JOrder> select(@RequestBody int from,
			@RequestBody int first, @RequestBody List<Criterion> filter) {
		return super.select(from, first, filter);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.impl.controllers.BaseController#insert(java.lang.Object
	 * )
	 */
	@Override
	@RequestMapping(value = "/serv/private/orders/add", method = RequestMethod.POST)
	public @ResponseBody JOrder insert(@RequestBody JOrder obj) {
		return super.insert(obj);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.impl.controllers.BaseController#update(java.lang.Object
	 * )
	 */
	@Override
	@RequestMapping(value = "/serv/private/orders/upd", method = RequestMethod.POST)
	public @ResponseBody JOrder update(@RequestBody JOrder obj) {
		return super.update(obj);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.impl.controllers.BaseController#delete(java.lang.Object
	 * )
	 */
	@Override
	@RequestMapping(value = "/serv/private/orders/del", method = RequestMethod.POST)
	public void delete(@RequestBody JOrder obj) {
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.api.controllers.IJOrderController#getLastPrice(org.
	 * minilis.back.entity.dics.Indicator)
	 */
	@RequestMapping(value = "/serv/private/orders/getlastprice", method = RequestMethod.GET)
	public @ResponseBody Double getLastPrice(Indicator obj) {
		List<Criterion> list = new ArrayList<Criterion>();
		list.add(Restrictions.eq("indicator.id", obj.getId()));
		return jOrderService.selectJResultIndicator(1, 0, list).get(0)
				.getjResult().get(0).getSum_();
	}

}
