package org.minilis.impl.controllers;

import java.io.Serializable;

import org.minilis.api.controllers.IQualityControlDtController;
import org.minilis.entity.jors.QualityControlDt;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * The Class QualityControlDtController.
 */
@Controller
@Scope("session")
public class QualityControlDtController extends
		BaseController<QualityControlDt> implements
		IQualityControlDtController, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 23110613442610519L;

}
