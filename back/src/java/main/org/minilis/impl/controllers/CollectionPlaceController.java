package org.minilis.impl.controllers;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.minilis.api.controllers.ICollectionPlaceController;
import org.minilis.entity.dics.CollectionPlace;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * The Class CollectionPlaceController.
 */
@Controller
@Scope("session")
public class CollectionPlaceController extends BaseController<CollectionPlace>
		implements ICollectionPlaceController, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 8622005462143515430L;

	/*
	 * (non-Javadoc)
	 *
	 * @see org.minilis.front.impl.controllers.BaseController#select(int, int,
	 * java.util.List)
	 */
	@Override
	@RequestMapping(value = "/serv/private/collectionplace", method = RequestMethod.GET)
	public @ResponseBody List<CollectionPlace> select(@RequestBody int from,
			@RequestBody int first, @RequestBody List<Criterion> filter) {
		return super.select(from, first, filter);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.impl.controllers.BaseController#insert(java.lang.Object
	 * )
	 */
	@Override
	@RequestMapping(value = "/serv/private/collectionplace/add", method = RequestMethod.POST)
	public @ResponseBody CollectionPlace insert(@RequestBody CollectionPlace obj) {
		return super.insert(obj);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.impl.controllers.BaseController#update(java.lang.Object
	 * )
	 */
	@Override
	@RequestMapping(value = "/serv/private/collectionplace/upd", method = RequestMethod.POST)
	public @ResponseBody CollectionPlace update(@RequestBody CollectionPlace obj) {
		return super.update(obj);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.impl.controllers.BaseController#delete(java.lang.Object
	 * )
	 */
	@Override
	@RequestMapping(value = "/serv/private/collectionplace/del", method = RequestMethod.POST)
	public void delete(@RequestBody CollectionPlace obj) {
	}
}
