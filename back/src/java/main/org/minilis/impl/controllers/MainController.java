/**
 *
 */
package org.minilis.impl.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.json.JSONObject;
import org.minilis.api.controllers.IMainController;
import org.minilis.api.services.IJOrderService;
import org.minilis.entity.jors.JOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * The Class MainController.
 */
@Controller
@Scope("session")
public class MainController extends HttpServlet implements IMainController,
		Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 4930488818998893391L;
	/** The JOrderService. */
	@Autowired
	private IJOrderService jOrderService;

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.api.controllers.IMainController#logOut(javax.servlet
	 * .http.HttpServletRequest)
	 */
	@RequestMapping(value = "/serv/private/logout", method = RequestMethod.POST)
	public @ResponseBody String logOut(HttpServletRequest request) {
		HttpSession session1 = request.getSession(false);

		if (session1 != null) {
			session1.invalidate();
		}
		return new JSONObject().put("responce", true).toString();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.minilis.front.api.controllers.IMainController#search(int,
	 * java.lang.String)
	 */
	@Override
	@RequestMapping(value = "/serv/search", method = RequestMethod.GET)
	public @ResponseBody List<JOrder> search(int from, String filter) {
		List<Criterion> list = new ArrayList<Criterion>();
		list.add(Restrictions.or(Restrictions.eq("num", filter),
				Restrictions.like("patient", filter, MatchMode.START)));
		return jOrderService.select(from, 0, list);
	}

}
