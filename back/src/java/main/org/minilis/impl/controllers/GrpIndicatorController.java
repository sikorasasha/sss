package org.minilis.impl.controllers;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.minilis.api.controllers.IGrpIndicatorController;
import org.minilis.entity.dics.GrpIndicator;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * The Class GrpIndicatorController.
 */
@Controller
@Scope("session")
public class GrpIndicatorController extends BaseController<GrpIndicator>
		implements IGrpIndicatorController, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 7882015064262495203L;

	/*
	 * (non-Javadoc)
	 *
	 * @see org.minilis.front.impl.controllers.BaseController#select(int, int,
	 * java.util.List)
	 */
	@Override
	@RequestMapping(value = "/serv/private/grpindicators", method = RequestMethod.GET)
	public @ResponseBody List<GrpIndicator> select(@RequestBody int from,
			@RequestBody int first, @RequestBody List<Criterion> filter) {
		return super.select(from, first, filter);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.impl.controllers.BaseController#insert(java.lang.Object
	 * )
	 */
	@Override
	@RequestMapping(value = "/serv/private/grpindicators/add", method = RequestMethod.POST)
	public @ResponseBody GrpIndicator insert(@RequestBody GrpIndicator obj) {
		return super.insert(obj);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.impl.controllers.BaseController#update(java.lang.Object
	 * )
	 */
	@Override
	@RequestMapping(value = "/serv/private/grpindicators/upd", method = RequestMethod.POST)
	public @ResponseBody GrpIndicator update(@RequestBody GrpIndicator obj) {
		return super.update(obj);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.impl.controllers.BaseController#delete(java.lang.Object
	 * )
	 */
	@Override
	@RequestMapping(value = "/serv/private/grpindicators/del", method = RequestMethod.POST)
	public void delete(@RequestBody GrpIndicator obj) {
	}

}
