package org.minilis.impl.controllers;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.minilis.api.controllers.IController;
import org.minilis.api.services.IService;
import org.springframework.stereotype.Controller;

/**
 * The Class BaseController.
 *
 * @param <T> the generic type
 */
@Controller
public class BaseController<T> implements IController<T> {

	/** The service. */
	protected IService<T> service;

	/** The obj is the generic type. */
	protected T obj;
	
	/** The list of the generic type. */
	protected List<T> list = new ArrayList<T>();
	
	/** The selected items. */
	protected List<T> selectedItems = new ArrayList<T>();
	
	/** The can edit. */
	private Boolean canEdit = true;

	/**
	 * Gets the obj.
	 *
	 * @return the obj
	 */
	public T getObj() {
		return obj;
	}

	/**
	 * Sets the obj.
	 *
	 * @param obj the new obj
	 */
	public void setObj(T obj) {
		this.obj = obj;
	}

	/**
	 * Gets the list.
	 *
	 * @return the list
	 */
	public List<T> getList() {
		return list;
	}

	/**
	 * Sets the list.
	 *
	 * @param list the new list
	 */
	public void setList(List<T> list) {
		this.list = list;
	}

	/**
	 * Gets the selected items.
	 *
	 * @return the selected items
	 */
	public List<T> getSelectedItems() {
		return selectedItems;
	}

	/**
	 * Sets the selected items.
	 *
	 * @param selectedItems the new selected items
	 */
	public void setSelectedItems(List<T> selectedItems) {
		this.selectedItems.clear();
		if (selectedItems.size() == 1) {
			obj = selectedItems.get(0);
			this.selectedItems = selectedItems;
			canEdit = false;
		} else {
			this.selectedItems = selectedItems;
			canEdit = true;
		}
	}

	/**
	 * New obj.
	 */
	public void newObj() {
		selectedItems.clear();
		canEdit = true;
	}

	/**
	 * Gets the can edit.
	 *
	 * @return the can edit
	 */
	public Boolean getCanEdit() {
		return canEdit;
	}

	/**
	 * Sets the can edit.
	 *
	 * @param canEdit the new can edit
	 */
	public void setCanEdit(Boolean canEdit) {
		this.canEdit = canEdit;
	}

	
	
	/* (non-Javadoc)
	 * @see org.minilis.front.api.controllers.IController#insert(java.lang.Object)
	 */
	@Override
	public T insert(T obj) {
		return service.insert(obj);
	}

	/* (non-Javadoc)
	 * @see org.minilis.front.api.controllers.IController#update(java.lang.Object)
	 */
	@Override
	public T update(T obj) {
		return service.update(obj);
	}

	/* (non-Javadoc)
	 * @see org.minilis.front.api.controllers.IController#delete(java.lang.Object)
	 */
	@Override
	public void delete(T obj) {
		service.delete(obj);
	}

	/* (non-Javadoc)
	 * @see org.minilis.front.api.controllers.IController#select(int, int, java.util.List)
	 */
	@Override
	public List<T> select(int from, int first, List<Criterion> filter) {
		return service.select(from, first, filter);
	}

	/* (non-Javadoc)
	 * @see org.minilis.front.api.controllers.IController#select(int, int, java.util.List)
	 */
	@Override
	public List<T> select(List<Criterion> filter) {
		return service.select(filter);
	}
}
