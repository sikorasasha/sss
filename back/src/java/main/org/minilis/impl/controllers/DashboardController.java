package org.minilis.impl.controllers;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.json.simple.JSONObject;
import org.minilis.api.controllers.IDashboardController;
import org.minilis.api.services.IJLabProccessService;
import org.minilis.api.services.IJOrderService;
import org.minilis.api.services.IJResultService;
import org.minilis.api.services.IQualityControlDtService;
import org.minilis.entity.jors.JOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * The Class DashboardController.
 */
@Controller
@Scope("session")
public class DashboardController implements IDashboardController, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 8592667949617677414L;

	/** The QualityControlDtService. */
	@Autowired
	private IQualityControlDtService qualityControlDtService;

	/** The JResultService. */
	@Autowired
	private IJResultService jResultService;

	/** The JOrgerService. */
	@Autowired
	private IJOrderService jOrgerService;

	/** The JLabProccessService. */
	@Autowired
	private IJLabProccessService jLabProccessService;

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.minilis.front.api.controllers.IDashboardController#getDachBoard()
	 */
	@SuppressWarnings("unchecked")
	@Override
	@RequestMapping(value = "/serv/private/dashboard", method = RequestMethod.GET)
	public String getDachBoard() {
		List<Criterion> list = new ArrayList<Criterion>();
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		String a = fmt.format(new Date());
		Date minDate = null;
		try {
			minDate = fmt.parse(a);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
		list.add(Restrictions.and(Restrictions.ge("dateTime", minDate),
				Restrictions.lt("dateTime", maxDate)));
		int k2 = jLabProccessService.select(0, 0, list).size();
		List<JOrder> jOrderList = jOrgerService.select(0, 0, null);
		for (int i = 0; i < jOrderList.size(); i++) {
			for (int j = 0; j < jOrderList.get(i).getjResult().size(); j++) {
				if (jOrderList.get(i).getjResult().get(j).getDateTimeVerified() == null) {
					jOrderList.remove(i);
				}
			}
		}
		int k3 = jOrderList.size();
		list.add(Restrictions.isNull("jResult.labprocId"));
		list.add(Restrictions.isNull("jResult.dateTimeVerified"));
		int k1 = jOrgerService.selectJResult(0, 0, list).size();
		double k4 = 0;
		if (jOrderList.size() != 0) {
			k4 = k3 / jOrderList.size() * 100;
		}
		JSONObject jo = new JSONObject();
		jo.put("JOrder", k1);
		jo.put("LabProccess", k2);
		jo.put("MadeJOrder", k3);
		jo.put("MadeJOrder%", k4);
		return jo.toString();
	}

}
