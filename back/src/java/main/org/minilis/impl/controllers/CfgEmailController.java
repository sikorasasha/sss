package org.minilis.impl.controllers;

import java.io.Serializable;

import org.minilis.api.controllers.ICfgEmailController;
import org.minilis.entity.config.CfgEmail;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * The Class CfgEmailController.
 */
@Controller
@Scope("session")
public class CfgEmailController extends BaseController<CfgEmail> implements
		ICfgEmailController, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 4084088267877698599L;

}
