package org.minilis.impl.bl;

// check name for fields in javadoc
/**
 * The Class QualityCalcIndics.
 */
public class QualityCalcIndics {

	/** The sum. */
	private double sum_;
	
	/** The lines. */
	private int lines_;
	
	/** The average sum. */
	private double avgSum;
	
	/** The atest value. */
	private double atestVal;
	
	/** The sd dyn. */
	private double sdDyn;
	
	/** The sd stat. */
	private double sdStat;
	
	/** The cv dyn. */
	private double cvDyn;
	
	/** The cv stat. */
	private double cvStat;
	
	/** The b. */
	private double b;
	
	/** The average double. */
	private double avgDouble;
	
	/** The type id. */
	private int typeId;

	/**
	 * Gets the sum_.
	 *
	 * @return the sum_
	 */
	public double getSum_() {
		return sum_;
	}

	/**
	 * Sets the sum_.
	 *
	 * @param sum the new sum_
	 */
	public void setSum_(double sum) {
		this.sum_ = sum;
	}

	/**
	 * Gets the lines_.
	 *
	 * @return the lines_
	 */
	public int getLines_() {
		return lines_;
	}

	/**
	 * Sets the lines_.
	 *
	 * @param lines the new lines_
	 */
	public void setLines_(int lines) {
		this.lines_ = lines;
	}

	/**
	 * Gets the avg sum.
	 *
	 * @return the avg sum
	 */
	public double getAvgSum() {
		return avgSum;
	}

	/**
	 * Sets the avg sum.
	 *
	 * @param avgSum the new avg sum
	 */
	public void setAvgSum(double avgSum) {
		this.avgSum = avgSum;
	}

	/**
	 * Gets the atest val.
	 *
	 * @return the atest val
	 */
	public double getAtestVal() {
		return atestVal;
	}

	/**
	 * Sets the atest val.
	 *
	 * @param atestVal the new atest val
	 */
	public void setAtestVal(double atestVal) {
		this.atestVal = atestVal;
	}

	/**
	 * Gets the sd dyn.
	 *
	 * @return the sd dyn
	 */
	public double getSdDyn() {
		return sdDyn;
	}

	/**
	 * Sets the sd dyn.
	 *
	 * @param sdDyn the new sd dyn
	 */
	public void setSdDyn(double sdDyn) {
		this.sdDyn = sdDyn;
	}

	/**
	 * Gets the sd stat.
	 *
	 * @return the sd stat
	 */
	public double getSdStat() {
		return sdStat;
	}

	/**
	 * Sets the sd stat.
	 *
	 * @param sdStat the new sd stat
	 */
	public void setSdStat(double sdStat) {
		this.sdStat = sdStat;
	}

	/**
	 * Gets the cv dyn.
	 *
	 * @return the cv dyn
	 */
	public double getCvDyn() {
		return cvDyn;
	}

	/**
	 * Sets the cv dyn.
	 *
	 * @param cvDyn the new cv dyn
	 */
	public void setCvDyn(double cvDyn) {
		this.cvDyn = cvDyn;
	}

	/**
	 * Gets the cv stat.
	 *
	 * @return the cv stat
	 */
	public double getCvStat() {
		return cvStat;
	}

	/**
	 * Sets the cv stat.
	 *
	 * @param cvStat the new cv stat
	 */
	public void setCvStat(double cvStat) {
		this.cvStat = cvStat;
	}

	/**
	 * Gets the b.
	 *
	 * @return the b
	 */
	public double getB() {
		return b;
	}

	/**
	 * Sets the b.
	 *
	 * @param b the new b
	 */
	public void setB(double b) {
		this.b = b;
	}

	/**
	 * Gets the avg double.
	 *
	 * @return the avg double
	 */
	public double getAvgDouble() {
		return avgDouble;
	}

	/**
	 * Sets the avg double.
	 *
	 * @param avgDouble the new avg double
	 */
	public void setAvgDouble(double avgDouble) {
		this.avgDouble = avgDouble;
	}

	/**
	 * Gets the type id.
	 *
	 * @return the type id
	 */
	public int getTypeId() {
		return typeId;
	}

	/**
	 * Sets the type id.
	 *
	 * @param typeId the new type id
	 */
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
}
