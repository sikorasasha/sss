package org.minilis.impl.bl;

import org.minilis.api.bl.IQualityRules;
import org.minilis.entity.jors.QualityControl;
import org.minilis.entity.jors.QualityControlDt;
import org.minilis.impl.bl.QualityCalcIndics;
import org.minilis.impl.bl.QualityRuleRes;

/**
 * The Class QualityRules check for Shewhard, Westgard rules.
 */
public class QualityRules implements IQualityRules {

	/**
	 * method the qualityCalcIindics.
	 *
	 * @param qc is current item from QualityControl journal.
	 * @return the quality parameters.
	 */
	@Override
	public QualityCalcIndics getQualityCalcIindics(QualityControl qc) {

		double sumDouble = 0;
		QualityCalcIndics ids = new QualityCalcIndics();
		if (qc.getAttestValue() > 0)
			ids.setAtestVal(qc.getAttestValue());
		else
			ids.setAtestVal(0);

		if ((qc.getAttestValue() > 0)
				&& (qc.getHeightRange() > qc.getAttestValue())) {
			ids.setSdStat((qc.getHeightRange() - qc.getAttestValue()) / 2);
		} else
			ids.setSdStat(0);

		ids.setTypeId(qc.getType_());
		if ((ids.getSdStat() > 0) && (ids.getAtestVal() > 0))
			ids.setCvStat(ids.getSdStat() / ids.getAtestVal());

		for (QualityControlDt qdt : qc.getQualityControlDt()) {

			if ((qdt.getResult() > 0) && (qdt.getIsAccepted() == true)) {
				ids.setLines_(ids.getLines_() + 1);
				ids.setSum_(ids.getSum_() + qdt.getResult());
				if (qdt.getResultDouble() > 0)
					sumDouble += Math
							.abs(ids.getSum_() - qdt.getResultDouble());
			}
		}

		if (ids.getLines_() > 0) {
			ids.setAvgSum(ids.getSum_() / ids.getLines_());
			ids.setAvgDouble(sumDouble / ids.getLines_());
		}

		if (ids.getLines_() >= 2) {
			for (QualityControlDt qdt1 : qc.getQualityControlDt()) {

				if ((qdt1.getResult() > 0) && (qdt1.getIsAccepted() == true))
					ids.setSdDyn(ids.getSdDyn()
							+ Math.pow(qdt1.getResult() - ids.getAvgSum(), 2));

			}
			ids.setSdDyn(Math.sqrt(ids.getSdDyn() / (ids.getLines_() - 1)));
		}

		if (ids.getAvgSum() != 0)
			ids.setCvDyn(ids.getSdDyn() / ids.getAvgSum() * 100);

		if (ids.getAtestVal() != 0)
			ids.setB((ids.getAvgSum() - ids.getAtestVal()) / ids.getAtestVal()
					* 100);

		return ids;
	}

	/**
	 * Gets the shewhart rule.
	 *
	 * @param qc is current item from QualityControl journal.
	 * @return error if the shewhart rule was failed.
	 */
	@Override
	public QualityRuleRes getShewhartRule(QualityControl qc) {

		double s1;
		double s2;
		double s3;
		double m1;
		double m2;
		double m3;
		double x;
		double sdTmp;
		double sumTmp;
		double cntTmp;
		double cntTmp1;
		double cntTmp2;
		double cntTmp3;
		double cntTmp4;
		double cntTmp5;
		double cntTmp6;
		double cntTmp7;
		double cntTmp8;
		double cntTmp9;
		double cntTmp10;
		double cntTmp11;
		double cntTmp12;
		double cntTmp13;
		double cntTmp14;

		QualityCalcIndics ids = getQualityCalcIindics(qc);
		QualityRuleRes res = new QualityRuleRes();

		if (ids.getSdStat() > 0)
			sdTmp = ids.getSdStat();
		else
			sdTmp = ids.getSdDyn();

		if (ids.getAtestVal() > 0)
			x = ids.getAtestVal();
		else
			x = ids.getAvgSum();

		s1 = x + sdTmp;
		s2 = x + sdTmp * 2;
		s3 = x + sdTmp * 3;
		m1 = x - sdTmp;
		m2 = x - sdTmp * 2;
		m3 = x - sdTmp * 3;
		/* ST1 */
		for (QualityControlDt qdt : qc.getQualityControlDt()) {
			if ((qdt.getResult() > 0) && (qdt.getIsAccepted() == true)) {
				sumTmp = qdt.getResult();
				if ((sumTmp > s3) || (sumTmp < m3)) {
					res.putRuleCode("ST01");
					return res;
				}
			}
		}
		/* ST3 */
		cntTmp = x;
		cntTmp1 = x;
		cntTmp2 = x;
		for (QualityControlDt qdt : qc.getQualityControlDt()) {
			if ((qdt.getResult() > 0) && (qdt.getIsAccepted() == true)) {
				sumTmp = qdt.getResult();
				cntTmp2 = cntTmp1;
				cntTmp1 = cntTmp;
				cntTmp = sumTmp;

				sumTmp = 0;

				if (cntTmp > s2)
					sumTmp += 1;
				if (cntTmp1 > s2)
					sumTmp += 1;
				if (cntTmp2 > s2)
					sumTmp += 1;
				if (cntTmp < m2)
					sumTmp -= 1;
				if (cntTmp1 < m2)
					sumTmp -= 1;
				if (cntTmp2 < m2)
					sumTmp -= 1;

				if ((sumTmp == 2) || (sumTmp == -2)) {
					res.putRuleCode("ST03");
					return res;
				}
			}
		}

		/* ST5 */
		cntTmp = x;
		cntTmp1 = x;
		cntTmp2 = x;
		cntTmp3 = x;
		cntTmp4 = x;
		for (QualityControlDt qdt : qc.getQualityControlDt()) {
			if ((qdt.getResult() > 0) && (qdt.getIsAccepted() == true)) {
				sumTmp = qdt.getResult();
				cntTmp4 = cntTmp3;
				cntTmp3 = cntTmp2;
				cntTmp2 = cntTmp1;
				cntTmp1 = cntTmp;
				cntTmp = sumTmp;

				sumTmp = 0;

				if (cntTmp > s1)
					sumTmp += 1;
				if (cntTmp1 > s1)
					sumTmp += 1;
				if (cntTmp2 > s1)
					sumTmp += 1;
				if (cntTmp3 > s1)
					sumTmp += 1;
				if (cntTmp4 > s1)
					sumTmp += 1;
				if (cntTmp < m1)
					sumTmp -= 1;
				if (cntTmp1 < m1)
					sumTmp -= 1;
				if (cntTmp2 < m1)
					sumTmp -= 1;
				if (cntTmp3 < m1)
					sumTmp -= 1;
				if (cntTmp4 < m1)
					sumTmp -= 1;

				if ((sumTmp == 4) || (sumTmp == -4)) {
					res.putRuleCode("ST05");
					return res;

				}
			}
		}
		/* ST6 */
		cntTmp = x;
		cntTmp1 = x;
		cntTmp2 = x;
		cntTmp3 = x;
		cntTmp4 = x;
		cntTmp5 = x;

		for (QualityControlDt qdt : qc.getQualityControlDt()) {
			if ((qdt.getResult() > 0) && (qdt.getIsAccepted() == true)) {
				sumTmp = qdt.getResult();
				cntTmp5 = cntTmp4;
				cntTmp4 = cntTmp3;
				cntTmp3 = cntTmp2;
				cntTmp2 = cntTmp1;
				cntTmp1 = cntTmp;
				cntTmp = sumTmp;

				if (((cntTmp < cntTmp1) && (cntTmp1 < cntTmp2)
						&& (cntTmp2 < cntTmp3) && (cntTmp3 < cntTmp4) && (cntTmp4 < cntTmp5))
						|| ((cntTmp > cntTmp1) && (cntTmp1 > cntTmp2)
								&& (cntTmp2 > cntTmp3) && (cntTmp3 > cntTmp4) && (cntTmp4 > cntTmp5))) {
					res.putRuleCode("ST06");
					return res;
				}
			}
		}
		/* ST8 */
		cntTmp = x;
		cntTmp1 = x;
		cntTmp2 = x;
		cntTmp3 = x;
		cntTmp4 = x;
		cntTmp5 = x;
		cntTmp6 = x;
		cntTmp7 = x;
		for (QualityControlDt qdt : qc.getQualityControlDt()) {
			if ((qdt.getResult() > 0) && (qdt.getIsAccepted() == true)) {
				sumTmp = qdt.getResult();
				cntTmp7 = cntTmp6;
				cntTmp6 = cntTmp5;
				cntTmp5 = cntTmp4;
				cntTmp4 = cntTmp3;
				cntTmp3 = cntTmp2;
				cntTmp2 = cntTmp1;
				cntTmp1 = cntTmp;
				cntTmp = sumTmp;

				sumTmp = 0;

				if ((cntTmp > s1) || (cntTmp < m1))
					sumTmp += 1;
				if ((cntTmp1 > s1) || (cntTmp1 < m1))
					sumTmp += 1;
				if ((cntTmp2 > s1) || (cntTmp2 < m1))
					sumTmp += 1;
				if ((cntTmp3 > s1) || (cntTmp3 < m1))
					sumTmp += 1;
				if ((cntTmp4 > s1) || (cntTmp4 < m1))
					sumTmp += 1;
				if ((cntTmp5 > s1) || (cntTmp5 < m1))
					sumTmp += 1;
				if ((cntTmp6 > s1) || (cntTmp6 < m1))
					sumTmp += 1;
				if ((cntTmp7 > s1) || (cntTmp7 < m1))
					sumTmp += 1;

				if (sumTmp == 8) {
					res.putRuleCode("ST08");
					return res;
				}
			}
		}
		/* ST9 */
		cntTmp = x;
		cntTmp1 = x;
		cntTmp2 = x;
		cntTmp3 = x;
		cntTmp4 = x;
		cntTmp5 = x;
		cntTmp6 = x;
		cntTmp7 = x;
		cntTmp8 = x;
		for (QualityControlDt qdt : qc.getQualityControlDt()) {
			if ((qdt.getResult() > 0) && (qdt.getIsAccepted() == true)) {
				sumTmp = qdt.getResult();
				cntTmp8 = cntTmp7;
				cntTmp7 = cntTmp6;
				cntTmp6 = cntTmp5;
				cntTmp5 = cntTmp4;
				cntTmp4 = cntTmp3;
				cntTmp3 = cntTmp2;
				cntTmp2 = cntTmp1;
				cntTmp1 = cntTmp;
				cntTmp = sumTmp;

				if (((cntTmp > x) && (cntTmp1 > x) && (cntTmp2 > x)
						&& (cntTmp3 > x) && (cntTmp4 > x) && (cntTmp5 > x)
						&& (cntTmp6 > x) && (cntTmp7 > x) && (cntTmp8 > x))
						|| ((cntTmp < x) && (cntTmp1 < x) && (cntTmp2 < x)
								&& (cntTmp3 < x) && (cntTmp4 < x)
								&& (cntTmp5 < x) && (cntTmp6 < x)
								&& (cntTmp7 < x) && (cntTmp8 < x))) {

					res.putRuleCode("ST09");
					return res;
				}
			}
		}
		/* ST14 */
		cntTmp = x;
		cntTmp1 = x;
		cntTmp2 = x;
		cntTmp3 = x;
		cntTmp4 = x;
		cntTmp5 = x;
		cntTmp6 = x;
		cntTmp7 = x;
		cntTmp8 = x;
		cntTmp9 = x;
		cntTmp10 = x;
		cntTmp11 = x;
		cntTmp12 = x;
		cntTmp13 = x;
		cntTmp14 = 0;
		for (QualityControlDt qdt : qc.getQualityControlDt()) {
			if ((qdt.getResult() > 0) && (qdt.getIsAccepted() == true)) {
				sumTmp = qdt.getResult();
				cntTmp14 += 1;

				cntTmp13 = cntTmp12;
				cntTmp12 = cntTmp11;
				cntTmp11 = cntTmp10;
				cntTmp10 = cntTmp9;
				cntTmp9 = cntTmp8;
				cntTmp8 = cntTmp7;
				cntTmp7 = cntTmp6;
				cntTmp6 = cntTmp5;
				cntTmp5 = cntTmp4;
				cntTmp4 = cntTmp3;
				cntTmp3 = cntTmp2;
				cntTmp2 = cntTmp1;
				cntTmp1 = cntTmp;
				cntTmp = sumTmp;

				if ((((cntTmp13 > cntTmp12) && (cntTmp12 < cntTmp11)
						&& (cntTmp11 > cntTmp10) && (cntTmp10 < cntTmp9)
						&& (cntTmp9 > cntTmp8) && (cntTmp8 < cntTmp7)
						&& (cntTmp7 > cntTmp6) && (cntTmp6 < cntTmp5)
						&& (cntTmp5 > cntTmp4) && (cntTmp4 < cntTmp3)
						&& (cntTmp3 > cntTmp2) && (cntTmp2 < cntTmp1) && (cntTmp1 > cntTmp)) || ((cntTmp13 < cntTmp12)
						&& (cntTmp12 > cntTmp11)
						&& (cntTmp11 < cntTmp10)
						&& (cntTmp10 > cntTmp9)
						&& (cntTmp9 < cntTmp8)
						&& (cntTmp8 > cntTmp7)
						&& (cntTmp7 < cntTmp6)
						&& (cntTmp6 > cntTmp5)
						&& (cntTmp5 < cntTmp4)
						&& (cntTmp4 > cntTmp3)
						&& (cntTmp3 < cntTmp2)
						&& (cntTmp2 > cntTmp1) && (cntTmp1 < cntTmp)))
						&& (cntTmp14 >= 14)) {
					res.putRuleCode("ST14");
					return res;
				}
			}
		}
		/* ST15 */
		cntTmp = 0;
		cntTmp1 = 0;
		cntTmp2 = 0;
		cntTmp3 = 0;
		cntTmp4 = 0;
		cntTmp5 = 0;
		cntTmp6 = 0;
		cntTmp7 = 0;
		cntTmp8 = 0;
		cntTmp9 = 0;
		cntTmp10 = 0;
		cntTmp11 = 0;
		cntTmp12 = 0;
		cntTmp13 = 0;
		cntTmp14 = 0;
		for (QualityControlDt qdt : qc.getQualityControlDt()) {
			if ((qdt.getResult() > 0) && (qdt.getIsAccepted() == true)) {
				sumTmp = qdt.getResult();
				cntTmp14 = cntTmp13;
				cntTmp13 = cntTmp12;
				cntTmp12 = cntTmp11;
				cntTmp11 = cntTmp10;
				cntTmp10 = cntTmp9;
				cntTmp9 = cntTmp8;
				cntTmp8 = cntTmp7;
				cntTmp7 = cntTmp6;
				cntTmp6 = cntTmp5;
				cntTmp5 = cntTmp4;
				cntTmp4 = cntTmp3;
				cntTmp3 = cntTmp2;
				cntTmp2 = cntTmp1;
				cntTmp1 = cntTmp;
				cntTmp = sumTmp;

				s3 = 0;
				m3 = 0;

				if ((cntTmp >= x) && (cntTmp < s1))
					s3 += 1;
				if ((cntTmp < x) && (cntTmp > m1))
					m3 += 1;

				if ((cntTmp1 >= x) && (cntTmp1 < s1))
					s3 += 1;
				if ((cntTmp1 < x) && (cntTmp1 > m1))
					m3 += 1;

				if ((cntTmp2 >= x) && (cntTmp2 < s1))
					s3 += 1;
				if ((cntTmp2 < x) && (cntTmp2 > m1))
					m3 += 1;

				if ((cntTmp3 >= x) && (cntTmp3 < s1))
					s3 += 1;
				if ((cntTmp3 < x) && (cntTmp3 > m1))
					m3 += 1;

				if ((cntTmp4 >= x) && (cntTmp4 < s1))
					s3 += 1;
				if ((cntTmp4 < x) && (cntTmp4 > m1))
					m3 += 1;

				if ((cntTmp5 >= x) && (cntTmp5 < s1))
					s3 += 1;
				if ((cntTmp5 < x) && (cntTmp5 > m1))
					m3 += 1;

				if ((cntTmp6 >= x) && (cntTmp6 < s1))
					s3 += 1;
				if ((cntTmp6 < x) && (cntTmp6 > m1))
					m3 += 1;

				if ((cntTmp7 >= x) && (cntTmp7 < s1))
					s3 += 1;
				if ((cntTmp7 < x) && (cntTmp7 > m1))
					m3 += 1;

				if ((cntTmp8 >= x) && (cntTmp8 < s1))
					s3 += 1;
				if ((cntTmp8 < x) && (cntTmp8 > m1))
					m3 += 1;

				if ((cntTmp9 >= x) && (cntTmp9 < s1))
					s3 += 1;
				if ((cntTmp9 < x) && (cntTmp9 > m1))
					m3 = m3 + 1;

				if ((cntTmp10 >= x) && (cntTmp10 < s1))
					s3 = s3 + 1;
				if ((cntTmp10 < x) && (cntTmp10 > m1))
					m3 = m3 + 1;

				if ((cntTmp11 >= x) && (cntTmp11 < s1))
					s3 = s3 + 1;
				if ((cntTmp11 < x) && (cntTmp11 > m1))
					m3 = m3 + 1;

				if ((cntTmp12 >= x) && (cntTmp12 < s1))
					s3 = s3 + 1;
				if ((cntTmp12 < x) && (cntTmp12 > m1))
					m3 = m3 + 1;

				if ((cntTmp13 >= x) && (cntTmp13 < s1))
					s3 = s3 + 1;
				if ((cntTmp13 < x) && (cntTmp13 > m1))
					m3 = m3 + 1;

				if ((cntTmp14 >= x) && (cntTmp14 < s1))
					s3 = s3 + 1;
				if ((cntTmp14 < x) && (cntTmp14 > m1))
					m3 = m3 + 1;

				if ((((s3 + m3)) == 15) && (s3 > 0) && (m3 > 0)) {
					res.putRuleCode("ST15");
					return res;
				}
			}
		}
		res.setAccepted(true);
		res.clearRuleCodeList();
		return res;
	}

	/**
	 * Gets the westgard rule.
	 *
	 * @param qc is current item from QualityControl journal.
	 * @return error if the westgard rule was failed.
	 */
	@Override
	public QualityRuleRes getWestgardRule(QualityControl qc) {

		QualityCalcIndics ids = getQualityCalcIindics(qc);
		QualityRuleRes res = new QualityRuleRes();
		double s1 = 0;
		double s2 = 0;
		double s3 = 0;
		double m1 = 0;
		double m2 = 0;
		double m3 = 0;
		double x = 0;
		double sdTmp = 0;
		double sumTmp = 0;
		double cntTmp = 0;
		double cntTmp1 = 0;

		if (ids.getSdStat() > 0)
			sdTmp = ids.getSdStat();
		else
			sdTmp = ids.getSdDyn();

		if (ids.getAtestVal() > 0)
			x = ids.getAtestVal();
		else
			x = ids.getAvgSum();

		s1 = x + sdTmp;
		s2 = x + sdTmp * 2;
		s3 = x + sdTmp * 3;
		m1 = x - sdTmp;
		m2 = x - sdTmp * 2;
		m3 = x - sdTmp * 3;
		/* 12S */
		for (QualityControlDt qdt : qc.getQualityControlDt()) {
			if ((qdt.getResult() > 0) && (qdt.getIsAccepted() == true)) {
				sumTmp = qdt.getResult();
				if ((sumTmp > s2) || (sumTmp < m2)) {
					res.putRuleCode("R12S");
					break;

				}
			}

		}
		if (res.getRuleCodeList().size() == 0) {
			res.setAccepted(true);
			return res;
		}
		/* 13S */
		for (QualityControlDt qdt : qc.getQualityControlDt()) {
			if ((qdt.getResult() > 0) && (qdt.getIsAccepted() == true)) {
				sumTmp = qdt.getResult();
				if ((sumTmp > s3) || (sumTmp < m3)) {
					res.putRuleCode("R13S");
					return res;
				}
			}

		}
		/* 22S */
		for (QualityControlDt qdt : qc.getQualityControlDt()) {
			if ((qdt.getResult() > 0) && (qdt.getIsAccepted() == true)) {
				sumTmp = qdt.getResult();
				if (sumTmp > s2)
					cntTmp += 1;
				if (sumTmp < m2)
					cntTmp -= 1;
				if ((cntTmp == 2) || (cntTmp == -2)) {
					res.putRuleCode("R22S");
					return res;
				}
			}

		}
		/* 4S */
		cntTmp = x;
		cntTmp1 = x;
		for (QualityControlDt qdt : qc.getQualityControlDt()) {
			if ((qdt.getResult() > 0) && (qdt.getIsAccepted() == true)) {
				sumTmp = qdt.getResult();
				cntTmp1 = cntTmp;
				cntTmp = sumTmp;
				if (((cntTmp > s2) && (cntTmp1 < m2))
						|| ((cntTmp < m2) && (cntTmp1 > s2))) {
					res.putRuleCode("R4S");
					return res;
				}
			}

		}
		/* 41S */
		cntTmp = 0;
		for (QualityControlDt qdt : qc.getQualityControlDt()) {
			if ((qdt.getResult() > 0) && (qdt.getIsAccepted() == true)) {
				sumTmp = qdt.getResult();
				if (sumTmp > s1)
					cntTmp += 1;
				if (sumTmp < m1)
					cntTmp -= 1;
			}

		}
		if ((cntTmp == 4) || (cntTmp == -4)) {
			res.putRuleCode("R41S");
			return res;
		}
		/* 10X */
		cntTmp = 0;
		for (QualityControlDt qdt : qc.getQualityControlDt()) {
			if ((qdt.getResult() > 0) && (qdt.getIsAccepted() == true)) {
				sumTmp = qdt.getResult();
				if (sumTmp > x)
					cntTmp += 1;
				if (sumTmp < x)
					cntTmp -= 1;
			}

		}
		if ((cntTmp == 10) || (cntTmp == -10)) {
			res.putRuleCode("R10X");
			return res;
		}
		res.setAccepted(true);
		res.clearRuleCodeList();
		return res;
	}
}
