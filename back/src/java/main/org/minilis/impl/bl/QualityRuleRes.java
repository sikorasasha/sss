package org.minilis.impl.bl;

import java.util.ArrayList;
import java.util.List;

// check the fields for Javadoc
/**
 * The Class QualityRuleRes.
 */
public class QualityRuleRes {

	/** The is accepted. */
	private boolean isAccepted;
	
	/** The rule code list. */
	private List<String> ruleCodeList;

	/**
	 * Instantiates a new quality rule res.
	 */
	public QualityRuleRes() {
		this.ruleCodeList = new ArrayList<String>();
	}

	/**
	 * Checks if is accepted.
	 *
	 * @return true, if is accepted
	 */
	public boolean isAccepted() {
		return isAccepted;
	}

	/**
	 * Sets the accepted.
	 *
	 * @param isAccepted the new accepted
	 */
	public void setAccepted(boolean isAccepted) {
		this.isAccepted = isAccepted;
	}

	/**
	 * Gets the rule code list.
	 *
	 * @return the rule code list
	 */
	public List<String> getRuleCodeList() {
		return ruleCodeList;
	}

	/**
	 * Sets the rule code list.
	 *
	 * @param ruleCodeList the new rule code list
	 */
	public void setRuleCodeList(List<String> ruleCodeList) {
		this.ruleCodeList = ruleCodeList;
	}

	/**
	 * Put rule code.
	 *
	 * @param ruleCode the rule code
	 */
	public void putRuleCode(String ruleCode) {
		this.ruleCodeList.add(ruleCode);
	}

	/**
	 * Del rule code.
	 *
	 * @param ruleCode the rule code
	 */
	public void delRuleCode(String ruleCode) {
		this.ruleCodeList.remove(this.ruleCodeList.indexOf(ruleCode));
	}

	/**
	 * Clear rule code list.
	 */
	public void clearRuleCodeList() {
		this.ruleCodeList.clear();
	}
}